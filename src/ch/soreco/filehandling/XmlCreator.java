package ch.soreco.filehandling;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import ch.soreco.orderbook.bo.Order;
import ch.soreco.orderbook.bo.Scanning;

public class XmlCreator {
	
	private static final String NAMESPACE_BSC = "http://www.imtf.com/hypersuite/metadata/bsc/";
	private static final String NAMESPACE_CUSTOM = "http://www.imtf.com/e-document/metadata/custom/";
	private static final String NAMESPACE_DC = "http://purl.org/dc/elements/1.1/";
	private static final String NAMESPACE_DCTERMS = "http://purl.org/dc/terms/";
	private static final String NAMESPACE_EDOC = "http://www.imtf.com/hypersuite/edoc/2.0/";
	private static final String NAMESPACE_PF = "http://www.imtf.com/hypersuite/pf/";
	private static final String NAMESPACE_RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
		
	/**
	 * 
	 * @param scanning	Abzubildendes Scanning-Objekt
	 * @param targetPath	Zielpfad des xml-Files
	 * @throws Exception 
	 */
	public XmlCreator(Scanning scanning, String orderNr, Order order, String targetPath ) throws Exception
	{
		//+"_"+scanning.getArchivingDate().format("yyMMddhhmmss")
		String fileName = getFilename(scanning);
		fileName = fileName.substring(0, fileName.lastIndexOf('.'));
		String filePath = targetPath + fileName + "\\";
		new java.io.File(filePath).mkdirs();
		
		fileName = fileName + ".xml";
		targetPath = filePath + fileName;
		
		Element root = createXML(scanning, orderNr, order);
		writeXML(root, targetPath);
	}
	
	/**
	 * Erstellt das xml-File
	 * @throws Exception 
	 * 
	 */
	private Element createXML(Scanning scanning, String orderNr, Order order) throws Exception
	{
		Namespace nsBsc = Namespace.getNamespace("bsc", NAMESPACE_BSC);
		Namespace nsCustom = Namespace.getNamespace("custom", NAMESPACE_CUSTOM);
		Namespace nsDc = Namespace.getNamespace("dc", NAMESPACE_DC);
		Namespace nsDcterms = Namespace.getNamespace("dcterms", NAMESPACE_DCTERMS);
		Namespace nsEdoc = Namespace.getNamespace("edoc", NAMESPACE_EDOC);
		Namespace nsPf = Namespace.getNamespace("pf", NAMESPACE_PF);
		Namespace nsRdf = Namespace.getNamespace("rdf", NAMESPACE_RDF);
		
		Element root = new Element("RDF", nsRdf);
		Element subroot = new Element("Description", nsRdf);

		subroot.setAttribute(new Attribute("about", getFilename(scanning), nsRdf));
		
		root.addNamespaceDeclaration(nsBsc);
		root.addNamespaceDeclaration(nsCustom);
		root.addNamespaceDeclaration(nsDc);
		root.addNamespaceDeclaration(nsDcterms);
		root.addNamespaceDeclaration(nsEdoc);
		root.addNamespaceDeclaration(nsPf);
		root.addNamespaceDeclaration(nsRdf);
		
		subroot.addContent(generateElement(nsEdoc, "Type", "ScanBZP"));
		subroot.addContent(generateElement(nsEdoc, "SchemaName", "DokKunden"));
		subroot.addContent(generateElement(nsEdoc, "AgencyIdentifier", "BZP"));
		subroot.addContent(generateElement(nsEdoc, "Classification", "CL-#" + scanning.getDocType()));
		subroot.addContent(generateElement(nsEdoc, "ClientNumber", getClientNr(order)));

//		int containerNr = getContainerNr(order);
		
		if(order.getBPId().length() == 12)
		{
			subroot.addContent(generateElement(nsEdoc, "AccountNumber", order.getBPId()));
		}
		
		subroot.addContent(generateElement(nsEdoc, "Date", scanning.getScanDate().toString()));
		
		if(scanning.getDocType().equals("HH"))
		{
			subroot.addContent(generateElement(nsEdoc, "DeliveryType", "HoldMail"));
		}
		else
		{
			subroot.addContent(generateElement(nsEdoc, "DeliveryType", "None"));
		}

		subroot.addContent(generateElement(nsBsc, "EmpfaengerNr", getClientNr(order)));
			
		if(!orderNr.isEmpty())
		{		

			subroot.addContent(generateElement(nsBsc, "OrderNr", orderNr));
		}
		
		if(order.getFAClanId() != null && !order.getFAClanId().isEmpty())
		{
			subroot.addContent(generateElement(nsBsc, "ReferenceNr", order.getFAClanId()));
		}

		
		// set orderNr = orderNr of mainOrder (if order was merged) 
		String ivyId = "";
		if(order.getMatchedToOrder() > 0){
			ivyId = order.getMatchedToOrder().toString();
		} else {
			ivyId = order.getOrderId().toString();
		}
		
		subroot.addContent(generateElement(nsBsc, "InternalNr", ivyId ));
		
		/**
		 * As of Project Integration Pension, (October, 2013) Tag DocumentNr is no longer required for 
		 * Hypersuite
		 */
		// replace . just to be sure
		//subroot.addContent(generateElement(nsBsc, "DocumentNr", getDokumentenNr(scanning).replace(".", "")));
		
		root.addContent(subroot);
		return root;
	}
	/**
	 * As of Project Integration Pension, (October, 2013) Tag DocumentNr is no longer required for 
	 * Hypersuite
	private String getDokumentenNr(Scanning scanning) throws Exception
	{
		Date scandatum = scanning.getScanDate();
		String jahr = Integer.toString(scandatum.getYear());
		
		String monat;
		if(scandatum.getMonth() < 10)
		{
			monat = "0" + scandatum.getMonth();
		}
		else
		{
			monat = "" + scandatum.getMonth();
		}
		
		String tag;
		if(scandatum.getDay() < 10)
		{
			tag = "0" + scandatum.getDay();
		}
		else
		{
			tag = "" + scandatum.getDay();
		}
		
		String scanNr = scanning.getScanShare().getScanNo();
		
		String dokId = "";
		if(!scanning.getPageNoStart().toString().isEmpty())
		{
			dokId = scanning.getPageNoStart().toString();//scanning.getDocNo();
			int dLength = dokId.length();
			
			while(dLength < 6){
				dokId = "0"+dokId;
				dLength++;
			}
		}
		
		else
		{
			throw new Exception("No DocumentNo. available (ScanFileId: " + scanning.getScanFileId() + ").");
		}
	
		return "" + jahr + monat + tag + scanNr + dokId;
	}
	*/
	/**
	private int getContainerNr(Order order) 
	{		
		int containerNr = 0;
		
		if(order.getRecording() != null && order.getRecording().getCorrespondence() != null & order.getRecording().getCorrespondence().getCorrespondenceContainer() != null)
		{
			for(CorrespondenceContainer container : order.getRecording().getCorrespondence().getCorrespondenceContainer())
			{
				try
				{
					int aktContNr = Integer.parseInt(container.getContainerOrderNo());
					
					if(aktContNr > containerNr)
					{
						containerNr = aktContNr;
					}
				}
				catch (NumberFormatException ex)
				{
					Ivy.log().debug("Error occured on XmlCreator - parsing Container OrderNo:"+ex.getMessage(),ex);
				}
			}
		}
		return containerNr;
	}
*/
	private String getClientNr(Order order)
	{
		if(order.getBPId().contains("."))
		{
			//return order.getBPId().substring(0, 7);
			// CR / PB / 09.12.2011:
			// BP can be 8 digits long
			return order.getBPId().substring(0,order.getBPId().indexOf("."));
		}			
		else
		{
			return order.getBPId();
		}
	}

	private Element generateElement(Namespace ns, String elementName, String content) {
		Element child = new Element(elementName, ns);
		child.addContent(content);
		return child;
	}
	
	private String getFilename(Scanning scanning)
	{
		//scanning.getScanFile();
		return scanning.getScanFile().replace(".pdf", "_"+scanning.getArchivingDate().format("yyMMddhhmmss")+".pdf"); 
	}
	
	/**
	 * Erstellt das xml-Dokument und speichert es ab.
	 * 
	 * @param root	Root-Element des erstellten .xml-Files.
	 * @throws IOException 
	 */
	private void writeXML(Element root, String targetPath) throws IOException 
	{
		File file = new File(targetPath);
		if(file.exists())
		{
			throw new IOException("File " + targetPath + " already exists.");
		}
		Document doc = new Document(root);
		XMLOutputter outputter = new XMLOutputter(Format.getPrettyFormat());
		FileOutputStream output;
		
		try
		{
			output = new FileOutputStream(targetPath);		
			outputter.output(doc,output);
		}
		catch(IOException ex)
		{
			throw new IOException("No access to " + targetPath + ".");
		}
	}
}