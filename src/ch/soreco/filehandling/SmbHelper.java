package ch.soreco.filehandling;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;

public class SmbHelper {
	private NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication("", "", "");
	public SmbHelper(String username, String password){
		this.auth = new NtlmPasswordAuthentication("", username, password);
	}
    public File getJavaFile( String from ) throws Exception {
		SmbFile source = getSmbFile(from);
		//File javaFile = File.createTempFile(source.getName(), null);
		File javaFile = new File( source.getName() );
        return getJavaFile(source, javaFile);
    }
    public File getJavaFile( String from, java.io.File javaFile ) throws Exception {
    	SmbFile source = getSmbFile(from);
        return getJavaFile(source, javaFile);
    }
    public File getJavaFile(String fromSMB , String toLocal) throws Exception{
    	File javaFile = new File( toLocal );
    	return getJavaFile(fromSMB, javaFile);
    }
    public File getJavaFile(SmbFile source, File javaFile) throws IOException{
        int i;
        byte[] buf = new byte[8192];
        
        SmbFileInputStream in = new SmbFileInputStream( source );
        FileOutputStream out = new FileOutputStream( javaFile );

        while(( i = in.read( buf )) > 0 ) {
            out.write( buf, 0, i );
        }
        in.close();
        out.close();

		javaFile.deleteOnExit();
		
        return javaFile;
    }
    public SmbFile getSmbFile( String uri ) throws MalformedURLException {
    	SmbFile source = new SmbFile(uri, this.auth);
    	return source;
    }
    public void renameTo(String from, String to) throws MalformedURLException, SmbException{
		SmbFile source = getSmbFile(from);
		SmbFile dest = getSmbFile(to);
		source.renameTo(dest);
	}
    public String getName(String uri) throws MalformedURLException, SmbException{
		SmbFile source = getSmbFile(uri);
		return source.getName();
	}
	public List<String> listFiles(String uri) throws MalformedURLException, SmbException{
		SmbFile[] filesArray;
		List<String> files = new ArrayList<String>();
		
		SmbFile dir = getSmbFile( uri );
		
		filesArray = dir.listFiles();
		for(int i=0;i<filesArray.length;i++){
			files.add(filesArray[i].getPath());
		}
		return files;
	}
	public void deleteDir(String uri) throws MalformedURLException, SmbException{
		SmbFile source = getSmbFile( uri );
		source.delete();
	}
	public void mkdir(String uri) throws MalformedURLException, SmbException{
		Boolean isDir;
		try {
			SmbFile source = getSmbFile( uri );
			isDir = source.isDirectory();
		} catch( Exception e ) {
			isDir = false;	
		}
		
		if (!isDir) {
			(getSmbFile( uri )).mkdir();
		}
		
	}
}
