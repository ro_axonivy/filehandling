package ch.soreco.filehandling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

/**
 * This class is used to manage files and directories.
 */
public class Filehelper {
	/**
	 * Deletes a directory and all contained files.
	 * @param path the directory which has to be deleted
	 * @return throw true if the directory was successful deleted
	 */
	public static boolean deleteDir(File path){
		
		if( path.exists() ) 
		{
	      File[] files = path.listFiles();
	      for(File iFile:files) {
	         if(iFile.isDirectory()) {
	           deleteDir(iFile);
	         }
	         else {
	           iFile.delete();
	         }
	      }
	      return( path.delete() );
	    }
	    else { return false;}
	}
	
	/**
	 * Deletes a file
	 */
	public static boolean deleteFile(File file)
	{
		if(file.exists() && file.isFile())
		{
			return file.delete();
		}
		return false;
	}
	
	/**
	 * Creates a copy of a file.
	 * @param in	File to copy
	 * @param out	Destination file
	 * @throws IOException
	 */
	public static void copyFile( File in, File out )  throws IOException
	{
		FileInputStream fis = new FileInputStream( in );
		FileChannel inChannel = fis.getChannel();
		FileOutputStream fos = new FileOutputStream( out );
		FileChannel outChannel = fos.getChannel();
		try
		{
			//  inChannel.transferTo(0, inChannel.size(), outChannel);      // original -- apparently has trouble copying large files on Windows

			// magic number for Windows, 64Mb - 32Kb)
			int maxCount = (64 * 1024 * 1024) - (32 * 1024);
			long size = inChannel.size();
			long position = 0;
			while ( position < size )
			{
				position += inChannel.transferTo( position, maxCount, outChannel );
			}
		}
		finally
		{
		    if ( inChannel != null )
		    {
		       inChannel.close();
		    }
		    if ( fis != null )
		    {
		       fis.close();
		    }
		    if ( outChannel != null )
		    {
		        outChannel.close();
		    }
		    if ( fos != null )
		    {
		        fos.close();
		    }
		    
		}
	}
}
