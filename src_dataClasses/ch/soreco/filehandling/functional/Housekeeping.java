package ch.soreco.filehandling.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Housekeeping", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Housekeeping extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 619800417697293526L;

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.soreco.orderbook.bo.ScanShare Share;

  /**
   * Gets the field Share.
   * @return the value of the field Share; may be null.
   */
  public ch.soreco.orderbook.bo.ScanShare getShare()
  {
    return Share;
  }

  /**
   * Sets the field Share.
   * @param _Share the new value of the field Share.
   */
  public void setShare(ch.soreco.orderbook.bo.ScanShare _Share)
  {
    Share = _Share;
  }

  private java.lang.String doneSuffix;

  /**
   * Gets the field doneSuffix.
   * @return the value of the field doneSuffix; may be null.
   */
  public java.lang.String getDoneSuffix()
  {
    return doneSuffix;
  }

  /**
   * Sets the field doneSuffix.
   * @param _doneSuffix the new value of the field doneSuffix.
   */
  public void setDoneSuffix(java.lang.String _doneSuffix)
  {
    doneSuffix = _doneSuffix;
  }

  private java.lang.String warnSuffix;

  /**
   * Gets the field warnSuffix.
   * @return the value of the field warnSuffix; may be null.
   */
  public java.lang.String getWarnSuffix()
  {
    return warnSuffix;
  }

  /**
   * Sets the field warnSuffix.
   * @param _warnSuffix the new value of the field warnSuffix.
   */
  public void setWarnSuffix(java.lang.String _warnSuffix)
  {
    warnSuffix = _warnSuffix;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> allFiles;

  /**
   * Gets the field allFiles.
   * @return the value of the field allFiles; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getAllFiles()
  {
    return allFiles;
  }

  /**
   * Sets the field allFiles.
   * @param _allFiles the new value of the field allFiles.
   */
  public void setAllFiles(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _allFiles)
  {
    allFiles = _allFiles;
  }

  private ch.soreco.orderbook.bo.ScanLog ScanLog;

  /**
   * Gets the field ScanLog.
   * @return the value of the field ScanLog; may be null.
   */
  public ch.soreco.orderbook.bo.ScanLog getScanLog()
  {
    return ScanLog;
  }

  /**
   * Sets the field ScanLog.
   * @param _ScanLog the new value of the field ScanLog.
   */
  public void setScanLog(ch.soreco.orderbook.bo.ScanLog _ScanLog)
  {
    ScanLog = _ScanLog;
  }

  private java.lang.String achiveFolder;

  /**
   * Gets the field achiveFolder.
   * @return the value of the field achiveFolder; may be null.
   */
  public java.lang.String getAchiveFolder()
  {
    return achiveFolder;
  }

  /**
   * Sets the field achiveFolder.
   * @param _achiveFolder the new value of the field achiveFolder.
   */
  public void setAchiveFolder(java.lang.String _achiveFolder)
  {
    achiveFolder = _achiveFolder;
  }

  private ch.ivyteam.ivy.scripting.objects.Date movingDate;

  /**
   * Gets the field movingDate.
   * @return the value of the field movingDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getMovingDate()
  {
    return movingDate;
  }

  /**
   * Sets the field movingDate.
   * @param _movingDate the new value of the field movingDate.
   */
  public void setMovingDate(ch.ivyteam.ivy.scripting.objects.Date _movingDate)
  {
    movingDate = _movingDate;
  }

  private ch.ivyteam.ivy.scripting.objects.Date deleteDate;

  /**
   * Gets the field deleteDate.
   * @return the value of the field deleteDate; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Date getDeleteDate()
  {
    return deleteDate;
  }

  /**
   * Sets the field deleteDate.
   * @param _deleteDate the new value of the field deleteDate.
   */
  public void setDeleteDate(ch.ivyteam.ivy.scripting.objects.Date _deleteDate)
  {
    deleteDate = _deleteDate;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> rootFiles;

  /**
   * Gets the field rootFiles.
   * @return the value of the field rootFiles; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getRootFiles()
  {
    return rootFiles;
  }

  /**
   * Sets the field rootFiles.
   * @param _rootFiles the new value of the field rootFiles.
   */
  public void setRootFiles(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _rootFiles)
  {
    rootFiles = _rootFiles;
  }

  private java.lang.String Directory;

  /**
   * Gets the field Directory.
   * @return the value of the field Directory; may be null.
   */
  public java.lang.String getDirectory()
  {
    return Directory;
  }

  /**
   * Sets the field Directory.
   * @param _Directory the new value of the field Directory.
   */
  public void setDirectory(java.lang.String _Directory)
  {
    Directory = _Directory;
  }

}
