package ch.soreco.filehandling.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class FileSearch", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class FileSearch extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 8118758408844421385L;

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.String fileSuffix;

  /**
   * Gets the field fileSuffix.
   * @return the value of the field fileSuffix; may be null.
   */
  public java.lang.String getFileSuffix()
  {
    return fileSuffix;
  }

  /**
   * Sets the field fileSuffix.
   * @param _fileSuffix the new value of the field fileSuffix.
   */
  public void setFileSuffix(java.lang.String _fileSuffix)
  {
    fileSuffix = _fileSuffix;
  }

  private java.lang.String formerFileSuffix;

  /**
   * Gets the field formerFileSuffix.
   * @return the value of the field formerFileSuffix; may be null.
   */
  public java.lang.String getFormerFileSuffix()
  {
    return formerFileSuffix;
  }

  /**
   * Sets the field formerFileSuffix.
   * @param _formerFileSuffix the new value of the field formerFileSuffix.
   */
  public void setFormerFileSuffix(java.lang.String _formerFileSuffix)
  {
    formerFileSuffix = _formerFileSuffix;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> allFiles;

  /**
   * Gets the field allFiles.
   * @return the value of the field allFiles; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getAllFiles()
  {
    return allFiles;
  }

  /**
   * Sets the field allFiles.
   * @param _allFiles the new value of the field allFiles.
   */
  public void setAllFiles(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _allFiles)
  {
    allFiles = _allFiles;
  }

  private java.lang.String Directory;

  /**
   * Gets the field Directory.
   * @return the value of the field Directory; may be null.
   */
  public java.lang.String getDirectory()
  {
    return Directory;
  }

  /**
   * Sets the field Directory.
   * @param _Directory the new value of the field Directory.
   */
  public void setDirectory(java.lang.String _Directory)
  {
    Directory = _Directory;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> FileSet;

  /**
   * Gets the field FileSet.
   * @return the value of the field FileSet; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getFileSet()
  {
    return FileSet;
  }

  /**
   * Sets the field FileSet.
   * @param _FileSet the new value of the field FileSet.
   */
  public void setFileSet(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _FileSet)
  {
    FileSet = _FileSet;
  }

  private ch.soreco.orderbook.bo.ScanShare Share;

  /**
   * Gets the field Share.
   * @return the value of the field Share; may be null.
   */
  public ch.soreco.orderbook.bo.ScanShare getShare()
  {
    return Share;
  }

  /**
   * Sets the field Share.
   * @param _Share the new value of the field Share.
   */
  public void setShare(ch.soreco.orderbook.bo.ScanShare _Share)
  {
    Share = _Share;
  }

  private java.lang.Integer countSets;

  /**
   * Gets the field countSets.
   * @return the value of the field countSets; may be null.
   */
  public java.lang.Integer getCountSets()
  {
    return countSets;
  }

  /**
   * Sets the field countSets.
   * @param _countSets the new value of the field countSets.
   */
  public void setCountSets(java.lang.Integer _countSets)
  {
    countSets = _countSets;
  }

  private java.lang.Integer countErrors;

  /**
   * Gets the field countErrors.
   * @return the value of the field countErrors; may be null.
   */
  public java.lang.Integer getCountErrors()
  {
    return countErrors;
  }

  /**
   * Sets the field countErrors.
   * @param _countErrors the new value of the field countErrors.
   */
  public void setCountErrors(java.lang.Integer _countErrors)
  {
    countErrors = _countErrors;
  }

  private ch.soreco.orderbook.bo.ScanLog ScanLog;

  /**
   * Gets the field ScanLog.
   * @return the value of the field ScanLog; may be null.
   */
  public ch.soreco.orderbook.bo.ScanLog getScanLog()
  {
    return ScanLog;
  }

  /**
   * Sets the field ScanLog.
   * @param _ScanLog the new value of the field ScanLog.
   */
  public void setScanLog(ch.soreco.orderbook.bo.ScanLog _ScanLog)
  {
    ScanLog = _ScanLog;
  }

  private ch.soreco.common.bo.ErrorLog ErrorLog;

  /**
   * Gets the field ErrorLog.
   * @return the value of the field ErrorLog; may be null.
   */
  public ch.soreco.common.bo.ErrorLog getErrorLog()
  {
    return ErrorLog;
  }

  /**
   * Sets the field ErrorLog.
   * @param _ErrorLog the new value of the field ErrorLog.
   */
  public void setErrorLog(ch.soreco.common.bo.ErrorLog _ErrorLog)
  {
    ErrorLog = _ErrorLog;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> rootFiles;

  /**
   * Gets the field rootFiles.
   * @return the value of the field rootFiles; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getRootFiles()
  {
    return rootFiles;
  }

  /**
   * Sets the field rootFiles.
   * @param _rootFiles the new value of the field rootFiles.
   */
  public void setRootFiles(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _rootFiles)
  {
    rootFiles = _rootFiles;
  }

  private java.lang.Boolean isDossier;

  /**
   * Gets the field isDossier.
   * @return the value of the field isDossier; may be null.
   */
  public java.lang.Boolean getIsDossier()
  {
    return isDossier;
  }

  /**
   * Sets the field isDossier.
   * @param _isDossier the new value of the field isDossier.
   */
  public void setIsDossier(java.lang.Boolean _isDossier)
  {
    isDossier = _isDossier;
  }

  private ch.soreco.orderbook.bo.Order Order;

  /**
   * Gets the field Order.
   * @return the value of the field Order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return Order;
  }

  /**
   * Sets the field Order.
   * @param _Order the new value of the field Order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _Order)
  {
    Order = _Order;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> scannings;

  /**
   * Gets the field scannings.
   * @return the value of the field scannings; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> getScannings()
  {
    return scannings;
  }

  /**
   * Sets the field scannings.
   * @param _scannings the new value of the field scannings.
   */
  public void setScannings(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> _scannings)
  {
    scannings = _scannings;
  }

  private java.lang.String dirSuffix;

  /**
   * Gets the field dirSuffix.
   * @return the value of the field dirSuffix; may be null.
   */
  public java.lang.String getDirSuffix()
  {
    return dirSuffix;
  }

  /**
   * Sets the field dirSuffix.
   * @param _dirSuffix the new value of the field dirSuffix.
   */
  public void setDirSuffix(java.lang.String _dirSuffix)
  {
    dirSuffix = _dirSuffix;
  }

  private java.lang.String formerDirSuffix;

  /**
   * Gets the field formerDirSuffix.
   * @return the value of the field formerDirSuffix; may be null.
   */
  public java.lang.String getFormerDirSuffix()
  {
    return formerDirSuffix;
  }

  /**
   * Sets the field formerDirSuffix.
   * @param _formerDirSuffix the new value of the field formerDirSuffix.
   */
  public void setFormerDirSuffix(java.lang.String _formerDirSuffix)
  {
    formerDirSuffix = _formerDirSuffix;
  }

  private ch.soreco.filehandling.SmbHelper smbObject;

  /**
   * Gets the field smbObject.
   * @return the value of the field smbObject; may be null.
   */
  public ch.soreco.filehandling.SmbHelper getSmbObject()
  {
    return smbObject;
  }

  /**
   * Sets the field smbObject.
   * @param _smbObject the new value of the field smbObject.
   */
  public void setSmbObject(ch.soreco.filehandling.SmbHelper _smbObject)
  {
    smbObject = _smbObject;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> smbList;

  /**
   * Gets the field smbList.
   * @return the value of the field smbList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getSmbList()
  {
    return smbList;
  }

  /**
   * Sets the field smbList.
   * @param _smbList the new value of the field smbList.
   */
  public void setSmbList(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _smbList)
  {
    smbList = _smbList;
  }

}
