package ch.soreco.filehandling.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Archiving", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Archiving extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 728276483878506019L;

  private ch.soreco.orderbook.bo.Scanning scanning;

  /**
   * Gets the field scanning.
   * @return the value of the field scanning; may be null.
   */
  public ch.soreco.orderbook.bo.Scanning getScanning()
  {
    return scanning;
  }

  /**
   * Sets the field scanning.
   * @param _scanning the new value of the field scanning.
   */
  public void setScanning(ch.soreco.orderbook.bo.Scanning _scanning)
  {
    scanning = _scanning;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.Integer i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Integer getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Integer _i)
  {
    i = _i;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> scannings;

  /**
   * Gets the field scannings.
   * @return the value of the field scannings; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> getScannings()
  {
    return scannings;
  }

  /**
   * Sets the field scannings.
   * @param _scannings the new value of the field scannings.
   */
  public void setScannings(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Scanning> _scannings)
  {
    scannings = _scannings;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> orders;

  /**
   * Gets the field orders.
   * @return the value of the field orders; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getOrders()
  {
    return orders;
  }

  /**
   * Sets the field orders.
   * @param _orders the new value of the field orders.
   */
  public void setOrders(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _orders)
  {
    orders = _orders;
  }

  private ch.soreco.orderbook.bo.Order order;

  /**
   * Gets the field order.
   * @return the value of the field order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return order;
  }

  /**
   * Sets the field order.
   * @param _order the new value of the field order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _order)
  {
    order = _order;
  }

  private java.lang.Boolean archivingDurationOver;

  /**
   * Gets the field archivingDurationOver.
   * @return the value of the field archivingDurationOver; may be null.
   */
  public java.lang.Boolean getArchivingDurationOver()
  {
    return archivingDurationOver;
  }

  /**
   * Sets the field archivingDurationOver.
   * @param _archivingDurationOver the new value of the field archivingDurationOver.
   */
  public void setArchivingDurationOver(java.lang.Boolean _archivingDurationOver)
  {
    archivingDurationOver = _archivingDurationOver;
  }

  private ch.soreco.common.bo.File archivingFile;

  /**
   * Gets the field archivingFile.
   * @return the value of the field archivingFile; may be null.
   */
  public ch.soreco.common.bo.File getArchivingFile()
  {
    return archivingFile;
  }

  /**
   * Sets the field archivingFile.
   * @param _archivingFile the new value of the field archivingFile.
   */
  public void setArchivingFile(ch.soreco.common.bo.File _archivingFile)
  {
    archivingFile = _archivingFile;
  }

  private ch.ivyteam.ivy.scripting.objects.Recordset recordset;

  /**
   * Gets the field recordset.
   * @return the value of the field recordset; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.Recordset getRecordset()
  {
    return recordset;
  }

  /**
   * Sets the field recordset.
   * @param _recordset the new value of the field recordset.
   */
  public void setRecordset(ch.ivyteam.ivy.scripting.objects.Recordset _recordset)
  {
    recordset = _recordset;
  }

}
