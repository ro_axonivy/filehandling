package ch.soreco.filehandling.functional;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class FileImport", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class FileImport extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = 6824010706473195980L;

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> FileSet;

  /**
   * Gets the field FileSet.
   * @return the value of the field FileSet; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getFileSet()
  {
    return FileSet;
  }

  /**
   * Sets the field FileSet.
   * @param _FileSet the new value of the field FileSet.
   */
  public void setFileSet(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _FileSet)
  {
    FileSet = _FileSet;
  }

  private ch.soreco.common.bo.File File;

  /**
   * Gets the field File.
   * @return the value of the field File; may be null.
   */
  public ch.soreco.common.bo.File getFile()
  {
    return File;
  }

  /**
   * Sets the field File.
   * @param _File the new value of the field File.
   */
  public void setFile(ch.soreco.common.bo.File _File)
  {
    File = _File;
  }

  private java.lang.String fileSuffix;

  /**
   * Gets the field fileSuffix.
   * @return the value of the field fileSuffix; may be null.
   */
  public java.lang.String getFileSuffix()
  {
    return fileSuffix;
  }

  /**
   * Sets the field fileSuffix.
   * @param _fileSuffix the new value of the field fileSuffix.
   */
  public void setFileSuffix(java.lang.String _fileSuffix)
  {
    fileSuffix = _fileSuffix;
  }

  private java.lang.String formerFileSuffix;

  /**
   * Gets the field formerFileSuffix.
   * @return the value of the field formerFileSuffix; may be null.
   */
  public java.lang.String getFormerFileSuffix()
  {
    return formerFileSuffix;
  }

  /**
   * Sets the field formerFileSuffix.
   * @param _formerFileSuffix the new value of the field formerFileSuffix.
   */
  public void setFormerFileSuffix(java.lang.String _formerFileSuffix)
  {
    formerFileSuffix = _formerFileSuffix;
  }

  private ch.soreco.orderbook.bo.Order Order;

  /**
   * Gets the field Order.
   * @return the value of the field Order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return Order;
  }

  /**
   * Sets the field Order.
   * @param _Order the new value of the field Order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _Order)
  {
    Order = _Order;
  }

  private ch.soreco.orderbook.bo.OrderMatching OrderMatching;

  /**
   * Gets the field OrderMatching.
   * @return the value of the field OrderMatching; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getOrderMatching()
  {
    return OrderMatching;
  }

  /**
   * Sets the field OrderMatching.
   * @param _OrderMatching the new value of the field OrderMatching.
   */
  public void setOrderMatching(ch.soreco.orderbook.bo.OrderMatching _OrderMatching)
  {
    OrderMatching = _OrderMatching;
  }

  private ch.soreco.common.bo.ErrorLog ErrorLog;

  /**
   * Gets the field ErrorLog.
   * @return the value of the field ErrorLog; may be null.
   */
  public ch.soreco.common.bo.ErrorLog getErrorLog()
  {
    return ErrorLog;
  }

  /**
   * Sets the field ErrorLog.
   * @param _ErrorLog the new value of the field ErrorLog.
   */
  public void setErrorLog(ch.soreco.common.bo.ErrorLog _ErrorLog)
  {
    ErrorLog = _ErrorLog;
  }

  private java.lang.Boolean manualFlag;

  /**
   * Gets the field manualFlag.
   * @return the value of the field manualFlag; may be null.
   */
  public java.lang.Boolean getManualFlag()
  {
    return manualFlag;
  }

  /**
   * Sets the field manualFlag.
   * @param _manualFlag the new value of the field manualFlag.
   */
  public void setManualFlag(java.lang.Boolean _manualFlag)
  {
    manualFlag = _manualFlag;
  }

  private ch.soreco.orderbook.bo.ScanShare scanShare;

  /**
   * Gets the field scanShare.
   * @return the value of the field scanShare; may be null.
   */
  public ch.soreco.orderbook.bo.ScanShare getScanShare()
  {
    return scanShare;
  }

  /**
   * Sets the field scanShare.
   * @param _scanShare the new value of the field scanShare.
   */
  public void setScanShare(ch.soreco.orderbook.bo.ScanShare _scanShare)
  {
    scanShare = _scanShare;
  }

  private ch.soreco.filehandling.SmbHelper smbObject;

  /**
   * Gets the field smbObject.
   * @return the value of the field smbObject; may be null.
   */
  public ch.soreco.filehandling.SmbHelper getSmbObject()
  {
    return smbObject;
  }

  /**
   * Sets the field smbObject.
   * @param _smbObject the new value of the field smbObject.
   */
  public void setSmbObject(ch.soreco.filehandling.SmbHelper _smbObject)
  {
    smbObject = _smbObject;
  }

}
