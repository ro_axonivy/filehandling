package filehandling;

/**
 */
@SuppressWarnings("all")
@javax.annotation.Generated(comments="This is the java file of the ivy data class Data", value={"ch.ivyteam.ivy.scripting.streamInOut.IvyScriptJavaClassBuilder"})
public class Data extends ch.ivyteam.ivy.scripting.objects.CompositeObject
{
  /** SerialVersionUID */
  private static final long serialVersionUID = -5246412100901880509L;

  private ch.soreco.common.bo.File File;

  /**
   * Gets the field File.
   * @return the value of the field File; may be null.
   */
  public ch.soreco.common.bo.File getFile()
  {
    return File;
  }

  /**
   * Sets the field File.
   * @param _File the new value of the field File.
   */
  public void setFile(ch.soreco.common.bo.File _File)
  {
    File = _File;
  }

  private ch.soreco.orderbook.bo.Scanning Scan;

  /**
   * Gets the field Scan.
   * @return the value of the field Scan; may be null.
   */
  public ch.soreco.orderbook.bo.Scanning getScan()
  {
    return Scan;
  }

  /**
   * Sets the field Scan.
   * @param _Scan the new value of the field Scan.
   */
  public void setScan(ch.soreco.orderbook.bo.Scanning _Scan)
  {
    Scan = _Scan;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.io.File> FileSet;

  /**
   * Gets the field FileSet.
   * @return the value of the field FileSet; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.io.File> getFileSet()
  {
    return FileSet;
  }

  /**
   * Sets the field FileSet.
   * @param _FileSet the new value of the field FileSet.
   */
  public void setFileSet(ch.ivyteam.ivy.scripting.objects.List<java.io.File> _FileSet)
  {
    FileSet = _FileSet;
  }

  private ch.soreco.orderbook.bo.OrderMatching OrderMatching;

  /**
   * Gets the field OrderMatching.
   * @return the value of the field OrderMatching; may be null.
   */
  public ch.soreco.orderbook.bo.OrderMatching getOrderMatching()
  {
    return OrderMatching;
  }

  /**
   * Sets the field OrderMatching.
   * @param _OrderMatching the new value of the field OrderMatching.
   */
  public void setOrderMatching(ch.soreco.orderbook.bo.OrderMatching _OrderMatching)
  {
    OrderMatching = _OrderMatching;
  }

  private ch.soreco.orderbook.bo.Order Order;

  /**
   * Gets the field Order.
   * @return the value of the field Order; may be null.
   */
  public ch.soreco.orderbook.bo.Order getOrder()
  {
    return Order;
  }

  /**
   * Sets the field Order.
   * @param _Order the new value of the field Order.
   */
  public void setOrder(ch.soreco.orderbook.bo.Order _Order)
  {
    Order = _Order;
  }

  private java.lang.Boolean success;

  /**
   * Gets the field success.
   * @return the value of the field success; may be null.
   */
  public java.lang.Boolean getSuccess()
  {
    return success;
  }

  /**
   * Sets the field success.
   * @param _success the new value of the field success.
   */
  public void setSuccess(java.lang.Boolean _success)
  {
    success = _success;
  }

  private java.lang.String error;

  /**
   * Gets the field error.
   * @return the value of the field error; may be null.
   */
  public java.lang.String getError()
  {
    return error;
  }

  /**
   * Sets the field error.
   * @param _error the new value of the field error.
   */
  public void setError(java.lang.String _error)
  {
    error = _error;
  }

  private java.lang.String fileSuffix;

  /**
   * Gets the field fileSuffix.
   * @return the value of the field fileSuffix; may be null.
   */
  public java.lang.String getFileSuffix()
  {
    return fileSuffix;
  }

  /**
   * Sets the field fileSuffix.
   * @param _fileSuffix the new value of the field fileSuffix.
   */
  public void setFileSuffix(java.lang.String _fileSuffix)
  {
    fileSuffix = _fileSuffix;
  }

  private java.lang.String dirSuffix;

  /**
   * Gets the field dirSuffix.
   * @return the value of the field dirSuffix; may be null.
   */
  public java.lang.String getDirSuffix()
  {
    return dirSuffix;
  }

  /**
   * Sets the field dirSuffix.
   * @param _dirSuffix the new value of the field dirSuffix.
   */
  public void setDirSuffix(java.lang.String _dirSuffix)
  {
    dirSuffix = _dirSuffix;
  }

  private java.lang.String formerFileSuffix;

  /**
   * Gets the field formerFileSuffix.
   * @return the value of the field formerFileSuffix; may be null.
   */
  public java.lang.String getFormerFileSuffix()
  {
    return formerFileSuffix;
  }

  /**
   * Sets the field formerFileSuffix.
   * @param _formerFileSuffix the new value of the field formerFileSuffix.
   */
  public void setFormerFileSuffix(java.lang.String _formerFileSuffix)
  {
    formerFileSuffix = _formerFileSuffix;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.io.File> allFiles;

  /**
   * Gets the field allFiles.
   * @return the value of the field allFiles; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.io.File> getAllFiles()
  {
    return allFiles;
  }

  /**
   * Sets the field allFiles.
   * @param _allFiles the new value of the field allFiles.
   */
  public void setAllFiles(ch.ivyteam.ivy.scripting.objects.List<java.io.File> _allFiles)
  {
    allFiles = _allFiles;
  }

  private java.io.File Directory;

  /**
   * Gets the field Directory.
   * @return the value of the field Directory; may be null.
   */
  public java.io.File getDirectory()
  {
    return Directory;
  }

  /**
   * Sets the field Directory.
   * @param _Directory the new value of the field Directory.
   */
  public void setDirectory(java.io.File _Directory)
  {
    Directory = _Directory;
  }

  private ch.soreco.orderbook.bo.ScanShare Share;

  /**
   * Gets the field Share.
   * @return the value of the field Share; may be null.
   */
  public ch.soreco.orderbook.bo.ScanShare getShare()
  {
    return Share;
  }

  /**
   * Sets the field Share.
   * @param _Share the new value of the field Share.
   */
  public void setShare(ch.soreco.orderbook.bo.ScanShare _Share)
  {
    Share = _Share;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanShare> Shares;

  /**
   * Gets the field Shares.
   * @return the value of the field Shares; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanShare> getShares()
  {
    return Shares;
  }

  /**
   * Sets the field Shares.
   * @param _Shares the new value of the field Shares.
   */
  public void setShares(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.ScanShare> _Shares)
  {
    Shares = _Shares;
  }

  private java.lang.Boolean test;

  /**
   * Gets the field test.
   * @return the value of the field test; may be null.
   */
  public java.lang.Boolean getTest()
  {
    return test;
  }

  /**
   * Sets the field test.
   * @param _test the new value of the field test.
   */
  public void setTest(java.lang.Boolean _test)
  {
    test = _test;
  }

  private java.lang.String orderNr;

  /**
   * Gets the field orderNr.
   * @return the value of the field orderNr; may be null.
   */
  public java.lang.String getOrderNr()
  {
    return orderNr;
  }

  /**
   * Sets the field orderNr.
   * @param _orderNr the new value of the field orderNr.
   */
  public void setOrderNr(java.lang.String _orderNr)
  {
    orderNr = _orderNr;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> orderList;

  /**
   * Gets the field orderList.
   * @return the value of the field orderList; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getOrderList()
  {
    return orderList;
  }

  /**
   * Sets the field orderList.
   * @param _orderList the new value of the field orderList.
   */
  public void setOrderList(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _orderList)
  {
    orderList = _orderList;
  }

  private java.lang.Number i;

  /**
   * Gets the field i.
   * @return the value of the field i; may be null.
   */
  public java.lang.Number getI()
  {
    return i;
  }

  /**
   * Sets the field i.
   * @param _i the new value of the field i.
   */
  public void setI(java.lang.Number _i)
  {
    i = _i;
  }

  private ch.soreco.filehandling.SmbHelper smbobj;

  /**
   * Gets the field smbobj.
   * @return the value of the field smbobj; may be null.
   */
  public ch.soreco.filehandling.SmbHelper getSmbobj()
  {
    return smbobj;
  }

  /**
   * Sets the field smbobj.
   * @param _smbobj the new value of the field smbobj.
   */
  public void setSmbobj(ch.soreco.filehandling.SmbHelper _smbobj)
  {
    smbobj = _smbobj;
  }

  private ch.ivyteam.ivy.scripting.objects.List<java.lang.String> smblist;

  /**
   * Gets the field smblist.
   * @return the value of the field smblist; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<java.lang.String> getSmblist()
  {
    return smblist;
  }

  /**
   * Sets the field smblist.
   * @param _smblist the new value of the field smblist.
   */
  public void setSmblist(ch.ivyteam.ivy.scripting.objects.List<java.lang.String> _smblist)
  {
    smblist = _smblist;
  }

  private ch.soreco.orderbook.bo.ScanLog scanLog;

  /**
   * Gets the field scanLog.
   * @return the value of the field scanLog; may be null.
   */
  public ch.soreco.orderbook.bo.ScanLog getScanLog()
  {
    return scanLog;
  }

  /**
   * Sets the field scanLog.
   * @param _scanLog the new value of the field scanLog.
   */
  public void setScanLog(ch.soreco.orderbook.bo.ScanLog _scanLog)
  {
    scanLog = _scanLog;
  }

  private ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> messages;

  /**
   * Gets the field messages.
   * @return the value of the field messages; may be null.
   */
  public ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> getMessages()
  {
    return messages;
  }

  /**
   * Sets the field messages.
   * @param _messages the new value of the field messages.
   */
  public void setMessages(ch.ivyteam.ivy.scripting.objects.List<ch.soreco.orderbook.bo.Order> _messages)
  {
    messages = _messages;
  }

  private ch.soreco.orderbook.bo.Order masterOrder;

  /**
   * Gets the field masterOrder.
   * @return the value of the field masterOrder; may be null.
   */
  public ch.soreco.orderbook.bo.Order getMasterOrder()
  {
    return masterOrder;
  }

  /**
   * Sets the field masterOrder.
   * @param _masterOrder the new value of the field masterOrder.
   */
  public void setMasterOrder(ch.soreco.orderbook.bo.Order _masterOrder)
  {
    masterOrder = _masterOrder;
  }

}
