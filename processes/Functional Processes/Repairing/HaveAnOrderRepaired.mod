[Ivy]
[>Created: Thu Nov 10 09:57:03 CET 2011]
12D999D463AE115B 3.15 #module
>Proto >Proto Collection #zClass
Hd0 HaveAnOrderRepaired Big #zClass
Hd0 B #cInfo
Hd0 #process
Hd0 @TextInP .resExport .resExport #zField
Hd0 @TextInP .type .type #zField
Hd0 @TextInP .processKind .processKind #zField
Hd0 @AnnotationInP-0n ai ai #zField
Hd0 @TextInP .xml .xml #zField
Hd0 @TextInP .responsibility .responsibility #zField
Hd0 @StartSub f0 '' #zField
Hd0 @EndSub f1 '' #zField
Hd0 @GridStep f2 '' #zField
Hd0 @PushWFArc f3 '' #zField
Hd0 @PushWFArc f4 '' #zField
>Proto Hd0 Hd0 HaveAnOrderRepaired #zField
Hd0 f0 inParamDecl '<ch.soreco.orderbook.bo.Scanning scanning> param;' #txt
Hd0 f0 inParamTable 'out.scanning=param.scanning;
' #txt
Hd0 f0 outParamDecl '<> result;
' #txt
Hd0 f0 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
Hd0 f0 callSignature call(ch.soreco.orderbook.bo.Scanning) #txt
Hd0 f0 type ch.soreco.filehandling.functional.Archiving #txt
Hd0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Scanning)</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f0 91 35 26 26 14 0 #rect
Hd0 f0 @|StartSubIcon #fIcon
Hd0 f1 type ch.soreco.filehandling.functional.Archiving #txt
Hd0 f1 91 195 26 26 14 0 #rect
Hd0 f1 @|EndSubIcon #fIcon
Hd0 f2 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
Hd0 f2 actionTable 'out=in;
out.scanning.archivingState=ch.soreco.orderbook.enums.ArchivingState.IN_REPAIR.toString();
' #txt
Hd0 f2 type ch.soreco.filehandling.functional.Archiving #txt
Hd0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set substate to &quot;in repair&quot;</name>
        <nameStyle>27,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hd0 f2 86 108 36 24 20 -2 #rect
Hd0 f2 @|StepIcon #fIcon
Hd0 f2 -13016147|-1|-16777216 #nodeStyle
Hd0 f3 expr out #txt
Hd0 f3 104 61 104 108 #arcP
Hd0 f4 expr out #txt
Hd0 f4 104 132 104 195 #arcP
>Proto Hd0 .type ch.soreco.filehandling.functional.Archiving #txt
>Proto Hd0 .processKind CALLABLE_SUB #txt
>Proto Hd0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Hd0 0 0 32 24 18 0 #rect
>Proto Hd0 @|BIcon #fIcon
Hd0 f0 mainOut f3 tail #connect
Hd0 f3 head f2 mainIn #connect
Hd0 f2 mainOut f4 tail #connect
Hd0 f4 head f1 mainIn #connect
