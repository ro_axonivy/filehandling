[Ivy]
12C9CF48B0F2CA34 3.20 #module
>Proto >Proto Collection #zClass
Ft0 FileImport Big #zClass
Ft0 B #cInfo
Ft0 #process
Ft0 @TextInP .resExport .resExport #zField
Ft0 @TextInP .type .type #zField
Ft0 @TextInP .processKind .processKind #zField
Ft0 @AnnotationInP-0n ai ai #zField
Ft0 @TextInP .xml .xml #zField
Ft0 @TextInP .responsibility .responsibility #zField
Ft0 @StartSub f0 '' #zField
Ft0 @EndSub f1 '' #zField
Ft0 @GridStep f13 '' #zField
Ft0 @CallSub f16 '' #zField
Ft0 @GridStep f19 '' #zField
Ft0 @CallSub f7 '' #zField
Ft0 @Alternative f5 '' #zField
Ft0 @Alternative f27 '' #zField
Ft0 @PushWFArc f28 '' #zField
Ft0 @PushWFArc f26 '' #zField
Ft0 @GridStep f29 '' #zField
Ft0 @PushWFArc f31 '' #zField
Ft0 @GridStep f37 '' #zField
Ft0 @Alternative f40 '' #zField
Ft0 @PushWFArc f43 '' #zField
Ft0 @PushWFArc f22 '' #zField
Ft0 @GridStep f30 '' #zField
Ft0 @GridStep f44 '' #zField
Ft0 @Alternative f48 '' #zField
Ft0 @Alternative f4 '' #zField
Ft0 @PushWFArc f8 '' #zField
Ft0 @PushWFArc f11 '' #zField
Ft0 @PushWFArc f12 '' #zField
Ft0 @PushWFArc f6 '' #zField
Ft0 @StartSub f9 '' #zField
Ft0 @StartSub f20 '' #zField
Ft0 @EndSub f32 '' #zField
Ft0 @PushWFArc f34 '' #zField
Ft0 @PushWFArc f38 '' #zField
Ft0 @PushWFArc f3 '' #zField
Ft0 @PushWFArc f14 '' #zField
Ft0 @EndSub f17 '' #zField
Ft0 @PushWFArc f18 '' #zField
Ft0 @PushWFArc f21 '' #zField
Ft0 @PushWFArc f35 '' #zField
Ft0 @PushWFArc f24 '' #zField
Ft0 @PushWFArc f36 '' #zField
Ft0 @PushWFArc f41 '' #zField
Ft0 @GridStep f10 '' #zField
Ft0 @PushWFArc f15 '' #zField
Ft0 @PushWFArc f23 '' #zField
Ft0 @PushWFArc f2 '' #zField
>Proto Ft0 Ft0 FileImport #zField
Ft0 f0 inParamDecl '<List<ch.soreco.orderbook.bo.Scanning> Scannings> param;' #txt
Ft0 f0 inParamTable 'out.manualFlag=false;
out.Order.scannings=param.Scannings;
' #txt
Ft0 f0 outParamDecl '<java.lang.Boolean success,java.lang.String error> result;
' #txt
Ft0 f0 outParamTable 'result.success=in.success;
result.error=in.error;
' #txt
Ft0 f0 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f0 callSignature setOrder(List<ch.soreco.orderbook.bo.Scanning>) #txt
Ft0 f0 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setOrder(List&lt;Scanning&gt;)</name>
    </language>
</elementInfo>
' #txt
Ft0 f0 563 43 26 26 -47 -34 #rect
Ft0 f0 @|StartSubIcon #fIcon
Ft0 f1 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f1 563 307 26 26 14 0 #rect
Ft0 f1 @|EndSubIcon #fIcon
Ft0 f13 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f13 actionTable 'out=in;
' #txt
Ft0 f13 actionCode 'import org.w3c.dom.*;
import javax.xml.parsers.*;
import java.text.*;//SimpleDateFormat;
import ch.soreco.filehandling.SmbHelper;

String errorMessage = "";
in.success = true;
java.io.File xmlFile;

try	{
	SmbHelper smb = new SmbHelper(in.scanShare.NetPathUID, in.scanShare.NetPathPWD);

//get XML file of Fileset
	xmlFile = smb.getJavaFile(in.FileSet.get(0));
		
//create "Document" of the XML
		Document doc = DocumentBuilderFactory
					.newInstance()
					.newDocumentBuilder()
					.parse(xmlFile);
		doc.getDocumentElement().normalize();
	
//get Date
		String dateString;
		try {
			Node dateNode = (doc.getElementsByTagName("DATE")
														.item(0) as Element)
														.getChildNodes().item(0);
			dateString = dateNode.getNodeValue().trim();
			if (dateString.length()>0) {
					DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
					Date scanDate = dateFormat.parse(dateString);     
				if (scanDate.getYear()<9999 && scanDate.getYear()>1800) {
					out.Order.scanning.scanDate = scanDate;
				}
				//Error: incorrect Date
				else {
					out.error = "The value ''DATE'' in XML-file \""+xmlFile.getName()+"\" is incorrect.";
					ivy.log.error(in.error);
					out.success = false;
				}
			}
			//Error: no value
			else {
				out.error = "Cannot find a value ''DATE'' in XML-file \""+xmlFile.getName()+"\".";
				ivy.log.error(in.error);
				out.success = false;
			}
		}
		//Error: parse value
		catch (ParseException e) {
			out.error = "Cannot cast the value \""+dateString+"\" as Date.\n"+
									"Reading ''DATE'' from XML-file \""+xmlFile.getName()+"\".";
			ivy.log.error(in.error,e);
			out.success = false;
		}
		//Error: read Node
		catch (Exception e) {
			out.error = "Cannot read ''DATE'' from XML-file \""+xmlFile.getName()+"\".";
			ivy.log.error(in.error,e);
			out.success = false;
		}

		
//get Time
		if (in.success) {		//do nothing if DATE throw an error
		String timeString;
		try {
			Node timeNode = (doc.getElementsByTagName("TIME")
														.item(0) as Element)
														.getChildNodes().item(0);
			timeString = timeNode.getNodeValue().trim();
			if (timeString.length()>0) {
				DateFormat timeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				DateTime scanTime = timeFormat.parse(dateString+timeString);
				out.Order.scanning.scanTime = scanTime;
			}
			//Error: no value
			else {
				out.error = "Cannot find a value ''TIME'' in XML-file \""+xmlFile.getName()+"\"";
				ivy.log.error(in.error);
				in.success = false;
			}
		}
		//Error: parse value
		catch (ParseException e) {
			out.error = "Cannot cast the value \""+dateString+timeString+"\" as DateTime. \n"+
			"Reading ''TIME'' from XML-file \""+xmlFile.getName()+"\"";
			ivy.log.error(in.error,e);
			in.success = false;
		}
		//Error: read Node
		catch (Exception e) {
			out.error = "Cannot read ''TIME'' from XML-file \""+xmlFile.getName()+"\"";
			ivy.log.error(in.error,e);
			in.success = false;
		}}
	
		
//get Doctype
		String doctypeString;
		try {
			Node doctypeNode = (doc.getElementsByTagName("DOCTYPE")
														.item(0) as Element)
														.getChildNodes().item(0);
			doctypeString = doctypeNode.getNodeValue().trim();
			if (doctypeString.length()>0){
				out.Order.scanning.docType = doctypeString;
			}
			//Error: no value
			else {
				out.error = "Cannot find a value ''DOCTYPE'' in XML-file \""+xmlFile.getName()+"\"";
				ivy.log.error(in.error);
				in.success = false;
			}
		}
		//Error: read Node
		catch (Exception e) {
			out.error = "Cannot read ''DOCTYPE'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.error(in.error,e);
			in.success = false;
		}
		
		
//get Dossier
		try {
			Node dossierNode = (doc.getElementsByTagName("DOSSIER")
														.item(0) as Element)
														.getChildNodes().item(0);
			if (dossierNode.getNodeValue().trim().equalsIgnoreCase("yes")) {
				out.Order.scanning.isDossier = true;
			}
			else {
				out.Order.scanning.isDossier = false;			
			};
		}
		//Warn: cannot read Node
		catch (Exception e) {
			out.error = "Cannot read ''DOCTYPE'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.warn(in.error,e);
			out.Order.scanning.isDossier = false;
		}
		
//get scanid
		String scanIdString;
		try{
			Node scanidNode = (doc.getElementsByTagName("SCANID")
														.item(0) as Element)
														.getChildNodes().item(0);
			scanIdString =  scanidNode.getNodeValue().trim();
			if (scanIdString.length()>0) {
				out.Order.scanning.scanId = scanIdString;
			}
			//Error: no value
			else {
				out.error = "Cannot find a value ''SCANID'' in XML-file \""+xmlFile.getName()+"\"";
				ivy.log.error(in.error);
				in.success = false;
			}
		}
		//Error: read Node
		catch (Exception e) {
			out.error = "Cannot read ''SCANID'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.error(in.error,e);
			in.success = false;
		}
		
//get nrofpages
		try {
			Node nrofpagesNode = (doc.getElementsByTagName("NUMBERofPAGES")
														.item(0) as Element)
														.getChildNodes().item(0);
			out.Order.scanning.numberOfPages =  nrofpagesNode.getNodeValue().trim().toNumber();
		}
		//Error: read Node or cast number
		catch (Exception e) {
			out.error = "Cannot read ''NUMBERofPAGES'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.warn(in.error,e);
		}
		
//get noofpagesnet
		try {
			Node noofpagesnetNode = (doc.getElementsByTagName("NUMBERofPAGESnet")
														.item(0) as Element)
														.getChildNodes().item(0);
			out.Order.scanning.numberOfPagesNet =  noofpagesnetNode.getNodeValue().trim().toNumber();
		}
		//Error: read Node or cast number
		catch (Exception e) {
			out.error = "Cannot read ''NUMBERofPAGESnet'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.warn(in.error,e);
		}

		
//get pagenostart
		try {
			Node pagenostartNode = (doc.getElementsByTagName("PAGNOstart")
														.item(0) as Element)
														.getChildNodes().item(0);
			out.Order.scanning.pageNoStart =  pagenostartNode.getNodeValue().trim().toNumber();
		}
		//Error: read Node or cast number
		catch (Exception e) {
			out.error = "Cannot read ''PAGNOstart'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.error(in.error,e);
			in.success = false;
		}
		
//get pagenoend
		try {
			Node pagenoendNode = (doc.getElementsByTagName("PAGNOend")
														.item(0) as Element)
														.getChildNodes().item(0);
			out.Order.scanning.pageNoEnd =  pagenoendNode.getNodeValue().trim().toNumber();
		}
		//Error: read Node or cast number
		catch (Exception e) {
			out.error = "Cannot read ''PAGNOend'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.warn(in.error,e);
		}
		
		
//get contentofdm
		try {
			Node contentofdmNode = (doc.getElementsByTagName("CONTENTofDM")
															.item(0) as Element)
															.getChildNodes().item(0);
			if (contentofdmNode != null) {
				out.Order.scanning.contentOfDM =  contentofdmNode.nodeValue.trim();
			}
		}
		//Error: read Node or cast number
		catch (Exception e) {
			out.error = "Cannot read ''CONTENTofDM'' from XML-file "+xmlFile.getName()+"\"";
			ivy.log.warn(in.error,e);
		}
	

//get document number
		out.Order.scanning.docNo = xmlFile.getName().substring(14,20);


//delete File from FileSystem
		try {
			if (!xmlFile.delete()) {
				out.error = "Cannot delete the XML-file "+xmlFile.getName()+" from the local filesystem.";
				ivy.log.warn(in.error);
			}
		} catch (SecurityException e) {
				out.error = "Cannot delete the XML-file "+xmlFile.getName()+" from the local filesystem.";
				ivy.log.warn(in.error,e);
		}
	}

//Error: read XML
	catch( Exception e )
	{
		out.error = "Cannot read the XML-file "+xmlFile.getName()+":\n";
		ivy.log.error(in.error,e);
		out.success = false;
	} 
' #txt
Ft0 f13 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>readXML</name>
        <nameStyle>7,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f13 78 196 36 24 20 -2 #rect
Ft0 f13 @|StepIcon #fIcon
Ft0 f13 -13016147|-1|-16777216 #nodeStyle
Ft0 f16 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f16 processCall 'Functional Processes/orderbook/Order:set(ch.soreco.orderbook.bo.Order)' #txt
Ft0 f16 doCall true #txt
Ft0 f16 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Ft0 f16 requestMappingAction 'param.order=in.Order;
' #txt
Ft0 f16 responseActionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f16 responseMappingAction 'out=in;
out.error=result.error;
out.success=result.success;
' #txt
Ft0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>setOrder</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f16 558 188 36 24 20 -2 #rect
Ft0 f16 @|CallSubIcon #fIcon
Ft0 f19 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f19 actionTable 'out=in;
' #txt
Ft0 f19 actionCode 'import jcifs.smb.SmbFile;
import ch.soreco.filehandling.SmbHelper;

java.io.File target;
java.io.File targetMono;

try {
	SmbHelper smb = new SmbHelper(in.scanShare.NetPathUID, in.scanShare.NetPathPWD);

	//set fileUri''s
	target = new java.io.File(smb.getName(in.FileSet.get(1)).replace("_PENDING",""));
	targetMono = new java.io.File(smb.getName(in.FileSet.get(2)).replace("_PENDING",""));

	//copy File to target
	java.io.File setFile = smb.getJavaFile(in.FileSet.get(1), target);
	java.io.File setFileMono = smb.getJavaFile(in.FileSet.get(2), targetMono);

	//set data
	out.Order.scanning.scanFile = setFile.getAbsolutePath();	
	out.Order.scanning.scanMonoFile = setFileMono.getAbsolutePath();
	out.success = true;

}
catch (Exception e) {
	out.error = "Cannot found or read complete FileSet of "+target.getName()+".";
	ivy.log.error(in.error,e);
	out.success = false;
}' #txt
Ft0 f19 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f19 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Scan Data</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f19 78 260 36 24 20 -2 #rect
Ft0 f19 @|StepIcon #fIcon
Ft0 f7 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f7 processCall 'Functional Processes/scanning/OrderMatching:get(ch.soreco.orderbook.bo.OrderMatching)' #txt
Ft0 f7 doCall true #txt
Ft0 f7 requestActionDecl '<ch.soreco.orderbook.bo.OrderMatching filter> param;
' #txt
Ft0 f7 requestMappingAction 'param.filter.docType=in.Order.scanning.docType;
' #txt
Ft0 f7 responseActionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f7 responseMappingAction 'out=in;
out.error=result.error;
out.Order.orderState=in.OrderMatching.stateAfterCreate;
out.Order.scanning.orderMatching=result.orderMatching;
out.Order.scanning.orderType=result.orderMatching.orderType;
out.OrderMatching=result.orderMatching;
out.success=result.success;
' #txt
Ft0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>auto Dispatch</name>
        <nameStyle>13
</nameStyle>
        <desc>05.08.2011 PB / ChangeRequest 57:
orderState after create will be set trough orderMatching</desc>
    </language>
</elementInfo>
' #txt
Ft0 f7 78 372 36 24 20 -2 #rect
Ft0 f7 @|CallSubIcon #fIcon
Ft0 f5 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>File exist?</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f5 218 258 28 28 14 0 #rect
Ft0 f5 @|AlternativeIcon #fIcon
Ft0 f27 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>MonoFile exist?</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f27 218 306 28 28 14 0 #rect
Ft0 f27 @|AlternativeIcon #fIcon
Ft0 f28 expr in #txt
Ft0 f28 outCond in.Order.scanning.scanFile.length()>0 #txt
Ft0 f28 232 286 232 306 #arcP
Ft0 f26 expr out #txt
Ft0 f26 114 272 218 272 #arcP
Ft0 f26 0 0.11179514998860299 0 0 #arcLabel
Ft0 f29 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f29 actionTable 'out=in;
' #txt
Ft0 f29 actionCode 'out.success = false;' #txt
Ft0 f29 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Error</name>
        <nameStyle>5
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f29 334 564 36 24 -72 -11 #rect
Ft0 f29 @|StepIcon #fIcon
Ft0 f29 -13016147|-1|-16777216 #nodeStyle
Ft0 f31 expr in #txt
Ft0 f31 246 272 370 567 #arcP
Ft0 f31 1 400 272 #addKink
Ft0 f31 2 400 552 #addKink
Ft0 f31 1 0.21191524211789384 0 0 #arcLabel
Ft0 f37 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f37 actionTable 'out=in;
out.Order.orderStart=new DateTime();
' #txt
Ft0 f37 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Order Data</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f37 558 148 36 24 20 -2 #rect
Ft0 f37 @|StepIcon #fIcon
Ft0 f40 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f40 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>XML ok?</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f40 218 194 28 28 14 0 #rect
Ft0 f40 @|AlternativeIcon #fIcon
Ft0 f43 expr in #txt
Ft0 f43 246 208 370 572 #arcP
Ft0 f43 1 416 208 #addKink
Ft0 f43 2 416 560 #addKink
Ft0 f43 1 0.08682629121659999 0 0 #arcLabel
Ft0 f22 expr in #txt
Ft0 f22 246 320 364 564 #arcP
Ft0 f22 1 384 320 #addKink
Ft0 f22 2 384 544 #addKink
Ft0 f22 1 0.04351083911987225 0 0 #arcLabel
Ft0 f30 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f30 actionTable 'out=in;
' #txt
Ft0 f30 actionCode 'import ch.soreco.filehandling.SmbHelper;

String fileName;
String fileRename;

try {

//Declaration
	SmbHelper smb = new SmbHelper(in.scanShare.NetPathUID, in.scanShare.NetPathPWD);

	for (String setFile:in.FileSet) {
		fileName = setFile;		
		String extension = fileName.substring(fileName.lastIndexOf(".")+1);
		fileRename = fileName.substring(0,fileName.lastIndexOf(".")) + in.fileSuffix + fileName.substring(fileName.lastIndexOf("."));
		fileRename = fileRename.replace(extension.toUpperCase(),extension.toLowerCase());

		smb.renameTo(fileName,fileRename);
		in.FileSet.set(in.FileSet.indexOf(setFile),fileRename);
		out.success = true;
	}
}
catch (Exception e) {
	out.error = "Cannot rename file "+fileName+" to "+fileRename+":\n"+
							e.getMessage();
	ivy.log.error(in.error,e);
	out.success = false;
}' #txt
Ft0 f30 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>rename Files</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f30 78 132 36 24 20 -2 #rect
Ft0 f30 @|StepIcon #fIcon
Ft0 f30 -13016147|-1|-16777216 #nodeStyle
Ft0 f44 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f44 actionTable 'out=in;
out.ErrorLog.ErrorDetail="";
out.ErrorLog.ErrorMassage="";
out.ErrorLog.LogObject="";
out.fileSuffix="_PENDING";
out.Order.scanning.scanShare=in.scanShare;
' #txt
Ft0 f44 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f44 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f44 214 84 36 24 20 -2 #rect
Ft0 f44 @|StepIcon #fIcon
Ft0 f48 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f48 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Dispatching ok?</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f48 218 370 28 28 14 0 #rect
Ft0 f48 @|AlternativeIcon #fIcon
Ft0 f4 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Set complete</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f4 218 130 28 28 12 0 #rect
Ft0 f4 @|AlternativeIcon #fIcon
Ft0 f8 expr in #txt
Ft0 f8 outCond in.success #txt
Ft0 f8 232 158 96 196 #arcP
Ft0 f8 1 232 176 #addKink
Ft0 f8 2 96 176 #addKink
Ft0 f8 2 0.07852576381215823 0 0 #arcLabel
Ft0 f11 expr out #txt
Ft0 f11 114 144 218 144 #arcP
Ft0 f12 expr out #txt
Ft0 f12 214 96 96 132 #arcP
Ft0 f12 1 96 96 #addKink
Ft0 f12 0 0.6933715339528196 0 0 #arcLabel
Ft0 f6 expr out #txt
Ft0 f6 114 208 218 208 #arcP
Ft0 f9 inParamDecl '<List<java.io.File> FileSet> param;' #txt
Ft0 f9 inParamTable 'out.manualFlag=true;
' #txt
Ft0 f9 outParamDecl '<java.lang.Boolean success,java.lang.String error> result;
' #txt
Ft0 f9 outParamTable 'result.success=in.success;
result.error=in.error;
' #txt
Ft0 f9 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f9 callSignature ManualImport(List<java.io.File>) #txt
Ft0 f9 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>ManualImport(List&lt;File&gt;)</name>
        <nameStyle>24,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f9 19 35 26 26 -32 -35 #rect
Ft0 f9 @|StartSubIcon #fIcon
Ft0 f20 inParamDecl '<ch.soreco.orderbook.bo.ScanShare share,List<java.lang.String> FileSet> param;' #txt
Ft0 f20 inParamTable 'out.FileSet=param.FileSet;
out.scanShare=param.share;
' #txt
Ft0 f20 outParamDecl '<ch.soreco.orderbook.bo.Scanning scanning,java.lang.Boolean success,java.lang.String error> result;
' #txt
Ft0 f20 outParamTable 'result.scanning=in.Order.scanning;
result.success=in.success;
result.error=in.error;
' #txt
Ft0 f20 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f20 callSignature initializeScanning(ch.soreco.orderbook.bo.ScanShare,List<String>) #txt
Ft0 f20 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>initializeScanning(ScanShare,List&lt;String&gt;)</name>
    </language>
</elementInfo>
' #txt
Ft0 f20 219 35 26 26 -60 -35 #rect
Ft0 f20 @|StartSubIcon #fIcon
Ft0 f32 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f32 219 563 26 26 14 0 #rect
Ft0 f32 @|EndSubIcon #fIcon
Ft0 f34 expr out #txt
Ft0 f34 232 61 232 84 #arcP
Ft0 f38 expr out #txt
Ft0 f38 114 384 218 384 #arcP
Ft0 f3 expr out #txt
Ft0 f3 3 #arcStyle
Ft0 f3 334 576 245 576 #arcP
Ft0 f3 0 0.7562893061826238 0 0 #arcLabel
Ft0 f14 expr in #txt
Ft0 f14 246 144 370 576 #arcP
Ft0 f14 1 432 144 #addKink
Ft0 f14 2 432 576 #addKink
Ft0 f14 0 0.7486700605498562 0 0 #arcLabel
Ft0 f17 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f17 19 283 26 26 14 0 #rect
Ft0 f17 @|EndSubIcon #fIcon
Ft0 f18 expr out #txt
Ft0 f18 32 61 32 283 #arcP
Ft0 f18 0 0.617289444507387 0 0 #arcLabel
Ft0 f21 expr in #txt
Ft0 f21 outCond in.Order.scanning.scanMonoFile.length()>0 #txt
Ft0 f21 218 320 96 372 #arcP
Ft0 f21 1 96 320 #addKink
Ft0 f21 1 0.6442530172632681 0 0 #arcLabel
Ft0 f35 expr out #txt
Ft0 f35 576 172 576 188 #arcP
Ft0 f24 expr in #txt
Ft0 f24 outCond in.success #txt
Ft0 f24 232 222 96 260 #arcP
Ft0 f24 1 232 240 #addKink
Ft0 f24 2 96 240 #addKink
Ft0 f24 1 0.7699882118722527 0 0 #arcLabel
Ft0 f36 expr in #txt
Ft0 f36 246 384 357 564 #arcP
Ft0 f36 1 368 384 #addKink
Ft0 f36 2 368 536 #addKink
Ft0 f36 0 0.8215003205045615 0 0 #arcLabel
Ft0 f41 expr in #txt
Ft0 f41 outCond in.success #txt
Ft0 f41 232 398 232 563 #arcP
Ft0 f41 0 0.7694180157440428 0 0 #arcLabel
Ft0 f10 actionDecl 'ch.soreco.filehandling.functional.FileImport out;
' #txt
Ft0 f10 actionTable 'out=in;
' #txt
Ft0 f10 actionCode 'import ch.soreco.orderbook.bo.Scanning;

List<Scanning> finalScanningList = new List<Scanning>() ;

if(in.Order.scannings.size() > 0){
	Scanning scan = in.Order.scannings.get(0) as Scanning;
	if(scan.orderMatching.orderPriority > 0){
		in.Order.priority = scan.orderMatching.orderPriority;
	}
	in.Order.orderState = scan.orderMatching.stateAfterCreate;
	

	for(Scanning cScan : in.Order.scannings){

		if(cScan.orderMatching.mapToDocType.length() > 0){
			cScan.docType = cScan.orderMatching.mapToDocType;
		}
		finalScanningList.add(cScan);
	}
	in.Order.scannings = finalScanningList;
}


' #txt
Ft0 f10 type ch.soreco.filehandling.functional.FileImport #txt
Ft0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set orderPriority</name>
        <nameStyle>17
</nameStyle>
    </language>
</elementInfo>
' #txt
Ft0 f10 558 92 36 24 20 -2 #rect
Ft0 f10 @|StepIcon #fIcon
Ft0 f15 expr out #txt
Ft0 f15 576 116 576 148 #arcP
Ft0 f23 expr out #txt
Ft0 f23 576 69 576 92 #arcP
Ft0 f2 expr out #txt
Ft0 f2 576 212 576 307 #arcP
>Proto Ft0 .type ch.soreco.filehandling.functional.FileImport #txt
>Proto Ft0 .processKind CALLABLE_SUB #txt
>Proto Ft0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Ft0 0 0 32 24 18 0 #rect
>Proto Ft0 @|BIcon #fIcon
Ft0 f5 out f28 tail #connect
Ft0 f28 head f27 in #connect
Ft0 f19 mainOut f26 tail #connect
Ft0 f26 head f5 in #connect
Ft0 f5 out f31 tail #connect
Ft0 f31 head f29 mainIn #connect
Ft0 f43 head f29 mainIn #connect
Ft0 f22 head f29 mainIn #connect
Ft0 f4 out f8 tail #connect
Ft0 f8 head f13 mainIn #connect
Ft0 f30 mainOut f11 tail #connect
Ft0 f11 head f4 in #connect
Ft0 f44 mainOut f12 tail #connect
Ft0 f12 head f30 mainIn #connect
Ft0 f13 mainOut f6 tail #connect
Ft0 f6 head f40 in #connect
Ft0 f20 mainOut f34 tail #connect
Ft0 f34 head f44 mainIn #connect
Ft0 f7 mainOut f38 tail #connect
Ft0 f38 head f48 in #connect
Ft0 f29 mainOut f3 tail #connect
Ft0 f3 head f32 mainIn #connect
Ft0 f4 out f14 tail #connect
Ft0 f14 head f29 mainIn #connect
Ft0 f9 mainOut f18 tail #connect
Ft0 f18 head f17 mainIn #connect
Ft0 f27 out f21 tail #connect
Ft0 f21 head f7 mainIn #connect
Ft0 f27 out f22 tail #connect
Ft0 f37 mainOut f35 tail #connect
Ft0 f35 head f16 mainIn #connect
Ft0 f40 out f24 tail #connect
Ft0 f24 head f19 mainIn #connect
Ft0 f40 out f43 tail #connect
Ft0 f36 head f29 mainIn #connect
Ft0 f48 out f41 tail #connect
Ft0 f41 head f32 mainIn #connect
Ft0 f48 out f36 tail #connect
Ft0 f10 mainOut f15 tail #connect
Ft0 f15 head f37 mainIn #connect
Ft0 f0 mainOut f23 tail #connect
Ft0 f23 head f10 mainIn #connect
Ft0 f16 mainOut f2 tail #connect
Ft0 f2 head f1 mainIn #connect
