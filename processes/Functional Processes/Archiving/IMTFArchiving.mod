[Ivy]
[>Created: Thu Nov 10 09:56:57 CET 2011]
12D983C3E7880069 3.15 #module
>Proto >Proto Collection #zClass
Ig0 IMTFArchiving Big #zClass
Ig0 B #cInfo
Ig0 #process
Ig0 @TextInP .resExport .resExport #zField
Ig0 @TextInP .type .type #zField
Ig0 @TextInP .processKind .processKind #zField
Ig0 @AnnotationInP-0n ai ai #zField
Ig0 @TextInP .xml .xml #zField
Ig0 @TextInP .responsibility .responsibility #zField
Ig0 @StartSub f0 '' #zField
Ig0 @EndSub f1 '' #zField
Ig0 @GridStep f4 '' #zField
Ig0 @PushWFArc f6 '' #zField
Ig0 @PushWFArc f2 '' #zField
>Proto Ig0 Ig0 IMTFArchiving #zField
Ig0 f0 inParamDecl '<ch.soreco.orderbook.bo.Scanning scanning> param;' #txt
Ig0 f0 inParamTable 'out.scanning=param.scanning;
' #txt
Ig0 f0 outParamDecl '<> result;
' #txt
Ig0 f0 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
Ig0 f0 callSignature call(ch.soreco.orderbook.bo.Scanning) #txt
Ig0 f0 type ch.soreco.filehandling.functional.Archiving #txt
Ig0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Scanning)</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ig0 f0 227 163 26 26 14 0 #rect
Ig0 f0 @|StartSubIcon #fIcon
Ig0 f1 type ch.soreco.filehandling.functional.Archiving #txt
Ig0 f1 227 307 26 26 14 0 #rect
Ig0 f1 @|EndSubIcon #fIcon
Ig0 f4 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
Ig0 f4 actionTable 'out=in;
out.scanning.archivingState=ch.soreco.orderbook.enums.ArchivingState.IMTF_ARCHIVED.toString();
' #txt
Ig0 f4 actionCode ivy.persistence.Adressmutation.merge(in.scanning); #txt
Ig0 f4 type ch.soreco.filehandling.functional.Archiving #txt
Ig0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Set substate to &quot;IMTF_Archived&quot;</name>
        <nameStyle>31,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ig0 f4 222 236 36 24 20 -2 #rect
Ig0 f4 @|StepIcon #fIcon
Ig0 f4 -13016147|-1|-16777216 #nodeStyle
Ig0 f6 expr out #txt
Ig0 f6 240 260 240 307 #arcP
Ig0 f2 expr out #txt
Ig0 f2 240 189 240 236 #arcP
>Proto Ig0 .type ch.soreco.filehandling.functional.Archiving #txt
>Proto Ig0 .processKind CALLABLE_SUB #txt
>Proto Ig0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Ig0 0 0 32 24 18 0 #rect
>Proto Ig0 @|BIcon #fIcon
Ig0 f4 mainOut f6 tail #connect
Ig0 f6 head f1 mainIn #connect
Ig0 f0 mainOut f2 tail #connect
Ig0 f2 head f4 mainIn #connect
