[Ivy]
[>Created: Thu Nov 10 09:55:48 CET 2011]
12DB86D2C6A4991C 3.15 #module
>Proto >Proto Collection #zClass
fg0 filterOrdersForIvyArchiving Big #zClass
fg0 B #cInfo
fg0 #process
fg0 @TextInP .resExport .resExport #zField
fg0 @TextInP .type .type #zField
fg0 @TextInP .processKind .processKind #zField
fg0 @AnnotationInP-0n ai ai #zField
fg0 @TextInP .xml .xml #zField
fg0 @TextInP .responsibility .responsibility #zField
fg0 @StartSub f0 '' #zField
fg0 @EndSub f1 '' #zField
fg0 @CallSub f5 '' #zField
fg0 @CallSub f14 '' #zField
fg0 @Alternative f10 '' #zField
fg0 @GridStep f16 '' #zField
fg0 @GridStep f6 '' #zField
fg0 @PushWFArc f11 '' #zField
fg0 @PushWFArc f17 '' #zField
fg0 @PushWFArc f19 '' #zField
fg0 @CallSub f3 '' #zField
fg0 @PushWFArc f4 '' #zField
fg0 @CallSub f12 '' #zField
fg0 @PushWFArc f13 '' #zField
fg0 @PushWFArc f8 '' #zField
fg0 @Alternative f9 '' #zField
fg0 @PushWFArc f18 '' #zField
fg0 @PushWFArc f15 '' #zField
fg0 @PushWFArc f20 '' #zField
fg0 @GridStep f23 '' #zField
fg0 @GridStep f21 '' #zField
fg0 @GridStep f28 '' #zField
fg0 @CallSub f26 '' #zField
fg0 @Alternative f22 '' #zField
fg0 @PushWFArc f24 '' #zField
fg0 @PushWFArc f25 '' #zField
fg0 @PushWFArc f27 '' #zField
fg0 @PushWFArc f29 '' #zField
fg0 @PushWFArc f30 '' #zField
fg0 @PushWFArc f31 '' #zField
fg0 @PushWFArc f7 '' #zField
fg0 @PushWFArc f2 '' #zField
>Proto fg0 fg0 filterOrdersForIvyArchiving #zField
fg0 f0 outParamDecl '<> result;
' #txt
fg0 f0 actionDecl 'filehandling.Data out;
' #txt
fg0 f0 callSignature call() #txt
fg0 f0 type filehandling.Data #txt
fg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
fg0 f0 523 43 26 26 14 0 #rect
fg0 f0 @|StartSubIcon #fIcon
fg0 f1 type filehandling.Data #txt
fg0 f1 523 939 26 26 14 0 #rect
fg0 f1 @|EndSubIcon #fIcon
fg0 f5 type filehandling.Data #txt
fg0 f5 processCall 'Functional Processes/orderbook/OrderBook:call(ch.soreco.orderbook.bo.Order)' #txt
fg0 f5 doCall true #txt
fg0 f5 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fg0 f5 requestMappingAction 'param.filter.orderState=ch.soreco.orderbook.enums.OrderState.Unidentified.toString();
' #txt
fg0 f5 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f5 responseMappingAction 'out=in;
out.orderList=in.orderList.addAll(result.orderBook);
' #txt
fg0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Orders in State Unidentified</name>
        <nameStyle>32,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f5 518 108 36 24 20 -2 #rect
fg0 f5 @|CallSubIcon #fIcon
fg0 f14 type filehandling.Data #txt
fg0 f14 processCall 'Functional Processes/Archiving/checkForIvyArchiving:call(ch.soreco.orderbook.bo.Order)' #txt
fg0 f14 doCall true #txt
fg0 f14 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
fg0 f14 requestMappingAction 'param.order=in.Order;
' #txt
fg0 f14 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f14 responseMappingAction 'out=in;
' #txt
fg0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
        <nameStyle>11,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f14 518 796 36 24 20 -2 #rect
fg0 f14 @|CallSubIcon #fIcon
fg0 f10 type filehandling.Data #txt
fg0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Orders i from Orderlist</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f10 522 626 28 28 15 0 #rect
fg0 f10 @|AlternativeIcon #fIcon
fg0 f16 actionDecl 'filehandling.Data out;
' #txt
fg0 f16 actionTable 'out=in;
out.i=in.i + 1;
' #txt
fg0 f16 type filehandling.Data #txt
fg0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f16 518 852 36 24 20 -2 #rect
fg0 f16 @|StepIcon #fIcon
fg0 f6 actionDecl 'filehandling.Data out;
' #txt
fg0 f6 actionTable 'out=in;
out.i=0;
' #txt
fg0 f6 type filehandling.Data #txt
fg0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f6 518 572 36 24 20 -2 #rect
fg0 f6 @|StepIcon #fIcon
fg0 f11 expr out #txt
fg0 f11 536 596 536 626 #arcP
fg0 f17 expr out #txt
fg0 f17 536 820 536 852 #arcP
fg0 f19 expr out #txt
fg0 f19 536 876 522 640 #arcP
fg0 f19 1 536 896 #addKink
fg0 f19 2 480 896 #addKink
fg0 f19 3 480 640 #addKink
fg0 f19 2 0.6293992350061263 0 0 #arcLabel
fg0 f3 type filehandling.Data #txt
fg0 f3 processCall 'Functional Processes/orderbook/OrderBook:call(ch.soreco.orderbook.bo.Order)' #txt
fg0 f3 doCall true #txt
fg0 f3 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fg0 f3 requestMappingAction 'param.filter.orderState=ch.soreco.orderbook.enums.OrderState.Rejected.toString();
' #txt
fg0 f3 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f3 responseMappingAction 'out=in;
out.orderList=in.orderList.addAll(result.orderBook);
' #txt
fg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Orders in State Rejected</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f3 518 172 36 24 20 -2 #rect
fg0 f3 @|CallSubIcon #fIcon
fg0 f3 -13016147|-1|-16777216 #nodeStyle
fg0 f4 expr out #txt
fg0 f4 536 132 536 172 #arcP
fg0 f12 type filehandling.Data #txt
fg0 f12 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
fg0 f12 doCall true #txt
fg0 f12 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fg0 f12 requestMappingAction 'param.filter.orderId=in.orderList.get(in.i).orderId;
' #txt
fg0 f12 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f12 responseMappingAction 'out=in;
out.error=result.error;
out.Order=result.order;
out.success=result.success;
' #txt
fg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Order</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f12 518 684 36 24 20 -2 #rect
fg0 f12 @|CallSubIcon #fIcon
fg0 f13 expr in #txt
fg0 f13 outCond 'in.i < in.orderList.size()' #txt
fg0 f13 536 654 536 684 #arcP
fg0 f8 expr in #txt
fg0 f8 550 640 536 939 #arcP
fg0 f8 1 624 640 #addKink
fg0 f8 2 624 912 #addKink
fg0 f8 3 536 912 #addKink
fg0 f8 1 0.45101718854367806 0 0 #arcLabel
fg0 f9 type filehandling.Data #txt
fg0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f9 522 738 28 28 14 0 #rect
fg0 f9 @|AlternativeIcon #fIcon
fg0 f18 expr out #txt
fg0 f18 536 708 536 738 #arcP
fg0 f15 expr in #txt
fg0 f15 outCond in.success #txt
fg0 f15 536 766 536 796 #arcP
fg0 f20 expr in #txt
fg0 f20 522 752 518 864 #arcP
fg0 f20 1 496 752 #addKink
fg0 f20 2 496 864 #addKink
fg0 f20 1 0.4060082623816523 0 0 #arcLabel
fg0 f23 actionDecl 'filehandling.Data out;
' #txt
fg0 f23 actionTable 'out=in;
out.i=in.i + 1;
' #txt
fg0 f23 type filehandling.Data #txt
fg0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f23 518 444 36 24 20 -2 #rect
fg0 f23 @|StepIcon #fIcon
fg0 f21 actionDecl 'filehandling.Data out;
' #txt
fg0 f21 actionTable 'out=in;
out.i=0;
' #txt
fg0 f21 type filehandling.Data #txt
fg0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f21 518 220 36 24 20 -2 #rect
fg0 f21 @|StepIcon #fIcon
fg0 f28 actionDecl 'filehandling.Data out;
' #txt
fg0 f28 actionTable 'out=in;
out.orderList=in.orderList.addAll(in.messages);
' #txt
fg0 f28 type filehandling.Data #txt
fg0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add Messages to Orders</name>
        <nameStyle>22
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f28 518 516 36 24 20 -2 #rect
fg0 f28 @|StepIcon #fIcon
fg0 f26 type filehandling.Data #txt
fg0 f26 processCall 'Functional Processes/orderbook/OrderBook:call(ch.soreco.orderbook.bo.Order)' #txt
fg0 f26 doCall true #txt
fg0 f26 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fg0 f26 requestMappingAction 'param.filter.matchedToOrder=in.orderList.get(in.i).orderId;
' #txt
fg0 f26 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f26 responseMappingAction 'out=in;
out.messages=in.messages.addAll(result.orderBook);
' #txt
fg0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Matched Orders</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f26 518 348 36 24 20 -2 #rect
fg0 f26 @|CallSubIcon #fIcon
fg0 f22 type filehandling.Data #txt
fg0 f22 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Orders i from Orderlist</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f22 522 274 28 28 14 0 #rect
fg0 f22 @|AlternativeIcon #fIcon
fg0 f24 expr out #txt
fg0 f24 536 244 536 274 #arcP
fg0 f25 expr out #txt
fg0 f25 518 456 522 288 #arcP
fg0 f25 1 472 456 #addKink
fg0 f25 2 472 288 #addKink
fg0 f25 1 0.5846964169874775 0 0 #arcLabel
fg0 f27 expr in #txt
fg0 f27 outCond 'in.i < in.orderList.size()' #txt
fg0 f27 536 302 536 348 #arcP
fg0 f29 expr out #txt
fg0 f29 536 372 536 444 #arcP
fg0 f30 expr in #txt
fg0 f30 550 288 536 516 #arcP
fg0 f30 1 632 288 #addKink
fg0 f30 2 632 496 #addKink
fg0 f30 3 536 496 #addKink
fg0 f30 0 0.7756558709143526 0 0 #arcLabel
fg0 f31 expr out #txt
fg0 f31 536 196 536 220 #arcP
fg0 f7 expr out #txt
fg0 f7 536 540 536 572 #arcP
fg0 f2 expr out #txt
fg0 f2 536 69 536 108 #arcP
>Proto fg0 .type filehandling.Data #txt
>Proto fg0 .processKind CALLABLE_SUB #txt
>Proto fg0 0 0 32 24 18 0 #rect
>Proto fg0 @|BIcon #fIcon
fg0 f6 mainOut f11 tail #connect
fg0 f11 head f10 in #connect
fg0 f14 mainOut f17 tail #connect
fg0 f17 head f16 mainIn #connect
fg0 f16 mainOut f19 tail #connect
fg0 f19 head f10 in #connect
fg0 f5 mainOut f4 tail #connect
fg0 f4 head f3 mainIn #connect
fg0 f10 out f13 tail #connect
fg0 f13 head f12 mainIn #connect
fg0 f10 out f8 tail #connect
fg0 f8 head f1 mainIn #connect
fg0 f12 mainOut f18 tail #connect
fg0 f18 head f9 in #connect
fg0 f9 out f15 tail #connect
fg0 f15 head f14 mainIn #connect
fg0 f9 out f20 tail #connect
fg0 f20 head f16 mainIn #connect
fg0 f21 mainOut f24 tail #connect
fg0 f24 head f22 in #connect
fg0 f23 mainOut f25 tail #connect
fg0 f25 head f22 in #connect
fg0 f22 out f27 tail #connect
fg0 f27 head f26 mainIn #connect
fg0 f26 mainOut f29 tail #connect
fg0 f29 head f23 mainIn #connect
fg0 f22 out f30 tail #connect
fg0 f30 head f28 mainIn #connect
fg0 f3 mainOut f31 tail #connect
fg0 f31 head f21 mainIn #connect
fg0 f28 mainOut f7 tail #connect
fg0 f7 head f6 mainIn #connect
fg0 f0 mainOut f2 tail #connect
fg0 f2 head f5 mainIn #connect
