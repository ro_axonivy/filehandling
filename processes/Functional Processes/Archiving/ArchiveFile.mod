[Ivy]
[>Created: Thu Nov 10 09:55:29 CET 2011]
12DC1BBFC651A288 3.15 #module
>Proto >Proto Collection #zClass
Ae0 ArchiveFile Big #zClass
Ae0 B #cInfo
Ae0 #process
Ae0 @TextInP .resExport .resExport #zField
Ae0 @TextInP .type .type #zField
Ae0 @TextInP .processKind .processKind #zField
Ae0 @AnnotationInP-0n ai ai #zField
Ae0 @TextInP .xml .xml #zField
Ae0 @TextInP .responsibility .responsibility #zField
Ae0 @StartSub f0 '' #zField
Ae0 @EndSub f1 '' #zField
Ae0 @GridStep f3 '' #zField
Ae0 @PushWFArc f2 '' #zField
Ae0 @CallSub f5 '' #zField
Ae0 @PushWFArc f6 '' #zField
Ae0 @PushWFArc f4 '' #zField
>Proto Ae0 Ae0 ArchiveFile #zField
Ae0 f0 inParamDecl '<java.lang.Integer fileId> param;' #txt
Ae0 f0 inParamTable 'out.File.fileId=param.fileId;
' #txt
Ae0 f0 outParamDecl '<java.lang.Boolean success> result;
' #txt
Ae0 f0 outParamTable 'result.success=in.success;
' #txt
Ae0 f0 actionDecl 'filehandling.Data out;
' #txt
Ae0 f0 callSignature call(Integer) #txt
Ae0 f0 type filehandling.Data #txt
Ae0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Integer)</name>
    </language>
</elementInfo>
' #txt
Ae0 f0 91 35 26 26 14 0 #rect
Ae0 f0 @|StartSubIcon #fIcon
Ae0 f1 type filehandling.Data #txt
Ae0 f1 91 291 26 26 14 0 #rect
Ae0 f1 @|EndSubIcon #fIcon
Ae0 f3 actionDecl 'filehandling.Data out;
' #txt
Ae0 f3 actionTable 'out=in;
' #txt
Ae0 f3 actionCode 'import ch.soreco.filehandling.Filehelper;

try
{
	in.success = true;
	java.io.File outputFile = new java.io.File(ivy.var.IMTFTarget + in.File.fileName);
	Filehelper.copyFile(in.File.file, outputFile);
} catch (Exception ex)
{
	in.success = false;
	ivy.log.error("Error on copy file to IMTF directory",ex);
}' #txt
Ae0 f3 type filehandling.Data #txt
Ae0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>copy File to Archiving Directory</name>
        <nameStyle>32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ae0 f3 86 188 36 24 20 -2 #rect
Ae0 f3 @|StepIcon #fIcon
Ae0 f2 expr out #txt
Ae0 f2 104 212 104 291 #arcP
Ae0 f5 type filehandling.Data #txt
Ae0 f5 processCall 'Functional Processes/common/File:get(ch.soreco.common.bo.File)' #txt
Ae0 f5 doCall true #txt
Ae0 f5 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Ae0 f5 requestMappingAction 'param.filter.fileId=in.File.fileId;
' #txt
Ae0 f5 responseActionDecl 'filehandling.Data out;
' #txt
Ae0 f5 responseMappingAction 'out=in;
out.File=result.file;
' #txt
Ae0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get File</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Ae0 f5 86 108 36 24 20 -2 #rect
Ae0 f5 @|CallSubIcon #fIcon
Ae0 f6 expr out #txt
Ae0 f6 104 61 104 108 #arcP
Ae0 f4 expr out #txt
Ae0 f4 104 132 104 188 #arcP
>Proto Ae0 .type filehandling.Data #txt
>Proto Ae0 .processKind CALLABLE_SUB #txt
>Proto Ae0 0 0 32 24 18 0 #rect
>Proto Ae0 @|BIcon #fIcon
Ae0 f3 mainOut f2 tail #connect
Ae0 f2 head f1 mainIn #connect
Ae0 f0 mainOut f6 tail #connect
Ae0 f6 head f5 mainIn #connect
Ae0 f5 mainOut f4 tail #connect
Ae0 f4 head f3 mainIn #connect
