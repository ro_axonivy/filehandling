[Ivy]
[>Created: Mon Jan 28 11:40:41 CET 2013]
12DBD46F7476D0C1 3.15 #module
>Proto >Proto Collection #zClass
fg0 filterOrdersForIMTFArchiving Big #zClass
fg0 B #cInfo
fg0 #process
fg0 @TextInP .resExport .resExport #zField
fg0 @TextInP .type .type #zField
fg0 @TextInP .processKind .processKind #zField
fg0 @AnnotationInP-0n ai ai #zField
fg0 @TextInP .xml .xml #zField
fg0 @TextInP .responsibility .responsibility #zField
fg0 @StartSub f0 '' #zField
fg0 @EndSub f1 '' #zField
fg0 @CallSub f8 '' #zField
fg0 @PushWFArc f2 '' #zField
fg0 @CallSub f14 '' #zField
fg0 @Alternative f10 '' #zField
fg0 @GridStep f16 '' #zField
fg0 @GridStep f5 '' #zField
fg0 @PushWFArc f11 '' #zField
fg0 @PushWFArc f19 '' #zField
fg0 @PushWFArc f7 '' #zField
fg0 @CallSub f12 '' #zField
fg0 @PushWFArc f6 '' #zField
fg0 @PushWFArc f3 '' #zField
fg0 @Alternative f13 '' #zField
fg0 @PushWFArc f15 '' #zField
fg0 @PushWFArc f9 '' #zField
fg0 @PushWFArc f17 '' #zField
fg0 @GridStep f18 '' #zField
fg0 @Alternative f21 '' #zField
fg0 @PushWFArc f22 '' #zField
fg0 @GridStep f23 '' #zField
fg0 @PushWFArc f25 '' #zField
fg0 @CallSub f26 '' #zField
fg0 @PushWFArc f27 '' #zField
fg0 @PushWFArc f24 '' #zField
fg0 @GridStep f28 '' #zField
fg0 @PushWFArc f29 '' #zField
fg0 @PushWFArc f4 '' #zField
fg0 @PushWFArc f20 '' #zField
fg0 @InfoButton f30 '' #zField
fg0 @AnnotationArc f32 '' #zField
>Proto fg0 fg0 filterOrdersForIMTFArchiving #zField
fg0 f0 outParamDecl '<> result;
' #txt
fg0 f0 actionDecl 'filehandling.Data out;
' #txt
fg0 f0 callSignature call() #txt
fg0 f0 type filehandling.Data #txt
fg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
fg0 f0 267 19 26 26 14 0 #rect
fg0 f0 @|StartSubIcon #fIcon
fg0 f1 type filehandling.Data #txt
fg0 f1 267 835 26 26 14 0 #rect
fg0 f1 @|EndSubIcon #fIcon
fg0 f8 type filehandling.Data #txt
fg0 f8 processCall 'Functional Processes/Archiving/getOrdersViaSubstateOfScanning:call(String)' #txt
fg0 f8 doCall true #txt
fg0 f8 requestActionDecl '<java.lang.String archiveState> param;
' #txt
fg0 f8 requestMappingAction 'param.archiveState=ch.soreco.orderbook.enums.ArchivingState.PREPARED.toString();
' #txt
fg0 f8 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f8 responseMappingAction 'out=in;
out.orderList=in.orderList.addAll(result.orderList);
' #txt
fg0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Orders in State Archived with Scannings in Substate Prepared</name>
        <nameStyle>64,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f8 262 84 36 24 20 -1 #rect
fg0 f8 @|CallSubIcon #fIcon
fg0 f8 -13016147|-1|-16777216 #nodeStyle
fg0 f2 expr out #txt
fg0 f2 280 45 280 84 #arcP
fg0 f14 type filehandling.Data #txt
fg0 f14 processCall 'Functional Processes/Archiving/checkForIMTFArchiving:call(ch.soreco.orderbook.bo.Order)' #txt
fg0 f14 doCall true #txt
fg0 f14 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
fg0 f14 requestMappingAction 'param.order=in.Order;
' #txt
fg0 f14 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f14 responseMappingAction 'out=in;
' #txt
fg0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
        <nameStyle>11,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f14 262 700 36 24 20 -2 #rect
fg0 f14 @|CallSubIcon #fIcon
fg0 f10 type filehandling.Data #txt
fg0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Orders i from Orderlist</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f10 266 546 28 28 14 0 #rect
fg0 f10 @|AlternativeIcon #fIcon
fg0 f16 actionDecl 'filehandling.Data out;
' #txt
fg0 f16 actionTable 'out=in;
out.i=in.i + 1;
' #txt
fg0 f16 type filehandling.Data #txt
fg0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f16 262 764 36 24 20 -2 #rect
fg0 f16 @|StepIcon #fIcon
fg0 f5 actionDecl 'filehandling.Data out;
' #txt
fg0 f5 actionTable 'out=in;
out.i=0;
' #txt
fg0 f5 type filehandling.Data #txt
fg0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f5 262 492 36 24 20 -2 #rect
fg0 f5 @|StepIcon #fIcon
fg0 f11 expr out #txt
fg0 f11 280 516 280 546 #arcP
fg0 f19 expr out #txt
fg0 f19 280 788 266 560 #arcP
fg0 f19 1 280 800 #addKink
fg0 f19 2 192 800 #addKink
fg0 f19 3 192 560 #addKink
fg0 f19 2 0.604734589144823 0 0 #arcLabel
fg0 f7 expr out #txt
fg0 f7 280 724 280 764 #arcP
fg0 f12 type filehandling.Data #txt
fg0 f12 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
fg0 f12 doCall true #txt
fg0 f12 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fg0 f12 requestMappingAction 'param.filter.orderId=in.orderList.get(in.i).orderId;
' #txt
fg0 f12 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f12 responseMappingAction 'out=in;
out.Order=result.order;
out.success=result.success;
' #txt
fg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Order</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f12 262 604 36 24 20 -2 #rect
fg0 f12 @|CallSubIcon #fIcon
fg0 f6 expr in #txt
fg0 f6 outCond 'in.i < in.orderList.size()' #txt
fg0 f6 280 574 280 604 #arcP
fg0 f3 expr in #txt
fg0 f3 294 560 280 835 #arcP
fg0 f3 1 344 560 #addKink
fg0 f3 2 344 808 #addKink
fg0 f3 3 280 808 #addKink
fg0 f3 1 0.48155782156650945 0 0 #arcLabel
fg0 f13 type filehandling.Data #txt
fg0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f13 266 650 28 28 14 0 #rect
fg0 f13 @|AlternativeIcon #fIcon
fg0 f15 expr out #txt
fg0 f15 280 628 280 650 #arcP
fg0 f9 expr in #txt
fg0 f9 outCond in.success #txt
fg0 f9 280 678 280 700 #arcP
fg0 f17 expr in #txt
fg0 f17 266 664 262 776 #arcP
fg0 f17 1 216 664 #addKink
fg0 f17 2 216 776 #addKink
fg0 f17 1 0.34065072789026324 0 0 #arcLabel
fg0 f18 actionDecl 'filehandling.Data out;
' #txt
fg0 f18 actionTable 'out=in;
out.i=0;
' #txt
fg0 f18 type filehandling.Data #txt
fg0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f18 262 140 36 24 20 -2 #rect
fg0 f18 @|StepIcon #fIcon
fg0 f21 type filehandling.Data #txt
fg0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Orders i from Orderlist</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f21 266 194 28 28 14 0 #rect
fg0 f21 @|AlternativeIcon #fIcon
fg0 f22 expr out #txt
fg0 f22 280 164 280 194 #arcP
fg0 f23 actionDecl 'filehandling.Data out;
' #txt
fg0 f23 actionTable 'out=in;
out.i=in.i + 1;
' #txt
fg0 f23 type filehandling.Data #txt
fg0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f23 262 348 36 24 20 -2 #rect
fg0 f23 @|StepIcon #fIcon
fg0 f25 expr out #txt
fg0 f25 262 360 266 208 #arcP
fg0 f25 1 208 360 #addKink
fg0 f25 2 208 208 #addKink
fg0 f25 1 0.7423545184202273 0 0 #arcLabel
fg0 f26 type filehandling.Data #txt
fg0 f26 processCall 'Functional Processes/orderbook/OrderBook:call(ch.soreco.orderbook.bo.Order)' #txt
fg0 f26 doCall true #txt
fg0 f26 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fg0 f26 requestMappingAction 'param.filter.matchedToOrder=in.orderList.get(in.i).orderId;
' #txt
fg0 f26 responseActionDecl 'filehandling.Data out;
' #txt
fg0 f26 responseMappingAction 'out=in;
out.messages=in.messages.addAll(result.orderBook);
' #txt
fg0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Matched Orders</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f26 262 268 36 24 20 -2 #rect
fg0 f26 @|CallSubIcon #fIcon
fg0 f27 expr in #txt
fg0 f27 outCond 'in.i < in.orderList.size()' #txt
fg0 f27 280 222 280 268 #arcP
fg0 f24 expr out #txt
fg0 f24 280 292 280 348 #arcP
fg0 f28 actionDecl 'filehandling.Data out;
' #txt
fg0 f28 actionTable 'out=in;
out.orderList=in.orderList.addAll(in.messages);
' #txt
fg0 f28 type filehandling.Data #txt
fg0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add Messages to Orders</name>
        <nameStyle>22
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f28 262 436 36 24 20 -2 #rect
fg0 f28 @|StepIcon #fIcon
fg0 f29 expr in #txt
fg0 f29 294 208 280 436 #arcP
fg0 f29 1 344 208 #addKink
fg0 f29 2 344 400 #addKink
fg0 f29 3 280 400 #addKink
fg0 f29 1 0.37365064460700603 0 0 #arcLabel
fg0 f4 expr out #txt
fg0 f4 280 460 280 492 #arcP
fg0 f20 expr out #txt
fg0 f20 262 96 262 504 #arcP
fg0 f20 1 128 96 #addKink
fg0 f20 2 128 504 #addKink
fg0 f20 2 0.027923950598557838 0 0 #arcLabel
fg0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>2013-01-28 PB: removed because not used. the check for matched orders will be done later anyway</name>
        <nameStyle>95,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fg0 f30 490 178 60 60 10 0 #rect
fg0 f30 @|IBIcon #fIcon
fg0 f30 -1|-1|-16777216 #nodeStyle
fg0 f32 490 208 294 208 #arcP
>Proto fg0 .type filehandling.Data #txt
>Proto fg0 .processKind CALLABLE_SUB #txt
>Proto fg0 0 0 32 24 18 0 #rect
>Proto fg0 @|BIcon #fIcon
fg0 f0 mainOut f2 tail #connect
fg0 f2 head f8 mainIn #connect
fg0 f5 mainOut f11 tail #connect
fg0 f11 head f10 in #connect
fg0 f16 mainOut f19 tail #connect
fg0 f19 head f10 in #connect
fg0 f14 mainOut f7 tail #connect
fg0 f7 head f16 mainIn #connect
fg0 f10 out f6 tail #connect
fg0 f6 head f12 mainIn #connect
fg0 f10 out f3 tail #connect
fg0 f3 head f1 mainIn #connect
fg0 f12 mainOut f15 tail #connect
fg0 f15 head f13 in #connect
fg0 f13 out f9 tail #connect
fg0 f9 head f14 mainIn #connect
fg0 f13 out f17 tail #connect
fg0 f17 head f16 mainIn #connect
fg0 f18 mainOut f22 tail #connect
fg0 f22 head f21 in #connect
fg0 f23 mainOut f25 tail #connect
fg0 f25 head f21 in #connect
fg0 f21 out f27 tail #connect
fg0 f27 head f26 mainIn #connect
fg0 f26 mainOut f24 tail #connect
fg0 f24 head f23 mainIn #connect
fg0 f21 out f29 tail #connect
fg0 f29 head f28 mainIn #connect
fg0 f28 mainOut f4 tail #connect
fg0 f4 head f5 mainIn #connect
fg0 f8 mainOut f20 tail #connect
fg0 f20 head f5 mainIn #connect
fg0 f30 ao f32 tail #connect
fg0 f32 head f21 @CG|ai #connect
