[Ivy]
[>Created: Tue Nov 22 16:50:13 CET 2011]
12DB7D6F848A7B24 3.15 #module
>Proto >Proto Collection #zClass
cg0 checkForIMTFArchiving Big #zClass
cg0 B #cInfo
cg0 #process
cg0 @TextInP .resExport .resExport #zField
cg0 @TextInP .type .type #zField
cg0 @TextInP .processKind .processKind #zField
cg0 @AnnotationInP-0n ai ai #zField
cg0 @TextInP .xml .xml #zField
cg0 @TextInP .responsibility .responsibility #zField
cg0 @StartSub f0 '' #zField
cg0 @EndSub f1 '' #zField
cg0 @GridStep f6 '' #zField
cg0 @GridStep f10 '' #zField
cg0 @Alternative f3 '' #zField
cg0 @GridStep f12 '' #zField
cg0 @CallSub f2 '' #zField
cg0 @PushWFArc f7 '' #zField
cg0 @PushWFArc f9 '' #zField
cg0 @PushWFArc f8 '' #zField
cg0 @PushWFArc f13 '' #zField
cg0 @CallSub f14 '' #zField
cg0 @CallSub f16 '' #zField
cg0 @PushWFArc f17 '' #zField
cg0 @Alternative f18 '' #zField
cg0 @PushWFArc f5 '' #zField
cg0 @Alternative f20 '' #zField
cg0 @PushWFArc f21 '' #zField
cg0 @PushWFArc f11 '' #zField
cg0 @PushWFArc f22 '' #zField
cg0 @GridStep f23 '' #zField
cg0 @PushWFArc f19 '' #zField
cg0 @CallSub f25 '' #zField
cg0 @PushWFArc f26 '' #zField
cg0 @PushWFArc f4 '' #zField
cg0 @PushWFArc f27 '' #zField
cg0 @InfoButton f15 '' #zField
cg0 @AnnotationArc f28 '' #zField
cg0 @PushWFArc f24 '' #zField
>Proto cg0 cg0 checkForIMTFArchiving #zField
cg0 f0 inParamDecl '<ch.soreco.orderbook.bo.Order order> param;' #txt
cg0 f0 inParamTable 'out.order=param.order;
' #txt
cg0 f0 outParamDecl '<> result;
' #txt
cg0 f0 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f0 callSignature call(ch.soreco.orderbook.bo.Order) #txt
cg0 f0 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
        <nameStyle>11,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f0 380 35 24 27 14 0 #rect
cg0 f0 @|StartSubIcon #fIcon
cg0 f1 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f1 379 707 26 26 14 0 #rect
cg0 f1 @|EndSubIcon #fIcon
cg0 f6 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f6 actionTable 'out=in;
out.scanning=in.order.scannings.get(in.i) as ch.soreco.orderbook.bo.Scanning;
' #txt
cg0 f6 actionCode 'in.scanning.orderId = in.order.orderId;
out.scanning.archivingDate = new  DateTime();' #txt
cg0 f6 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Scanning</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f6 374 276 36 24 20 -2 #rect
cg0 f6 @|StepIcon #fIcon
cg0 f6 -13016147|-1|-16777216 #nodeStyle
cg0 f10 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f10 actionTable 'out=in;
out.i=in.i + 1;
' #txt
cg0 f10 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f10 374 636 36 24 20 -2 #rect
cg0 f10 @|StepIcon #fIcon
cg0 f10 -13016147|-1|-16777216 #nodeStyle
cg0 f3 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Scannings i from ScanGroup</name>
        <nameStyle>31
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f3 378 202 28 28 20 -22 #rect
cg0 f3 @|AlternativeIcon #fIcon
cg0 f3 -13016147|-1|-16777216 #nodeStyle
cg0 f12 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f12 actionTable 'out=in;
out.i=0;
' #txt
cg0 f12 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f12 374 100 36 24 20 -2 #rect
cg0 f12 @|StepIcon #fIcon
cg0 f12 -13016147|-1|-16777216 #nodeStyle
cg0 f2 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f2 processCall 'Functional Processes/Archiving/IMTFArchiving:call(ch.soreco.orderbook.bo.Scanning)' #txt
cg0 f2 doCall true #txt
cg0 f2 requestActionDecl '<ch.soreco.orderbook.bo.Scanning scanning> param;
' #txt
cg0 f2 requestMappingAction 'param.scanning=in.scanning;
' #txt
cg0 f2 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f2 responseMappingAction 'out=in;
' #txt
cg0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Scanning)</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f2 374 476 36 24 20 -2 #rect
cg0 f2 @|CallSubIcon #fIcon
cg0 f2 -13016147|-1|-16777216 #nodeStyle
cg0 f7 expr in #txt
cg0 f7 outCond 'in.i < in.order.scannings.size()' #txt
cg0 f7 392 230 392 276 #arcP
cg0 f9 expr out #txt
cg0 f9 374 648 378 216 #arcP
cg0 f9 1 256 648 #addKink
cg0 f9 2 256 216 #addKink
cg0 f9 1 0.5905782854802178 0 0 #arcLabel
cg0 f8 expr out #txt
cg0 f8 392 61 392 100 #arcP
cg0 f13 expr in #txt
cg0 f13 406 216 392 707 #arcP
cg0 f13 1 544 216 #addKink
cg0 f13 2 544 680 #addKink
cg0 f13 3 392 680 #addKink
cg0 f13 1 0.43001892537196257 0 0 #arcLabel
cg0 f14 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f14 processCall 'Functional Processes/Archiving/DeleteFile:call(Integer)' #txt
cg0 f14 doCall true #txt
cg0 f14 requestActionDecl '<java.lang.Integer fileId> param;
' #txt
cg0 f14 requestMappingAction 'param.fileId=in.scanning.scanFileId;
' #txt
cg0 f14 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f14 responseMappingAction 'out=in;
' #txt
cg0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>deleteScanFile</name>
        <nameStyle>14
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f14 582 532 36 24 20 -2 #rect
cg0 f14 @|CallSubIcon #fIcon
cg0 f16 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f16 processCall 'Functional Processes/Archiving/DeleteFile:call(Integer)' #txt
cg0 f16 doCall true #txt
cg0 f16 requestActionDecl '<java.lang.Integer fileId> param;
' #txt
cg0 f16 requestMappingAction 'param.fileId=in.scanning.scanMonoFileId;
' #txt
cg0 f16 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f16 responseMappingAction 'out=in;
' #txt
cg0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>deleteScanMonoFile</name>
        <nameStyle>18
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f16 582 588 36 24 20 -2 #rect
cg0 f16 @|CallSubIcon #fIcon
cg0 f17 expr out #txt
cg0 f17 600 556 600 588 #arcP
cg0 f18 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>has to be archived?</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f18 378 418 28 28 14 0 #rect
cg0 f18 @|AlternativeIcon #fIcon
cg0 f18 -13016147|-1|-16777216 #nodeStyle
cg0 f5 expr in #txt
cg0 f5 outCond in.archivingDurationOver #txt
cg0 f5 6 #arcStyle
cg0 f5 392 446 392 476 #arcP
cg0 f20 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f20 378 586 28 28 14 0 #rect
cg0 f20 @|AlternativeIcon #fIcon
cg0 f21 expr out #txt
cg0 f21 582 600 406 600 #arcP
cg0 f11 expr in #txt
cg0 f11 392 614 392 636 #arcP
cg0 f22 expr in #txt
cg0 f22 378 432 378 600 #arcP
cg0 f22 1 328 432 #addKink
cg0 f22 2 328 600 #addKink
cg0 f22 1 0.4368204164083921 0 0 #arcLabel
cg0 f23 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f23 actionTable 'out=in;
' #txt
cg0 f23 actionCode 'import java.util.GregorianCalendar;
import java.util.Calendar;

/*int deadlineInSeconds = in.order.scanning.archivingPreparedDate.toNumber() + in.order.scanning.orderMatching.archiveDuration.longValue();;

DateTime now = new DateTime();
int nowInSeconds = now.toNumber();

ivy.log.debug("Deadline: " + deadlineInSeconds + " Now: " + nowInSeconds);

if(deadlineInSeconds <= nowInSeconds)
{
		in.archivingDurationOver = true;
}
else
{
		in.archivingDurationOver = false;
}

*/

DateTime deadline = in.order.scanning.archivingPreparedDate;

Calendar deadlineCal = new GregorianCalendar(deadline.getYear(), deadline.getMonth(), deadline.getDay(), deadline.getHours(), deadline.getMinutes(), deadline.getSeconds());

deadlineCal = deadlineCal.add(Calendar.SECOND, in.order.scanning.orderMatching.archiveDuration);

GregorianCalendar nowCal = new GregorianCalendar();

if(nowCal.compareTo(deadlineCal) >= 0)
{
		in.archivingDurationOver = true;
}
else
{
		in.archivingDurationOver = false;
}' #txt
cg0 f23 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>calculate if archiving duration is over</name>
        <nameStyle>39,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f23 374 364 36 24 20 -2 #rect
cg0 f23 @|StepIcon #fIcon
cg0 f23 -13016147|-1|-16777216 #nodeStyle
cg0 f19 expr out #txt
cg0 f19 392 388 392 418 #arcP
cg0 f25 type ch.soreco.filehandling.functional.Archiving #txt
cg0 f25 processCall 'Functional Processes/orderbook/Dispatching:getOriginalScannings(Integer)' #txt
cg0 f25 doCall true #txt
cg0 f25 requestActionDecl '<java.lang.Integer orderId> param;
' #txt
cg0 f25 requestMappingAction 'param.orderId=in.order.orderId;
' #txt
cg0 f25 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
cg0 f25 responseMappingAction 'out=in;
out.order.scannings=result.success ? result.scannings : in.order.scannings;
' #txt
cg0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get original Scannings if ComboDoc</name>
        <nameStyle>34
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f25 374 148 36 24 20 -2 #rect
cg0 f25 @|CallSubIcon #fIcon
cg0 f26 expr out #txt
cg0 f26 392 124 392 148 #arcP
cg0 f4 expr out #txt
cg0 f4 392 172 392 202 #arcP
cg0 f27 expr out #txt
cg0 f27 392 500 392 586 #arcP
cg0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>17.11.2011 / PB
documents should not be deleted
Telephone with B.Hurter</name>
        <nameStyle>15,7
56,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f15 762 508 60 72 2 -30 #rect
cg0 f15 @|IBIcon #fIcon
cg0 f15 -1|-1|-16777216 #nodeStyle
cg0 f28 762 544 618 544 #arcP
cg0 f24 expr out #txt
cg0 f24 392 300 392 364 #arcP
>Proto cg0 .type ch.soreco.filehandling.functional.Archiving #txt
>Proto cg0 .processKind CALLABLE_SUB #txt
>Proto cg0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto cg0 0 0 32 24 18 0 #rect
>Proto cg0 @|BIcon #fIcon
cg0 f3 out f7 tail #connect
cg0 f7 head f6 mainIn #connect
cg0 f10 mainOut f9 tail #connect
cg0 f9 head f3 in #connect
cg0 f0 mainOut f8 tail #connect
cg0 f8 head f12 mainIn #connect
cg0 f3 out f13 tail #connect
cg0 f13 head f1 mainIn #connect
cg0 f14 mainOut f17 tail #connect
cg0 f17 head f16 mainIn #connect
cg0 f18 out f5 tail #connect
cg0 f5 head f2 mainIn #connect
cg0 f16 mainOut f21 tail #connect
cg0 f21 head f20 in #connect
cg0 f20 out f11 tail #connect
cg0 f11 head f10 mainIn #connect
cg0 f18 out f22 tail #connect
cg0 f22 head f20 in #connect
cg0 f23 mainOut f19 tail #connect
cg0 f19 head f18 in #connect
cg0 f12 mainOut f26 tail #connect
cg0 f26 head f25 mainIn #connect
cg0 f25 mainOut f4 tail #connect
cg0 f4 head f3 in #connect
cg0 f2 mainOut f27 tail #connect
cg0 f27 head f20 in #connect
cg0 f15 ao f28 tail #connect
cg0 f28 head f14 @CG|ai #connect
cg0 f6 mainOut f24 tail #connect
cg0 f24 head f23 mainIn #connect
