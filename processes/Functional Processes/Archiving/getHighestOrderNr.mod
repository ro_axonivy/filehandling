[Ivy]
[>Created: Thu Nov 17 10:09:51 CET 2011]
12DC76A6F4FC791C 3.15 #module
>Proto >Proto Collection #zClass
gr0 getHighestOrderNr Big #zClass
gr0 B #cInfo
gr0 #process
gr0 @TextInP .resExport .resExport #zField
gr0 @TextInP .type .type #zField
gr0 @TextInP .processKind .processKind #zField
gr0 @AnnotationInP-0n ai ai #zField
gr0 @TextInP .xml .xml #zField
gr0 @TextInP .responsibility .responsibility #zField
gr0 @StartSub f0 '' #zField
gr0 @EndSub f1 '' #zField
gr0 @GridStep f2 '' #zField
gr0 @PushWFArc f3 '' #zField
gr0 @PushWFArc f4 '' #zField
>Proto gr0 gr0 getHighestOrderNr #zField
gr0 f0 inParamDecl '<ch.soreco.orderbook.bo.Order order> param;' #txt
gr0 f0 inParamTable 'out.Order=param.order;
' #txt
gr0 f0 outParamDecl '<java.lang.String orderNr> result;
' #txt
gr0 f0 outParamTable 'result.orderNr=in.orderNr;
' #txt
gr0 f0 actionDecl 'filehandling.Data out;
' #txt
gr0 f0 callSignature call(ch.soreco.orderbook.bo.Order) #txt
gr0 f0 type filehandling.Data #txt
gr0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
    </language>
</elementInfo>
' #txt
gr0 f0 91 35 26 26 14 0 #rect
gr0 f0 @|StartSubIcon #fIcon
gr0 f1 type filehandling.Data #txt
gr0 f1 91 291 26 26 14 0 #rect
gr0 f1 @|EndSubIcon #fIcon
gr0 f2 actionDecl 'filehandling.Data out;
' #txt
gr0 f2 actionTable 'out=in;
' #txt
gr0 f2 actionCode 'import ch.soreco.orderbook.bo.RecordingLSV;
import ch.soreco.orderbook.bo.DomicileOrOwner;

List<String> orderNrs = new List<String>();

if(in.Order.recording != null)
{
	if(in.Order.recording.affirmation != null && !in.Order.recording.affirmation.orderNo.isEmpty())
	{
		ivy.log.debug("AffirmationOrderNo: " + in.Order.recording.affirmation.orderNo);
		orderNrs.add(in.Order.recording.affirmation.orderNo);
	}
	
	if(in.Order.recording.businessPartner != null && !in.Order.recording.businessPartner.bpOrderNo.isEmpty())
	{
		ivy.log.debug("BPOrderNo: " + in.Order.recording.businessPartner.bpOrderNo);
		orderNrs.add(in.Order.recording.businessPartner.bpOrderNo);
	}
	
	if(in.Order.recording.correspondence != null && !in.Order.recording.correspondence.changeAddressOrderNo.isEmpty())
	{
		ivy.log.debug("ChangeAddressOrderNo: " + in.Order.recording.correspondence.changeAddressOrderNo);
		orderNrs.add(in.Order.recording.correspondence.changeAddressOrderNo);
	}
	
	if(in.Order.recording.correspondence != null && !in.Order.recording.correspondence.newAddressOrderNo.isEmpty())
	{
		ivy.log.debug("NewAddressOrderNo: " + in.Order.recording.correspondence.newAddressOrderNo);
		orderNrs.add(in.Order.recording.correspondence.newAddressOrderNo);	
	}
	
	for(DomicileOrOwner dor : in.Order.recording.domicileOrOwner)
	{
		if(!dor.orderNo.isEmpty())
		{
			ivy.log.debug("DomicileOrOwnerOrderNo: " + dor.orderNo);
			orderNrs.add(dor.orderNo);
		}	
	}
	
	for(RecordingLSV lsv : in.Order.recording.lsv)
	{
		if(!lsv.lsvOrderNo.isEmpty())
		{
			ivy.log.debug("LSVOrderNo: " + lsv.lsvOrderNo);
			orderNrs.add(lsv.lsvOrderNo);
		}	
	}
}

String nr = "";
for(String orderNr:orderNrs)
{
		String aktNr = orderNr;
		if(aktNr > nr)
		{
			nr = aktNr;	
		}
}

ivy.log.debug("Highest OrderNr: " + nr.toString());

if(nr != "")
{
	in.orderNr = nr.toString();
}' #txt
gr0 f2 type filehandling.Data #txt
gr0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get highest Ordernr</name>
        <nameStyle>19,7
</nameStyle>
        <desc>Änderung 28.02.2011 AIM:
---------------------------------
Old Version
--------------
int nr = 0;
for(String orderNr:orderNrs)
{
		int aktNr = Integer.parseInt(orderNr);
		if(aktNr &gt; nr)
		{
			nr = aktNr;	
		}
}

if(nr != 0)
{
	in.orderNr = nr.toString();
}

New Version
---------------


 </desc>
    </language>
</elementInfo>
' #txt
gr0 f2 86 132 36 24 20 -2 #rect
gr0 f2 @|StepIcon #fIcon
gr0 f3 expr out #txt
gr0 f3 104 61 104 132 #arcP
gr0 f4 expr out #txt
gr0 f4 104 156 104 291 #arcP
>Proto gr0 .type filehandling.Data #txt
>Proto gr0 .processKind CALLABLE_SUB #txt
>Proto gr0 0 0 32 24 18 0 #rect
>Proto gr0 @|BIcon #fIcon
gr0 f0 mainOut f3 tail #connect
gr0 f3 head f2 mainIn #connect
gr0 f2 mainOut f4 tail #connect
gr0 f4 head f1 mainIn #connect
