[Ivy]
[>Created: Mon Jul 25 11:07:21 CEST 2011]
12DB7D7591C16AD5 3.15 #module
>Proto >Proto Collection #zClass
cg0 checkForIvyArchiving Big #zClass
cg0 B #cInfo
cg0 #process
cg0 @TextInP .resExport .resExport #zField
cg0 @TextInP .type .type #zField
cg0 @TextInP .processKind .processKind #zField
cg0 @AnnotationInP-0n ai ai #zField
cg0 @TextInP .xml .xml #zField
cg0 @TextInP .responsibility .responsibility #zField
cg0 @StartSub f0 '' #zField
cg0 @EndSub f1 '' #zField
cg0 @GridStep f6 '' #zField
cg0 @GridStep f10 '' #zField
cg0 @Alternative f3 '' #zField
cg0 @GridStep f12 '' #zField
cg0 @CallSub f2 '' #zField
cg0 @PushWFArc f7 '' #zField
cg0 @PushWFArc f5 '' #zField
cg0 @PushWFArc f11 '' #zField
cg0 @PushWFArc f9 '' #zField
cg0 @PushWFArc f13 '' #zField
cg0 @GridStep f14 '' #zField
cg0 @PushWFArc f15 '' #zField
cg0 @PushWFArc f8 '' #zField
cg0 @CallSub f16 '' #zField
cg0 @PushWFArc f17 '' #zField
cg0 @PushWFArc f4 '' #zField
>Proto cg0 cg0 checkForIvyArchiving #zField
cg0 f0 inParamDecl '<ch.soreco.orderbook.bo.Order order> param;' #txt
cg0 f0 inParamTable 'out.order=param.order;
' #txt
cg0 f0 outParamDecl '<> result;
' #txt
cg0 f0 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cg0 f0 callSignature call(ch.soreco.orderbook.bo.Order) #txt
cg0 f0 type ch.soreco.orderbook.functional.Order #txt
cg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
        <nameStyle>11,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f0 91 35 26 26 14 0 #rect
cg0 f0 @|StartSubIcon #fIcon
cg0 f1 type ch.soreco.orderbook.functional.Order #txt
cg0 f1 91 531 26 26 14 0 #rect
cg0 f1 @|EndSubIcon #fIcon
cg0 f6 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cg0 f6 actionTable 'out=in;
out.order.scanning=in.order.scannings.get(in.i) as ch.soreco.orderbook.bo.Scanning;
' #txt
cg0 f6 type ch.soreco.orderbook.functional.Order #txt
cg0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>getScanning</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f6 86 268 36 24 20 -2 #rect
cg0 f6 @|StepIcon #fIcon
cg0 f6 -13016147|-1|-16777216 #nodeStyle
cg0 f10 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cg0 f10 actionTable 'out=in;
out.i=in.i + 1;
' #txt
cg0 f10 type ch.soreco.orderbook.functional.Order #txt
cg0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f10 86 388 36 24 20 -2 #rect
cg0 f10 @|StepIcon #fIcon
cg0 f10 -13016147|-1|-16777216 #nodeStyle
cg0 f3 type ch.soreco.orderbook.functional.Order #txt
cg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Scannings i from ScanGroup</name>
        <nameStyle>31
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f3 90 210 28 28 20 -22 #rect
cg0 f3 @|AlternativeIcon #fIcon
cg0 f3 -13016147|-1|-16777216 #nodeStyle
cg0 f12 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cg0 f12 actionTable 'out=in;
out.i=0;
' #txt
cg0 f12 type ch.soreco.orderbook.functional.Order #txt
cg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f12 86 92 36 24 20 -2 #rect
cg0 f12 @|StepIcon #fIcon
cg0 f12 -13016147|-1|-16777216 #nodeStyle
cg0 f2 type ch.soreco.orderbook.functional.Order #txt
cg0 f2 processCall 'Functional Processes/Archiving/IvyArchiving:call(ch.soreco.orderbook.bo.Scanning)' #txt
cg0 f2 doCall true #txt
cg0 f2 requestActionDecl '<ch.soreco.orderbook.bo.Scanning scanning> param;
' #txt
cg0 f2 requestMappingAction 'param.scanning=in.order.scanning;
' #txt
cg0 f2 responseActionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cg0 f2 responseMappingAction 'out=in;
' #txt
cg0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Scanning)</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f2 86 324 36 24 20 -2 #rect
cg0 f2 @|CallSubIcon #fIcon
cg0 f2 -13016147|-1|-16777216 #nodeStyle
cg0 f7 expr in #txt
cg0 f7 outCond 'in.i < in.order.scannings.size()' #txt
cg0 f7 104 238 104 268 #arcP
cg0 f5 expr out #txt
cg0 f5 104 292 104 324 #arcP
cg0 f11 expr out #txt
cg0 f11 104 348 104 388 #arcP
cg0 f9 expr out #txt
cg0 f9 86 400 90 224 #arcP
cg0 f9 1 32 400 #addKink
cg0 f9 2 32 224 #addKink
cg0 f9 1 0.5868050618691859 0 0 #arcLabel
cg0 f13 expr out #txt
cg0 f13 104 61 104 92 #arcP
cg0 f14 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cg0 f14 actionTable 'out=in;
' #txt
cg0 f14 actionCode 'import ch.soreco.orderbook.enums.OrderState;


if(in.order.orderState.equalsIgnoreCase(OrderState.Unidentified.toString())
 || in.order.orderState.equalsIgnoreCase(OrderState.Rejected.toString()))
{
	in.order.orderState = OrderState.Archived.toString();
	ivy.persistence.Adressmutation.merge(in.order);
}' #txt
cg0 f14 type ch.soreco.orderbook.functional.Order #txt
cg0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set OrderState to Archived</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f14 86 476 36 24 20 -2 #rect
cg0 f14 @|StepIcon #fIcon
cg0 f15 expr in #txt
cg0 f15 118 224 104 476 #arcP
cg0 f15 1 200 224 #addKink
cg0 f15 2 200 448 #addKink
cg0 f15 3 104 448 #addKink
cg0 f15 1 0.4390593341517963 0 0 #arcLabel
cg0 f8 expr out #txt
cg0 f8 104 500 104 531 #arcP
cg0 f16 type ch.soreco.orderbook.functional.Order #txt
cg0 f16 processCall 'Functional Processes/orderbook/Dispatching:getOriginalScannings(Integer)' #txt
cg0 f16 doCall true #txt
cg0 f16 requestActionDecl '<java.lang.Integer orderId> param;
' #txt
cg0 f16 requestMappingAction 'param.orderId=in.order.orderId;
' #txt
cg0 f16 responseActionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cg0 f16 responseMappingAction 'out=in;
out.order.scannings=result.success ? result.scannings : in.order.scannings;
' #txt
cg0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get original Scannings if ComboDoc</name>
        <nameStyle>34
</nameStyle>
    </language>
</elementInfo>
' #txt
cg0 f16 86 140 36 24 20 -2 #rect
cg0 f16 @|CallSubIcon #fIcon
cg0 f17 expr out #txt
cg0 f17 104 116 104 140 #arcP
cg0 f4 expr out #txt
cg0 f4 104 164 104 210 #arcP
>Proto cg0 .type ch.soreco.orderbook.functional.Order #txt
>Proto cg0 .processKind CALLABLE_SUB #txt
>Proto cg0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto cg0 0 0 32 24 18 0 #rect
>Proto cg0 @|BIcon #fIcon
cg0 f3 out f7 tail #connect
cg0 f7 head f6 mainIn #connect
cg0 f6 mainOut f5 tail #connect
cg0 f5 head f2 mainIn #connect
cg0 f2 mainOut f11 tail #connect
cg0 f11 head f10 mainIn #connect
cg0 f10 mainOut f9 tail #connect
cg0 f9 head f3 in #connect
cg0 f0 mainOut f13 tail #connect
cg0 f13 head f12 mainIn #connect
cg0 f3 out f15 tail #connect
cg0 f15 head f14 mainIn #connect
cg0 f14 mainOut f8 tail #connect
cg0 f8 head f1 mainIn #connect
cg0 f12 mainOut f17 tail #connect
cg0 f17 head f16 mainIn #connect
cg0 f16 mainOut f4 tail #connect
cg0 f4 head f3 in #connect
