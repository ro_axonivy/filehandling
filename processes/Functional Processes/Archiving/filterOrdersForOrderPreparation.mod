[Ivy]
[>Created: Thu Oct 16 10:23:24 CEST 2014]
12DBD1BA99AAC28E 3.17 #module
>Proto >Proto Collection #zClass
fn0 filterOrdersForOrderPreparation Big #zClass
fn0 B #cInfo
fn0 #process
fn0 @TextInP .resExport .resExport #zField
fn0 @TextInP .type .type #zField
fn0 @TextInP .processKind .processKind #zField
fn0 @AnnotationInP-0n ai ai #zField
fn0 @TextInP .xml .xml #zField
fn0 @TextInP .responsibility .responsibility #zField
fn0 @StartSub f0 '' #zField
fn0 @EndSub f1 '' #zField
fn0 @CallSub f4 '' #zField
fn0 @GridStep f5 '' #zField
fn0 @Alternative f10 '' #zField
fn0 @PushWFArc f11 '' #zField
fn0 @CallSub f14 '' #zField
fn0 @GridStep f16 '' #zField
fn0 @PushWFArc f17 '' #zField
fn0 @PushWFArc f19 '' #zField
fn0 @CallSub f12 '' #zField
fn0 @PushWFArc f13 '' #zField
fn0 @Alternative f20 '' #zField
fn0 @PushWFArc f22 '' #zField
fn0 @GridStep f23 '' #zField
fn0 @CallSub f26 '' #zField
fn0 @GridStep f24 '' #zField
fn0 @Alternative f25 '' #zField
fn0 @GridStep f28 '' #zField
fn0 @PushWFArc f27 '' #zField
fn0 @PushWFArc f29 '' #zField
fn0 @PushWFArc f30 '' #zField
fn0 @PushWFArc f32 '' #zField
fn0 @PushWFArc f6 '' #zField
fn0 @GridStep f34 '' #zField
fn0 @PushWFArc f35 '' #zField
fn0 @Alternative f36 '' #zField
fn0 @PushWFArc f37 '' #zField
fn0 @Alternative f38 '' #zField
fn0 @PushWFArc f39 '' #zField
fn0 @PushWFArc f18 '' #zField
fn0 @PushWFArc f40 '' #zField
fn0 @Alternative f41 '' #zField
fn0 @PushWFArc f42 '' #zField
fn0 @PushWFArc f15 '' #zField
fn0 @CallSub f43 '' #zField
fn0 @PushWFArc f44 '' #zField
fn0 @PushWFArc f46 '' #zField
fn0 @PushWFArc f47 '' #zField
fn0 @PushWFArc f3 '' #zField
fn0 @CallSub f9 '' #zField
fn0 @GridStep f50 '' #zField
fn0 @PushWFArc f51 '' #zField
fn0 @PushWFArc f21 '' #zField
fn0 @Alternative f49 '' #zField
fn0 @PushWFArc f52 '' #zField
fn0 @PushWFArc f54 '' #zField
fn0 @PushWFArc f53 '' #zField
fn0 @PushWFArc f48 '' #zField
>Proto fn0 fn0 filterOrdersForOrderPreparation #zField
fn0 f0 outParamDecl '<> result;
' #txt
fn0 f0 actionDecl 'filehandling.Data out;
' #txt
fn0 f0 callSignature call() #txt
fn0 f0 type filehandling.Data #txt
fn0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
fn0 f0 227 19 26 26 14 0 #rect
fn0 f0 @|StartSubIcon #fIcon
fn0 f1 type filehandling.Data #txt
fn0 f1 227 1251 26 26 14 0 #rect
fn0 f1 @|EndSubIcon #fIcon
fn0 f4 type filehandling.Data #txt
fn0 f4 processCall 'Functional Processes/orderbook/OrderBook:call(ch.soreco.orderbook.bo.Order)' #txt
fn0 f4 doCall true #txt
fn0 f4 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fn0 f4 requestMappingAction 'param.filter.orderState=ch.soreco.orderbook.enums.OrderState.Finished.toString();
' #txt
fn0 f4 responseActionDecl 'filehandling.Data out;
' #txt
fn0 f4 responseMappingAction 'out=in;
out.orderList=in.orderList.addAll(result.orderBook);
' #txt
fn0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Orders in State Finished</name>
        <nameStyle>28,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f4 222 268 36 24 20 -1 #rect
fn0 f4 @|CallSubIcon #fIcon
fn0 f5 actionDecl 'filehandling.Data out;
' #txt
fn0 f5 actionTable 'out=in;
out.i=0;
' #txt
fn0 f5 type filehandling.Data #txt
fn0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f5 222 748 36 24 20 -2 #rect
fn0 f5 @|StepIcon #fIcon
fn0 f10 type filehandling.Data #txt
fn0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Orders i from Orderlist</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f10 226 794 28 28 14 0 #rect
fn0 f10 @|AlternativeIcon #fIcon
fn0 f11 expr out #txt
fn0 f11 240 772 240 794 #arcP
fn0 f14 type filehandling.Data #txt
fn0 f14 processCall 'Functional Processes/Archiving/checkForOrderPreparation:call(ch.soreco.orderbook.bo.Order,Number)' #txt
fn0 f14 doCall true #txt
fn0 f14 requestActionDecl '<ch.soreco.orderbook.bo.Order order,java.lang.Number masterOrderId> param;
' #txt
fn0 f14 requestMappingAction 'param.order=in.Order;
param.masterOrderId=in.Order.matchedToOrder;
' #txt
fn0 f14 responseActionDecl 'filehandling.Data out;
' #txt
fn0 f14 responseMappingAction 'out=in;
' #txt
fn0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>checkForOrderPreparation</name>
        <nameStyle>24,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f14 222 1068 36 24 20 -2 #rect
fn0 f14 @|CallSubIcon #fIcon
fn0 f16 actionDecl 'filehandling.Data out;
' #txt
fn0 f16 actionTable 'out=in;
out.i=in.i + 1;
' #txt
fn0 f16 type filehandling.Data #txt
fn0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f16 222 1124 36 24 20 -2 #rect
fn0 f16 @|StepIcon #fIcon
fn0 f17 expr out #txt
fn0 f17 240 1092 240 1124 #arcP
fn0 f19 expr out #txt
fn0 f19 240 1148 226 808 #arcP
fn0 f19 1 240 1168 #addKink
fn0 f19 2 144 1168 #addKink
fn0 f19 3 144 808 #addKink
fn0 f19 3 0.18988617393385346 0 0 #arcLabel
fn0 f12 type filehandling.Data #txt
fn0 f12 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
fn0 f12 doCall true #txt
fn0 f12 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fn0 f12 requestMappingAction 'param.filter.orderId=in.orderList.get(in.i).orderId;
' #txt
fn0 f12 responseActionDecl 'filehandling.Data out;
' #txt
fn0 f12 responseMappingAction 'out=in;
out.error=result.error;
out.Order=result.order;
out.Order.matchedToOrder=in.orderList.get(in.i).matchedToOrder;
out.success=result.success;
' #txt
fn0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Order</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f12 222 844 36 24 20 -2 #rect
fn0 f12 @|CallSubIcon #fIcon
fn0 f13 expr in #txt
fn0 f13 outCond 'in.i < in.orderList.size()' #txt
fn0 f13 240 822 240 844 #arcP
fn0 f20 type filehandling.Data #txt
fn0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f20 226 970 28 28 14 0 #rect
fn0 f20 @|AlternativeIcon #fIcon
fn0 f22 expr in #txt
fn0 f22 226 984 222 1136 #arcP
fn0 f22 1 192 984 #addKink
fn0 f22 2 192 1136 #addKink
fn0 f22 1 0.36789553338972963 0 0 #arcLabel
fn0 f23 actionDecl 'filehandling.Data out;
' #txt
fn0 f23 actionTable 'out=in;
out.i=in.i + 1;
' #txt
fn0 f23 type filehandling.Data #txt
fn0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f23 222 548 36 24 20 -2 #rect
fn0 f23 @|StepIcon #fIcon
fn0 f26 type filehandling.Data #txt
fn0 f26 processCall 'Functional Processes/orderbook/OrderBook:getMatchedOrders(ch.soreco.orderbook.bo.Order)' #txt
fn0 f26 doCall true #txt
fn0 f26 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
fn0 f26 requestMappingAction 'param.filter.matchedToOrder=in.orderList.get(in.i).orderId;
' #txt
fn0 f26 responseActionDecl 'filehandling.Data out;
' #txt
fn0 f26 responseMappingAction 'out=in;
out.messages=in.messages.addAll(result.matchedOrders);
' #txt
fn0 f26 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Matched Orders</name>
        <nameStyle>18,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f26 223 500 34 24 21 -2 #rect
fn0 f26 @|CallSubIcon #fIcon
fn0 f24 actionDecl 'filehandling.Data out;
' #txt
fn0 f24 actionTable 'out=in;
out.i=0;
' #txt
fn0 f24 type filehandling.Data #txt
fn0 f24 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f24 222 380 36 24 20 -2 #rect
fn0 f24 @|StepIcon #fIcon
fn0 f25 type filehandling.Data #txt
fn0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Orders i from Orderlist</name>
        <nameStyle>28
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f25 226 434 28 28 14 0 #rect
fn0 f25 @|AlternativeIcon #fIcon
fn0 f28 actionDecl 'filehandling.Data out;
' #txt
fn0 f28 actionTable 'out=in;
' #txt
fn0 f28 actionCode 'import ch.soreco.orderbook.bo.Order;
// orderlist = in.orderList.addAll(in.messages)

List addList = new List();
boolean add = false;

for(Order o : in.messages){
	/*
	if(!in.orderList.contains(o)){
		in.orderList.add(o);
	}
	*/
	for(Order cOrder : in.orderList){
		if(cOrder.orderId == o.orderId ||cOrder.scanFileId == o.scanFileId ){
			add = false;
			break;
		} else {
			add = true;
		}
	}
	if(add){
		in.orderList.add(o);
	}
	add = false;
}' #txt
fn0 f28 type filehandling.Data #txt
fn0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add Messages to Orders</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f28 222 676 36 24 20 -2 #rect
fn0 f28 @|StepIcon #fIcon
fn0 f27 expr out #txt
fn0 f27 240 404 240 434 #arcP
fn0 f29 expr out #txt
fn0 f29 222 560 226 448 #arcP
fn0 f29 1 168 560 #addKink
fn0 f29 2 168 448 #addKink
fn0 f29 1 0.6118689353488138 0 0 #arcLabel
fn0 f30 expr in #txt
fn0 f30 outCond 'in.i < in.orderList.size()' #txt
fn0 f30 240 462 240 500 #arcP
fn0 f32 expr in #txt
fn0 f32 254 448 240 676 #arcP
fn0 f32 1 424 448 #addKink
fn0 f32 2 424 640 #addKink
fn0 f32 3 240 640 #addKink
fn0 f32 1 0.4129743641875707 0 0 #arcLabel
fn0 f6 expr out #txt
fn0 f6 240 700 240 748 #arcP
fn0 f34 actionDecl 'filehandling.Data out;
' #txt
fn0 f34 actionTable 'out=in;
' #txt
fn0 f34 actionCode 'java.io.File file = new java.io.File(ivy.var.IMTFTarget);

if (file.exists() && file.isDirectory()) {
	in.success = true;
	ivy.log.info("IMTFTarget directory is set to: "+ivy.var.IMTFTarget);
} else {
	in.success = false;
	ivy.log.error("IMTFTarget directory does not exist! Check global variable value of IMTFTarget: "+ivy.var.IMTFTarget);
}' #txt
fn0 f34 type filehandling.Data #txt
fn0 f34 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>check if IMTF directory exists</name>
        <nameStyle>30,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f34 222 76 36 24 19 -2 #rect
fn0 f34 @|StepIcon #fIcon
fn0 f35 expr out #txt
fn0 f35 240 45 240 76 #arcP
fn0 f36 type filehandling.Data #txt
fn0 f36 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f36 226 146 28 28 14 0 #rect
fn0 f36 @|AlternativeIcon #fIcon
fn0 f37 expr out #txt
fn0 f37 240 100 240 146 #arcP
fn0 f38 type filehandling.Data #txt
fn0 f38 226 1202 28 28 14 0 #rect
fn0 f38 @|AlternativeIcon #fIcon
fn0 f39 expr in #txt
fn0 f39 254 808 254 1216 #arcP
fn0 f39 1 624 808 #addKink
fn0 f39 2 624 1216 #addKink
fn0 f39 1 0.3584121448755468 0 0 #arcLabel
fn0 f18 expr in #txt
fn0 f18 240 1230 240 1251 #arcP
fn0 f40 expr in #txt
fn0 f40 226 160 226 1216 #arcP
fn0 f40 1 120 160 #addKink
fn0 f40 2 120 1216 #addKink
fn0 f40 1 0.5103203826938648 0 0 #arcLabel
fn0 f41 type filehandling.Data #txt
fn0 f41 226 1018 28 28 14 0 #rect
fn0 f41 @|AlternativeIcon #fIcon
fn0 f42 expr in #txt
fn0 f42 outCond in.success #txt
fn0 f42 240 998 240 1018 #arcP
fn0 f15 expr in #txt
fn0 f15 outCond 'in.Order.BPId.trim().length() > 0 && !in.Order.BPId.equalsIgnoreCase("0000000") && !in.Order.BPId.equalsIgnoreCase("00000000")' #txt
fn0 f15 240 1046 240 1068 #arcP
fn0 f43 type filehandling.Data #txt
fn0 f43 processCall 'Functional Processes/Archiving/checkForIvyArchiving:call(ch.soreco.orderbook.bo.Order)' #txt
fn0 f43 doCall true #txt
fn0 f43 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
fn0 f43 requestMappingAction 'param.order=in.Order;
' #txt
fn0 f43 responseActionDecl 'filehandling.Data out;
' #txt
fn0 f43 responseMappingAction 'out=in;
' #txt
fn0 f43 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>checkForIvyArchiving</name>
        <nameStyle>20,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f43 438 1068 36 24 20 -2 #rect
fn0 f43 @|CallSubIcon #fIcon
fn0 f44 expr in #txt
fn0 f44 254 1032 456 1068 #arcP
fn0 f44 1 456 1032 #addKink
fn0 f44 0 0.6502343192673085 0 0 #arcLabel
fn0 f46 expr out #txt
fn0 f46 456 1092 258 1136 #arcP
fn0 f46 1 456 1136 #addKink
fn0 f46 1 0.2560846087558569 0 0 #arcLabel
fn0 f47 expr in #txt
fn0 f47 outCond in.success #txt
fn0 f47 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f47 6 #arcStyle
fn0 f47 240 174 240 268 #arcP
fn0 f3 expr out #txt
fn0 f3 240 292 240 380 #arcP
fn0 f9 type filehandling.Data #txt
fn0 f9 processCall 'Functional Processes/orderbook/Dispatching:getOriginalScannings(Integer)' #txt
fn0 f9 doCall true #txt
fn0 f9 requestActionDecl '<java.lang.Integer orderId> param;
' #txt
fn0 f9 requestMappingAction 'param.orderId=in.Order.orderId;
' #txt
fn0 f9 responseActionDecl 'filehandling.Data out;
' #txt
fn0 f9 responseMappingAction 'out=in;
out.masterOrder.scannings=result.scannings;
' #txt
fn0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get original Scannings if ComboDoc</name>
        <nameStyle>34,7
</nameStyle>
    </language>
</elementInfo>
' #txt
fn0 f9 310 892 36 24 20 -2 #rect
fn0 f9 @|CallSubIcon #fIcon
fn0 f50 actionDecl 'filehandling.Data out;
' #txt
fn0 f50 actionTable 'out=in;
' #txt
fn0 f50 actionCode 'import ch.soreco.orderbook.bo.Scanning;
/*
List scanningsToAdd = new List();

boolean add = false;
if(in.Order.scannings.size() == 0){
		scanningsToAdd.addAll(in.masterOrder.scannings);
		break;
	} else {
		for(Scanning cScan : in.masterOrder.scannings){
			for(Scanning vScan : in.Order.scannings){
				if(vScan.scanFileId == cScan.scanFileId){
					add = false;
					break;
				} else {
					add = true;
				}
			}
			if(add){
				scanningsToAdd.add(cScan);
			}
			add = false;
		}
}
*/
/*
boolean add = false;
if(in.Order.scannings.size() == 0){
		scanningsToAdd.addAll(in.masterOrder.scannings);
		break;
} else {
	for(Scanning vScan : in.Order.scannings){
			for(Scanning cScan : in.masterOrder.scannings){
				ivy.log.debug("Masterscan: "+cScan.scanFileId+" - OrderScan"+vScan.scanFileId);
				if(vScan.scanFileId == cScan.scanFileId){
					in.masterOrder.scannings.remove(cScan);
					break;
				} else {
					scanningsToAdd.add(cScan);
				}
			}
	}
}
*/

//out.Order.scannings.addAll(scanningsToAdd);
out.Order.scannings.addAll(in.masterOrder.scannings);' #txt
fn0 f50 type filehandling.Data #txt
fn0 f50 310 932 36 24 20 -2 #rect
fn0 f50 @|StepIcon #fIcon
fn0 f51 expr out #txt
fn0 f51 328 916 328 932 #arcP
fn0 f21 expr out #txt
fn0 f21 310 952 250 980 #arcP
fn0 f49 type filehandling.Data #txt
fn0 f49 226 890 28 28 14 0 #rect
fn0 f49 @|AlternativeIcon #fIcon
fn0 f52 expr in #txt
fn0 f52 outCond 'in.Order.scannings.size() > 0' #txt
fn0 f52 240 918 240 970 #arcP
fn0 f54 expr in #txt
fn0 f54 254 904 310 904 #arcP
fn0 f53 expr out #txt
fn0 f53 240 868 240 890 #arcP
fn0 f48 expr out #txt
fn0 f48 240 524 240 548 #arcP
>Proto fn0 .type filehandling.Data #txt
>Proto fn0 .processKind CALLABLE_SUB #txt
>Proto fn0 0 0 32 24 18 0 #rect
>Proto fn0 @|BIcon #fIcon
fn0 f5 mainOut f11 tail #connect
fn0 f11 head f10 in #connect
fn0 f14 mainOut f17 tail #connect
fn0 f17 head f16 mainIn #connect
fn0 f16 mainOut f19 tail #connect
fn0 f19 head f10 in #connect
fn0 f10 out f13 tail #connect
fn0 f13 head f12 mainIn #connect
fn0 f22 head f16 mainIn #connect
fn0 f24 mainOut f27 tail #connect
fn0 f27 head f25 in #connect
fn0 f23 mainOut f29 tail #connect
fn0 f29 head f25 in #connect
fn0 f25 out f30 tail #connect
fn0 f30 head f26 mainIn #connect
fn0 f25 out f32 tail #connect
fn0 f32 head f28 mainIn #connect
fn0 f28 mainOut f6 tail #connect
fn0 f6 head f5 mainIn #connect
fn0 f0 mainOut f35 tail #connect
fn0 f35 head f34 mainIn #connect
fn0 f34 mainOut f37 tail #connect
fn0 f37 head f36 in #connect
fn0 f10 out f39 tail #connect
fn0 f39 head f38 in #connect
fn0 f38 out f18 tail #connect
fn0 f18 head f1 mainIn #connect
fn0 f40 head f38 in #connect
fn0 f20 out f42 tail #connect
fn0 f42 head f41 in #connect
fn0 f20 out f22 tail #connect
fn0 f41 out f15 tail #connect
fn0 f15 head f14 mainIn #connect
fn0 f41 out f44 tail #connect
fn0 f44 head f43 mainIn #connect
fn0 f43 mainOut f46 tail #connect
fn0 f46 head f16 mainIn #connect
fn0 f36 out f47 tail #connect
fn0 f47 head f4 mainIn #connect
fn0 f36 out f40 tail #connect
fn0 f4 mainOut f3 tail #connect
fn0 f3 head f24 mainIn #connect
fn0 f9 mainOut f51 tail #connect
fn0 f51 head f50 mainIn #connect
fn0 f50 mainOut f21 tail #connect
fn0 f21 head f20 in #connect
fn0 f49 out f52 tail #connect
fn0 f52 head f20 in #connect
fn0 f49 out f54 tail #connect
fn0 f54 head f9 mainIn #connect
fn0 f12 mainOut f53 tail #connect
fn0 f53 head f49 in #connect
fn0 f26 mainOut f48 tail #connect
fn0 f48 head f23 mainIn #connect
