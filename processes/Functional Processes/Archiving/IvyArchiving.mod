[Ivy]
[>Created: Thu Nov 10 09:56:49 CET 2011]
12D9368DC93A66EF 3.15 #module
>Proto >Proto Collection #zClass
Ig0 IvyArchiving Big #zClass
Ig0 B #cInfo
Ig0 #process
Ig0 @TextInP .resExport .resExport #zField
Ig0 @TextInP .type .type #zField
Ig0 @TextInP .processKind .processKind #zField
Ig0 @AnnotationInP-0n ai ai #zField
Ig0 @TextInP .xml .xml #zField
Ig0 @TextInP .responsibility .responsibility #zField
Ig0 @StartSub f0 '' #zField
Ig0 @EndSub f1 '' #zField
Ig0 @GridStep f3 '' #zField
Ig0 @PushWFArc f5 '' #zField
Ig0 @PushWFArc f2 '' #zField
>Proto Ig0 Ig0 IvyArchiving #zField
Ig0 f0 inParamDecl '<ch.soreco.orderbook.bo.Scanning scanning> param;' #txt
Ig0 f0 inParamTable 'out.scanning=param.scanning;
' #txt
Ig0 f0 outParamDecl '<> result;
' #txt
Ig0 f0 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
Ig0 f0 callSignature call(ch.soreco.orderbook.bo.Scanning) #txt
Ig0 f0 type ch.soreco.filehandling.functional.Archiving #txt
Ig0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Scanning)</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ig0 f0 235 147 26 26 14 0 #rect
Ig0 f0 @|StartSubIcon #fIcon
Ig0 f1 type ch.soreco.filehandling.functional.Archiving #txt
Ig0 f1 235 331 26 26 14 0 #rect
Ig0 f1 @|EndSubIcon #fIcon
Ig0 f3 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
Ig0 f3 actionTable 'out=in;
out.scanning.archivingState=ch.soreco.orderbook.enums.ArchivingState.IVY_ARCHIVED.toString();
' #txt
Ig0 f3 actionCode ivy.persistence.Adressmutation.merge(in.scanning); #txt
Ig0 f3 type ch.soreco.filehandling.functional.Archiving #txt
Ig0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Set substate of Archiving to &quot;Ivy_Archived&quot;</name>
        <nameStyle>43,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ig0 f3 230 236 36 24 20 -2 #rect
Ig0 f3 @|StepIcon #fIcon
Ig0 f3 -13016147|-1|-16777216 #nodeStyle
Ig0 f5 expr out #txt
Ig0 f5 248 260 248 331 #arcP
Ig0 f2 expr out #txt
Ig0 f2 248 173 248 236 #arcP
>Proto Ig0 .type ch.soreco.filehandling.functional.Archiving #txt
>Proto Ig0 .processKind CALLABLE_SUB #txt
>Proto Ig0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Ig0 0 0 32 24 18 0 #rect
>Proto Ig0 @|BIcon #fIcon
Ig0 f3 mainOut f5 tail #connect
Ig0 f5 head f1 mainIn #connect
Ig0 f0 mainOut f2 tail #connect
Ig0 f2 head f3 mainIn #connect
