[Ivy]
[>Created: Thu Nov 10 09:55:36 CET 2011]
12DC17504967218D 3.15 #module
>Proto >Proto Collection #zClass
Dg0 DeleteFile Big #zClass
Dg0 B #cInfo
Dg0 #process
Dg0 @TextInP .resExport .resExport #zField
Dg0 @TextInP .type .type #zField
Dg0 @TextInP .processKind .processKind #zField
Dg0 @AnnotationInP-0n ai ai #zField
Dg0 @TextInP .xml .xml #zField
Dg0 @TextInP .responsibility .responsibility #zField
Dg0 @StartSub f0 '' #zField
Dg0 @EndSub f1 '' #zField
Dg0 @CallSub f3 '' #zField
Dg0 @GridStep f9 '' #zField
Dg0 @PushWFArc f10 '' #zField
Dg0 @GridStep f11 '' #zField
Dg0 @PushWFArc f4 '' #zField
Dg0 @Alternative f5 '' #zField
Dg0 @PushWFArc f6 '' #zField
Dg0 @PushWFArc f7 '' #zField
Dg0 @Alternative f12 '' #zField
Dg0 @PushWFArc f13 '' #zField
Dg0 @PushWFArc f2 '' #zField
Dg0 @GridStep f25 '' #zField
Dg0 @PushWFArc f8 '' #zField
Dg0 @PushWFArc f14 '' #zField
>Proto Dg0 Dg0 DeleteFile #zField
Dg0 f0 inParamDecl '<java.lang.Integer fileId> param;' #txt
Dg0 f0 inParamTable 'out.File.fileId=param.fileId;
out.success=true;
' #txt
Dg0 f0 outParamDecl '<java.lang.Boolean success> result;
' #txt
Dg0 f0 outParamTable 'result.success=in.success;
' #txt
Dg0 f0 actionDecl 'filehandling.Data out;
' #txt
Dg0 f0 callSignature call(Integer) #txt
Dg0 f0 type filehandling.Data #txt
Dg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(FileId)</name>
        <nameStyle>12,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f0 259 35 26 26 14 0 #rect
Dg0 f0 @|StartSubIcon #fIcon
Dg0 f1 type filehandling.Data #txt
Dg0 f1 259 411 26 26 14 0 #rect
Dg0 f1 @|EndSubIcon #fIcon
Dg0 f3 type filehandling.Data #txt
Dg0 f3 processCall 'Functional Processes/common/File:get(ch.soreco.common.bo.File)' #txt
Dg0 f3 doCall true #txt
Dg0 f3 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
Dg0 f3 requestMappingAction 'param.filter.fileId=in.File.fileId;
' #txt
Dg0 f3 responseActionDecl 'filehandling.Data out;
' #txt
Dg0 f3 responseMappingAction 'out=in;
out.File=result.file;
' #txt
Dg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get File</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f3 254 100 36 24 20 -2 #rect
Dg0 f3 @|CallSubIcon #fIcon
Dg0 f3 -13016147|-1|-16777216 #nodeStyle
Dg0 f9 actionDecl 'filehandling.Data out;
' #txt
Dg0 f9 actionTable 'out=in;
' #txt
Dg0 f9 actionCode 'import ch.soreco.filehandling.Filehelper;

in.success = Filehelper.deleteFile(in.File.file);' #txt
Dg0 f9 type filehandling.Data #txt
Dg0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Delete File from Disk</name>
        <nameStyle>21
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f9 254 164 36 24 20 -2 #rect
Dg0 f9 @|StepIcon #fIcon
Dg0 f9 -13016147|-1|-16777216 #nodeStyle
Dg0 f10 expr out #txt
Dg0 f10 272 124 272 164 #arcP
Dg0 f11 actionDecl 'filehandling.Data out;
' #txt
Dg0 f11 actionTable 'out=in;
out.File.isDeleted=true;
' #txt
Dg0 f11 type filehandling.Data #txt
Dg0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Set Flag isDeleted true</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f11 254 292 36 24 20 -2 #rect
Dg0 f11 @|StepIcon #fIcon
Dg0 f11 -13016147|-1|-16777216 #nodeStyle
Dg0 f4 expr out #txt
Dg0 f4 272 61 272 100 #arcP
Dg0 f5 type filehandling.Data #txt
Dg0 f5 258 218 28 28 14 0 #rect
Dg0 f5 @|AlternativeIcon #fIcon
Dg0 f6 expr out #txt
Dg0 f6 272 188 272 218 #arcP
Dg0 f7 expr in #txt
Dg0 f7 outCond in.success #txt
Dg0 f7 6 #arcStyle
Dg0 f7 272 246 272 292 #arcP
Dg0 f12 type filehandling.Data #txt
Dg0 f12 258 362 28 28 14 0 #rect
Dg0 f12 @|AlternativeIcon #fIcon
Dg0 f13 expr out #txt
Dg0 f13 272 316 272 362 #arcP
Dg0 f2 expr in #txt
Dg0 f2 272 390 272 411 #arcP
Dg0 f25 actionDecl 'filehandling.Data out;
' #txt
Dg0 f25 actionTable 'out=in;
' #txt
Dg0 f25 actionCode 'ivy.log.error("Couldn''t delete file " + in.File.fileName + " from Filesystem.");' #txt
Dg0 f25 type filehandling.Data #txt
Dg0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>log Error</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
Dg0 f25 446 292 36 24 30 -4 #rect
Dg0 f25 @|StepIcon #fIcon
Dg0 f25 -13016147|-1|-16777216 #nodeStyle
Dg0 f8 expr in #txt
Dg0 f8 286 232 464 292 #arcP
Dg0 f8 1 464 232 #addKink
Dg0 f8 1 0.06746126310143337 0 0 #arcLabel
Dg0 f14 expr out #txt
Dg0 f14 464 316 286 376 #arcP
Dg0 f14 1 464 376 #addKink
Dg0 f14 1 0.3658583954038894 0 0 #arcLabel
>Proto Dg0 .type filehandling.Data #txt
>Proto Dg0 .processKind CALLABLE_SUB #txt
>Proto Dg0 0 0 32 24 18 0 #rect
>Proto Dg0 @|BIcon #fIcon
Dg0 f3 mainOut f10 tail #connect
Dg0 f10 head f9 mainIn #connect
Dg0 f0 mainOut f4 tail #connect
Dg0 f4 head f3 mainIn #connect
Dg0 f9 mainOut f6 tail #connect
Dg0 f6 head f5 in #connect
Dg0 f5 out f7 tail #connect
Dg0 f7 head f11 mainIn #connect
Dg0 f11 mainOut f13 tail #connect
Dg0 f13 head f12 in #connect
Dg0 f12 out f2 tail #connect
Dg0 f2 head f1 mainIn #connect
Dg0 f5 out f8 tail #connect
Dg0 f8 head f25 mainIn #connect
Dg0 f25 mainOut f14 tail #connect
Dg0 f14 head f12 in #connect
