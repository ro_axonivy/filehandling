[Ivy]
[>Created: Mon Mar 12 12:54:04 CET 2012]
12DB7D7D452BAAEA 3.15 #module
>Proto >Proto Collection #zClass
cn0 checkForOrderPreparation Big #zClass
cn0 B #cInfo
cn0 #process
cn0 @TextInP .resExport .resExport #zField
cn0 @TextInP .type .type #zField
cn0 @TextInP .processKind .processKind #zField
cn0 @AnnotationInP-0n ai ai #zField
cn0 @TextInP .xml .xml #zField
cn0 @TextInP .responsibility .responsibility #zField
cn0 @StartSub f0 '' #zField
cn0 @EndSub f1 '' #zField
cn0 @CallSub f2 '' #zField
cn0 @Alternative f3 '' #zField
cn0 @GridStep f10 '' #zField
cn0 @PushWFArc f9 '' #zField
cn0 @GridStep f12 '' #zField
cn0 @PushWFArc f13 '' #zField
cn0 @GridStep f6 '' #zField
cn0 @PushWFArc f7 '' #zField
cn0 @GridStep f14 '' #zField
cn0 @Alternative f16 '' #zField
cn0 @PushWFArc f17 '' #zField
cn0 @GridStep f18 '' #zField
cn0 @PushWFArc f19 '' #zField
cn0 @PushWFArc f20 '' #zField
cn0 @Alternative f21 '' #zField
cn0 @PushWFArc f22 '' #zField
cn0 @PushWFArc f15 '' #zField
cn0 @Alternative f23 '' #zField
cn0 @PushWFArc f24 '' #zField
cn0 @PushWFArc f8 '' #zField
cn0 @PushWFArc f25 '' #zField
cn0 @PushWFArc f28 '' #zField
cn0 @CallSub f11 '' #zField
cn0 @PushWFArc f4 '' #zField
cn0 @Alternative f27 '' #zField
cn0 @PushWFArc f29 '' #zField
cn0 @CallSub f30 '' #zField
cn0 @PushWFArc f32 '' #zField
cn0 @InfoButton f33 '' #zField
cn0 @AnnotationArc f34 '' #zField
cn0 @StartSub f35 '' #zField
cn0 @PushWFArc f36 '' #zField
cn0 @PushWFArc f31 '' #zField
cn0 @PushWFArc f5 '' #zField
cn0 @PushWFArc f37 '' #zField
>Proto cn0 cn0 checkForOrderPreparation #zField
cn0 f0 inParamDecl '<ch.soreco.orderbook.bo.Order order> param;' #txt
cn0 f0 inParamTable 'out.order=param.order;
' #txt
cn0 f0 outParamDecl '<> result;
' #txt
cn0 f0 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f0 callSignature call(ch.soreco.orderbook.bo.Order) #txt
cn0 f0 type ch.soreco.orderbook.functional.Order #txt
cn0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order)</name>
        <nameStyle>11,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f0 83 35 26 26 14 0 #rect
cn0 f0 @|StartSubIcon #fIcon
cn0 f1 type ch.soreco.orderbook.functional.Order #txt
cn0 f1 83 795 26 26 14 0 #rect
cn0 f1 @|EndSubIcon #fIcon
cn0 f2 type ch.soreco.orderbook.functional.Order #txt
cn0 f2 processCall 'Functional Processes/Archiving/OrderPreparation:call(Number,ch.soreco.orderbook.bo.Scanning)' #txt
cn0 f2 doCall true #txt
cn0 f2 requestActionDecl '<java.lang.Number masterOrderId,ch.soreco.orderbook.bo.Scanning scanning> param;
' #txt
cn0 f2 requestMappingAction 'param.masterOrderId=in.order.matchedToOrder;
param.scanning=in.order.scanning;
' #txt
cn0 f2 responseActionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f2 responseMappingAction 'out=in;
out.success=result.success;
' #txt
cn0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Number,Scanning)</name>
    </language>
</elementInfo>
' #txt
cn0 f2 78 356 36 24 20 -2 #rect
cn0 f2 @|CallSubIcon #fIcon
cn0 f2 -13016147|-1|-16777216 #nodeStyle
cn0 f3 type ch.soreco.orderbook.functional.Order #txt
cn0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Scannings i from ScanGroup</name>
        <nameStyle>31
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f3 82 210 28 28 20 -22 #rect
cn0 f3 @|AlternativeIcon #fIcon
cn0 f3 -13016147|-1|-16777216 #nodeStyle
cn0 f10 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f10 actionTable 'out=in;
out.i=in.i + 1;
' #txt
cn0 f10 type ch.soreco.orderbook.functional.Order #txt
cn0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f10 78 556 36 24 20 -2 #rect
cn0 f10 @|StepIcon #fIcon
cn0 f10 -13016147|-1|-16777216 #nodeStyle
cn0 f9 expr out #txt
cn0 f9 78 568 82 224 #arcP
cn0 f9 1 24 568 #addKink
cn0 f9 2 24 224 #addKink
cn0 f9 1 0.5113636363636364 0 0 #arcLabel
cn0 f12 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f12 actionTable 'out=in;
out.i=0;
out.j=0;
' #txt
cn0 f12 type ch.soreco.orderbook.functional.Order #txt
cn0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i + j</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f12 78 92 36 24 20 -2 #rect
cn0 f12 @|StepIcon #fIcon
cn0 f12 -13016147|-1|-16777216 #nodeStyle
cn0 f13 expr out #txt
cn0 f13 96 61 96 92 #arcP
cn0 f6 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f6 actionTable 'out=in;
out.order.scanning=in.order.scannings.get(in.i) as ch.soreco.orderbook.bo.Scanning;
' #txt
cn0 f6 actionCode '// override orderId
// 17.11.2011 / PB / CR ITS-998 & 1052
in.order.scanning.orderId = in.order.orderId;
' #txt
cn0 f6 type ch.soreco.orderbook.functional.Order #txt
cn0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Scanning
override orderId</name>
        <nameStyle>29,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f6 78 260 36 24 20 -2 #rect
cn0 f6 @|StepIcon #fIcon
cn0 f7 expr in #txt
cn0 f7 outCond 'in.i < in.order.scannings.size()' #txt
cn0 f7 96 238 96 260 #arcP
cn0 f14 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f14 actionTable 'out=in;
' #txt
cn0 f14 actionCode 'import ch.soreco.orderbook.enums.OrderState;


if(in.order.orderState.equalsIgnoreCase(OrderState.Aborted.toString())
 || in.order.orderState.equalsIgnoreCase(OrderState.Finished.toString()))
{
	in.order.orderState = OrderState.Archived.toString();
	ivy.persistence.Adressmutation.merge(in.order);
}' #txt
cn0 f14 type ch.soreco.orderbook.functional.Order #txt
cn0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set OrderState to Archived</name>
        <nameStyle>26
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f14 78 684 36 24 20 -2 #rect
cn0 f14 @|StepIcon #fIcon
cn0 f16 type ch.soreco.orderbook.functional.Order #txt
cn0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f16 82 434 28 28 14 0 #rect
cn0 f16 @|AlternativeIcon #fIcon
cn0 f17 expr out #txt
cn0 f17 96 380 96 434 #arcP
cn0 f18 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f18 actionTable 'out=in;
out.j=in.j + 1;
' #txt
cn0 f18 type ch.soreco.orderbook.functional.Order #txt
cn0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>j++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f18 150 500 36 24 20 -2 #rect
cn0 f18 @|StepIcon #fIcon
cn0 f19 expr in #txt
cn0 f19 0 #arcStyle
cn0 f19 110 448 168 500 #arcP
cn0 f19 1 168 448 #addKink
cn0 f19 0 0.9099402469669138 0 0 #arcLabel
cn0 f20 expr out #txt
cn0 f20 168 524 114 568 #arcP
cn0 f20 1 168 568 #addKink
cn0 f20 0 0.9512597984423166 0 0 #arcLabel
cn0 f21 type ch.soreco.orderbook.functional.Order #txt
cn0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>no errors?</name>
        <nameStyle>10,7
</nameStyle>
        <desc>in.j == 0</desc>
    </language>
</elementInfo>
' #txt
cn0 f21 82 626 28 28 14 0 #rect
cn0 f21 @|AlternativeIcon #fIcon
cn0 f22 expr in #txt
cn0 f22 110 224 96 626 #arcP
cn0 f22 1 280 224 #addKink
cn0 f22 2 280 600 #addKink
cn0 f22 3 96 600 #addKink
cn0 f22 1 0.4695172359473607 0 0 #arcLabel
cn0 f15 expr in #txt
cn0 f15 outCond in.success #txt
cn0 f15 6 #arcStyle
cn0 f15 96 654 96 684 #arcP
cn0 f23 type ch.soreco.orderbook.functional.Order #txt
cn0 f23 82 738 28 28 14 0 #rect
cn0 f23 @|AlternativeIcon #fIcon
cn0 f24 expr out #txt
cn0 f24 96 708 96 738 #arcP
cn0 f8 expr in #txt
cn0 f8 96 766 96 795 #arcP
cn0 f25 expr in #txt
cn0 f25 82 640 82 752 #arcP
cn0 f25 1 32 640 #addKink
cn0 f25 2 32 752 #addKink
cn0 f25 1 0.33744930908942256 0 0 #arcLabel
cn0 f28 expr in #txt
cn0 f28 outCond in.success #txt
cn0 f28 6 #arcStyle
cn0 f28 96 462 96 556 #arcP
cn0 f11 type ch.soreco.orderbook.functional.Order #txt
cn0 f11 processCall 'Functional Processes/orderbook/Dispatching:getOriginalScannings(Integer)' #txt
cn0 f11 doCall true #txt
cn0 f11 requestActionDecl '<java.lang.Integer orderId> param;
' #txt
cn0 f11 requestMappingAction 'param.orderId=in.order.matchedToOrder;
' #txt
cn0 f11 responseActionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f11 responseMappingAction 'out=in;
out.order.scannings=result.success ? result.scannings : in.order.scannings;
' #txt
cn0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get original Scannings if ComboDoc</name>
        <nameStyle>34,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f11 126 148 36 24 20 -2 #rect
cn0 f11 @|CallSubIcon #fIcon
cn0 f4 expr out #txt
cn0 f4 135 172 102 216 #arcP
cn0 f27 type ch.soreco.orderbook.functional.Order #txt
cn0 f27 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>IL?</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f27 82 306 28 28 14 0 #rect
cn0 f27 @|AlternativeIcon #fIcon
cn0 f29 expr out #txt
cn0 f29 96 284 96 306 #arcP
cn0 f30 type ch.soreco.orderbook.functional.Order #txt
cn0 f30 processCall 'Functional Processes/scanning/OrderMatching:get(ch.soreco.orderbook.bo.OrderMatching)' #txt
cn0 f30 doCall true #txt
cn0 f30 requestActionDecl '<ch.soreco.orderbook.bo.OrderMatching filter> param;
' #txt
cn0 f30 requestMappingAction 'param.filter.orderType=in.order.orderType;
' #txt
cn0 f30 responseActionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f30 responseMappingAction 'out=in;
out.order.scanning.docType=in.order.scanning.docType.equalsIgnoreCase("FO")? in.order.scanning.docType : result.orderMatching.mapToDocType.trim().length() > 0 ? result.orderMatching.mapToDocType : result.orderMatching.docType;
' #txt
cn0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(OrderMatching)</name>
        <nameStyle>18,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f30 158 308 36 24 20 -2 #rect
cn0 f30 @|CallSubIcon #fIcon
cn0 f32 expr out #txt
cn0 f32 158 331 114 357 #arcP
cn0 f33 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>17.11.2011 / PB / ITS-998 &amp; ITS-1052
check if docType = IL
=&gt; pick the current orderType to docType mapping of the order</name>
        <nameStyle>37,7
83,7
</nameStyle>
    </language>
</elementInfo>
' #txt
cn0 f33 354 290 60 60 9 -23 #rect
cn0 f33 @|IBIcon #fIcon
cn0 f33 -1|-1|-16777216 #nodeStyle
cn0 f34 354 320 194 320 #arcP
cn0 f35 inParamDecl '<ch.soreco.orderbook.bo.Order order,java.lang.Number masterOrderId> param;' #txt
cn0 f35 inParamTable 'out.order=param.order;
out.order.matchedToOrder=param.masterOrderId;
' #txt
cn0 f35 outParamDecl '<> result;
' #txt
cn0 f35 actionDecl 'ch.soreco.orderbook.functional.Order out;
' #txt
cn0 f35 callSignature call(ch.soreco.orderbook.bo.Order,Number) #txt
cn0 f35 type ch.soreco.orderbook.functional.Order #txt
cn0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Order,Number)</name>
    </language>
</elementInfo>
' #txt
cn0 f35 227 35 26 26 14 0 #rect
cn0 f35 @|StartSubIcon #fIcon
cn0 f36 expr out #txt
cn0 f36 240 61 114 104 #arcP
cn0 f36 1 240 104 #addKink
cn0 f36 1 0.2876275893240214 0 0 #arcLabel
cn0 f31 expr in #txt
cn0 f31 outCond '!in.order.scanning.docType.equalsIgnoreCase("IL") && !in.order.scanning.docType.equalsIgnoreCase("")' #txt
cn0 f31 110 320 158 320 #arcP
cn0 f5 expr in #txt
cn0 f5 96 334 96 356 #arcP
cn0 f37 expr out #txt
cn0 f37 96 116 96 210 #arcP
>Proto cn0 .type ch.soreco.orderbook.functional.Order #txt
>Proto cn0 .processKind CALLABLE_SUB #txt
>Proto cn0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto cn0 0 0 32 24 18 0 #rect
>Proto cn0 @|BIcon #fIcon
cn0 f10 mainOut f9 tail #connect
cn0 f9 head f3 in #connect
cn0 f0 mainOut f13 tail #connect
cn0 f13 head f12 mainIn #connect
cn0 f7 head f6 mainIn #connect
cn0 f3 out f7 tail #connect
cn0 f2 mainOut f17 tail #connect
cn0 f17 head f16 in #connect
cn0 f19 head f18 mainIn #connect
cn0 f18 mainOut f20 tail #connect
cn0 f20 head f10 mainIn #connect
cn0 f3 out f22 tail #connect
cn0 f22 head f21 in #connect
cn0 f21 out f15 tail #connect
cn0 f15 head f14 mainIn #connect
cn0 f14 mainOut f24 tail #connect
cn0 f24 head f23 in #connect
cn0 f23 out f8 tail #connect
cn0 f8 head f1 mainIn #connect
cn0 f21 out f25 tail #connect
cn0 f25 head f23 in #connect
cn0 f16 out f28 tail #connect
cn0 f28 head f10 mainIn #connect
cn0 f16 out f19 tail #connect
cn0 f11 mainOut f4 tail #connect
cn0 f4 head f3 in #connect
cn0 f6 mainOut f29 tail #connect
cn0 f29 head f27 in #connect
cn0 f30 mainOut f32 tail #connect
cn0 f32 head f2 mainIn #connect
cn0 f33 ao f34 tail #connect
cn0 f34 head f30 @CG|ai #connect
cn0 f35 mainOut f36 tail #connect
cn0 f36 head f12 mainIn #connect
cn0 f27 out f31 tail #connect
cn0 f31 head f30 mainIn #connect
cn0 f27 out f5 tail #connect
cn0 f5 head f2 mainIn #connect
cn0 f12 mainOut f37 tail #connect
cn0 f37 head f3 in #connect
