[Ivy]
[>Created: Thu Nov 10 09:56:03 CET 2011]
12DC1F41EC0D1DAD 3.15 #module
>Proto >Proto Collection #zClass
gg0 getOrdersViaSubstateOfScanning Big #zClass
gg0 B #cInfo
gg0 #process
gg0 @TextInP .resExport .resExport #zField
gg0 @TextInP .type .type #zField
gg0 @TextInP .processKind .processKind #zField
gg0 @AnnotationInP-0n ai ai #zField
gg0 @TextInP .xml .xml #zField
gg0 @TextInP .responsibility .responsibility #zField
gg0 @StartSub f0 '' #zField
gg0 @EndSub f1 '' #zField
gg0 @CallSub f3 '' #zField
gg0 @PushWFArc f4 '' #zField
gg0 @GridStep f5 '' #zField
gg0 @Alternative f7 '' #zField
gg0 @PushWFArc f8 '' #zField
gg0 @CallSub f9 '' #zField
gg0 @GridStep f11 '' #zField
gg0 @PushWFArc f12 '' #zField
gg0 @PushWFArc f10 '' #zField
gg0 @GridStep f13 '' #zField
gg0 @GridStep f15 '' #zField
gg0 @PushWFArc f16 '' #zField
gg0 @PushWFArc f17 '' #zField
gg0 @GridStep f18 '' #zField
gg0 @PushWFArc f19 '' #zField
gg0 @Alternative f20 '' #zField
gg0 @PushWFArc f21 '' #zField
gg0 @PushWFArc f6 '' #zField
gg0 @Alternative f22 '' #zField
gg0 @PushWFArc f23 '' #zField
gg0 @PushWFArc f2 '' #zField
gg0 @PushWFArc f24 '' #zField
gg0 @Alternative f25 '' #zField
gg0 @PushWFArc f26 '' #zField
gg0 @PushWFArc f14 '' #zField
gg0 @PushWFArc f27 '' #zField
gg0 @InfoButton f28 '' #zField
>Proto gg0 gg0 getOrdersViaSubstateOfScanning #zField
gg0 f0 inParamDecl '<java.lang.String archiveState> param;' #txt
gg0 f0 inParamTable 'out.scanning.archivingState=param.archiveState;
' #txt
gg0 f0 outParamDecl '<ch.ivyteam.ivy.scripting.objects.Recordset orderRecordset,List<ch.soreco.orderbook.bo.Order> orderList> result;
' #txt
gg0 f0 outParamTable 'result.orderRecordset=in.recordset;
result.orderList=in.orders;
' #txt
gg0 f0 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f0 callSignature call(String) #txt
gg0 f0 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(String)</name>
        <nameStyle>12,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f0 211 35 26 26 14 0 #rect
gg0 f0 @|StartSubIcon #fIcon
gg0 f1 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f1 211 787 26 26 14 0 #rect
gg0 f1 @|EndSubIcon #fIcon
gg0 f3 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f3 processCall 'Functional Processes/scanning/Scanning:getScanningList(ch.soreco.orderbook.bo.Scanning)' #txt
gg0 f3 doCall true #txt
gg0 f3 requestActionDecl '<ch.soreco.orderbook.bo.Scanning Scanning> param;
' #txt
gg0 f3 requestMappingAction 'param.Scanning.archivingState=in.scanning.archivingState;
' #txt
gg0 f3 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f3 responseMappingAction 'out=in;
out.scannings=result.Scannings;
out.success=result.success;
' #txt
gg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Scannings</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f3 206 92 36 24 20 -2 #rect
gg0 f3 @|CallSubIcon #fIcon
gg0 f4 expr out #txt
gg0 f4 224 61 224 92 #arcP
gg0 f5 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f5 actionTable 'out=in;
out.i=0;
' #txt
gg0 f5 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reset i</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f5 206 212 36 24 20 -2 #rect
gg0 f5 @|StepIcon #fIcon
gg0 f7 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Loop Scannings i</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f7 210 266 28 28 14 0 #rect
gg0 f7 @|AlternativeIcon #fIcon
gg0 f8 expr out #txt
gg0 f8 224 236 224 266 #arcP
gg0 f9 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f9 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
gg0 f9 doCall true #txt
gg0 f9 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
gg0 f9 requestMappingAction 'param.filter.orderId=in.scanning.orderId;
' #txt
gg0 f9 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f9 responseMappingAction 'out=in;
out.order=result.order;
out.success=result.success;
' #txt
gg0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(Order)</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f9 406 332 36 24 20 -2 #rect
gg0 f9 @|CallSubIcon #fIcon
gg0 f11 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f11 actionTable 'out=in;
out.scanning=in.scannings.get(in.i);
' #txt
gg0 f11 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Scanning</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f11 406 268 36 24 20 -2 #rect
gg0 f11 @|StepIcon #fIcon
gg0 f12 expr in #txt
gg0 f12 outCond 'in.i < in.scannings.size()' #txt
gg0 f12 238 280 406 280 #arcP
gg0 f10 expr out #txt
gg0 f10 424 292 424 332 #arcP
gg0 f13 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f13 actionTable 'out=in;
' #txt
gg0 f13 actionCode 'if(!in.orders.contains(in.order))
{
		//in.order.scanning = in.scanning;
		in.orders.add(in.order);
}

' #txt
gg0 f13 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>add Order</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f13 406 460 36 24 20 -2 #rect
gg0 f13 @|StepIcon #fIcon
gg0 f15 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f15 actionTable 'out=in;
out.i=in.i + 1;
' #txt
gg0 f15 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f15 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>i++</name>
        <nameStyle>3
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f15 406 532 36 24 20 -2 #rect
gg0 f15 @|StepIcon #fIcon
gg0 f16 expr out #txt
gg0 f16 424 484 424 532 #arcP
gg0 f17 expr out #txt
gg0 f17 406 544 233 286 #arcP
gg0 f17 1 360 544 #addKink
gg0 f17 2 360 368 #addKink
gg0 f17 1 0.5355052156766322 0 0 #arcLabel
gg0 f18 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
gg0 f18 actionTable 'out=in;
' #txt
gg0 f18 actionCode in.recordset.addAll(in.orders); #txt
gg0 f18 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Orders in Recordset</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f18 206 588 36 24 20 -2 #rect
gg0 f18 @|StepIcon #fIcon
gg0 f19 expr in #txt
gg0 f19 224 294 224 588 #arcP
gg0 f19 0 0.36650470768195387 0 0 #arcLabel
gg0 f20 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f20 210 146 28 28 14 0 #rect
gg0 f20 @|AlternativeIcon #fIcon
gg0 f21 expr out #txt
gg0 f21 224 116 224 146 #arcP
gg0 f6 expr in #txt
gg0 f6 6 #arcStyle
gg0 f6 224 174 224 212 #arcP
gg0 f22 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f22 210 714 28 28 14 0 #rect
gg0 f22 @|AlternativeIcon #fIcon
gg0 f23 expr out #txt
gg0 f23 224 612 224 714 #arcP
gg0 f23 0 0.28675743211423854 0 0 #arcLabel
gg0 f2 expr in #txt
gg0 f2 224 742 224 787 #arcP
gg0 f24 expr in #txt
gg0 f24 3 #arcStyle
gg0 f24 210 160 210 728 #arcP
gg0 f24 1 120 160 #addKink
gg0 f24 2 120 728 #addKink
gg0 f24 1 0.4269981452523815 0 0 #arcLabel
gg0 f25 type ch.soreco.filehandling.functional.Archiving #txt
gg0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f25 410 394 28 28 14 0 #rect
gg0 f25 @|AlternativeIcon #fIcon
gg0 f26 expr out #txt
gg0 f26 424 356 424 394 #arcP
gg0 f14 expr in #txt
gg0 f14 outCond in.success #txt
gg0 f14 6 #arcStyle
gg0 f14 424 422 424 460 #arcP
gg0 f27 expr in #txt
gg0 f27 3 #arcStyle
gg0 f27 438 408 442 544 #arcP
gg0 f27 1 512 408 #addKink
gg0 f27 2 512 544 #addKink
gg0 f27 1 0.3646965326056293 0 0 #arcLabel
gg0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Description
--------------
Returns all Orders of a given SubState / ArchivingState


Changes
-----------
18.02.2011 / PB: change in [add Order] to deliver  only one Order if multiple scannings with given SubState</name>
        <nameStyle>212
</nameStyle>
    </language>
</elementInfo>
' #txt
gg0 f28 402 25 60 143 35 -68 #rect
gg0 f28 @|IBIcon #fIcon
gg0 f28 -1|-1|-16777216 #nodeStyle
>Proto gg0 .type ch.soreco.filehandling.functional.Archiving #txt
>Proto gg0 .processKind CALLABLE_SUB #txt
>Proto gg0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto gg0 0 0 32 24 18 0 #rect
>Proto gg0 @|BIcon #fIcon
gg0 f0 mainOut f4 tail #connect
gg0 f4 head f3 mainIn #connect
gg0 f5 mainOut f8 tail #connect
gg0 f8 head f7 in #connect
gg0 f7 out f12 tail #connect
gg0 f12 head f11 mainIn #connect
gg0 f11 mainOut f10 tail #connect
gg0 f10 head f9 mainIn #connect
gg0 f13 mainOut f16 tail #connect
gg0 f16 head f15 mainIn #connect
gg0 f15 mainOut f17 tail #connect
gg0 f17 head f7 in #connect
gg0 f7 out f19 tail #connect
gg0 f19 head f18 mainIn #connect
gg0 f3 mainOut f21 tail #connect
gg0 f21 head f20 in #connect
gg0 f20 out f6 tail #connect
gg0 f6 head f5 mainIn #connect
gg0 f18 mainOut f23 tail #connect
gg0 f23 head f22 in #connect
gg0 f22 out f2 tail #connect
gg0 f2 head f1 mainIn #connect
gg0 f20 out f24 tail #connect
gg0 f24 head f22 in #connect
gg0 f9 mainOut f26 tail #connect
gg0 f26 head f25 in #connect
gg0 f25 out f14 tail #connect
gg0 f14 head f13 mainIn #connect
gg0 f25 out f27 tail #connect
gg0 f27 head f15 mainIn #connect
