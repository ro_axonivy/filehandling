[Ivy]
[>Created: Mon Mar 12 11:11:42 CET 2012]
12DBCD803C6EE630 3.15 #module
>Proto >Proto Collection #zClass
Ce0 CreateIndexFile Big #zClass
Ce0 B #cInfo
Ce0 #process
Ce0 @TextInP .resExport .resExport #zField
Ce0 @TextInP .type .type #zField
Ce0 @TextInP .processKind .processKind #zField
Ce0 @AnnotationInP-0n ai ai #zField
Ce0 @TextInP .xml .xml #zField
Ce0 @TextInP .responsibility .responsibility #zField
Ce0 @StartSub f0 '' #zField
Ce0 @EndSub f1 '' #zField
Ce0 @GridStep f2 '' #zField
Ce0 @CallSub f3 '' #zField
Ce0 @CallSub f9 '' #zField
Ce0 @PushWFArc f7 '' #zField
Ce0 @Alternative f8 '' #zField
Ce0 @PushWFArc f5 '' #zField
Ce0 @Alternative f11 '' #zField
Ce0 @PushWFArc f12 '' #zField
Ce0 @PushWFArc f6 '' #zField
Ce0 @GridStep f25 '' #zField
Ce0 @PushWFArc f14 '' #zField
Ce0 @PushWFArc f13 '' #zField
Ce0 @CallSub f30 '' #zField
Ce0 @PushWFArc f10 '' #zField
Ce0 @CallSub f16 '' #zField
Ce0 @PushWFArc f17 '' #zField
Ce0 @PushWFArc f4 '' #zField
Ce0 @GridStep f18 '' #zField
Ce0 @PushWFArc f15 '' #zField
Ce0 @Alternative f20 '' #zField
Ce0 @PushWFArc f21 '' #zField
Ce0 @PushWFArc f19 '' #zField
Ce0 @PushWFArc f22 '' #zField
>Proto Ce0 Ce0 CreateIndexFile #zField
Ce0 f0 inParamDecl '<java.lang.Number masterOrderId,ch.soreco.orderbook.bo.Scanning scanning> param;' #txt
Ce0 f0 inParamTable 'out.Order.matchedToOrder=param.masterOrderId;
out.Scan=param.scanning;
out.success=true;
' #txt
Ce0 f0 outParamDecl '<java.lang.Boolean success> result;
' #txt
Ce0 f0 outParamTable 'result.success=in.success;
' #txt
Ce0 f0 actionDecl 'filehandling.Data out;
' #txt
Ce0 f0 callSignature call(Number,ch.soreco.orderbook.bo.Scanning) #txt
Ce0 f0 type filehandling.Data #txt
Ce0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Scanning)</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f0 83 35 26 26 14 0 #rect
Ce0 f0 @|StartSubIcon #fIcon
Ce0 f1 type filehandling.Data #txt
Ce0 f1 83 627 26 26 14 0 #rect
Ce0 f1 @|EndSubIcon #fIcon
Ce0 f2 actionDecl 'filehandling.Data out;
' #txt
Ce0 f2 actionTable 'out=in;
out.success=false;
' #txt
Ce0 f2 actionCode 'import java.io.IOException;
import ch.soreco.filehandling.XmlCreator;

try
{
	XmlCreator xmlCreator = new XmlCreator(in.Scan, in.orderNr, in.Order, ivy.var.IMTFTarget);
	in.success = true;
} 

catch (Exception ex)
{
	ivy.log.error("Error on creating index file", ex);
	in.success = false;
} ' #txt
Ce0 f2 type filehandling.Data #txt
Ce0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>createXml</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f2 78 468 36 24 20 -2 #rect
Ce0 f2 @|StepIcon #fIcon
Ce0 f3 type filehandling.Data #txt
Ce0 f3 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
Ce0 f3 doCall true #txt
Ce0 f3 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
Ce0 f3 requestMappingAction 'param.filter.orderId=in.Scan.orderId;
' #txt
Ce0 f3 responseActionDecl 'filehandling.Data out;
' #txt
Ce0 f3 responseMappingAction 'out=in;
out.Order=result.order;
out.success=result.success;
' #txt
Ce0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get Order</name>
        <nameStyle>9,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f3 78 132 36 24 20 -2 #rect
Ce0 f3 @|CallSubIcon #fIcon
Ce0 f9 type filehandling.Data #txt
Ce0 f9 processCall 'Functional Processes/Archiving/getHighestOrderNr:call(ch.soreco.orderbook.bo.Order)' #txt
Ce0 f9 doCall true #txt
Ce0 f9 requestActionDecl '<ch.soreco.orderbook.bo.Order order> param;
' #txt
Ce0 f9 requestMappingAction 'param.order=in.Order;
' #txt
Ce0 f9 responseActionDecl 'filehandling.Data out;
' #txt
Ce0 f9 responseMappingAction 'out=in;
out.orderNr=result.orderNr;
' #txt
Ce0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get OrderNr</name>
        <nameStyle>11,7
</nameStyle>
        <desc>also set the matchedToOrder id </desc>
    </language>
</elementInfo>
' #txt
Ce0 f9 78 196 36 24 20 -2 #rect
Ce0 f9 @|CallSubIcon #fIcon
Ce0 f9 -613726|-1|-16777216 #nodeStyle
Ce0 f7 expr out #txt
Ce0 f7 96 156 96 196 #arcP
Ce0 f8 type filehandling.Data #txt
Ce0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f8 82 394 28 28 14 0 #rect
Ce0 f8 @|AlternativeIcon #fIcon
Ce0 f5 expr in #txt
Ce0 f5 outCond in.success #txt
Ce0 f5 6 #arcStyle
Ce0 f5 96 422 96 468 #arcP
Ce0 f11 type filehandling.Data #txt
Ce0 f11 82 538 28 28 14 0 #rect
Ce0 f11 @|AlternativeIcon #fIcon
Ce0 f12 expr out #txt
Ce0 f12 96 492 96 538 #arcP
Ce0 f6 expr in #txt
Ce0 f6 96 566 96 627 #arcP
Ce0 f25 actionDecl 'filehandling.Data out;
' #txt
Ce0 f25 actionTable 'out=in;
' #txt
Ce0 f25 actionCode 'ivy.log.error("Error on creating index file: Order not found (OrderId: " + in.Scan.orderId + ")");
in.success = false;' #txt
Ce0 f25 type filehandling.Data #txt
Ce0 f25 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Log Error (Order not found)</name>
        <nameStyle>27
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f25 230 468 36 24 23 -7 #rect
Ce0 f25 @|StepIcon #fIcon
Ce0 f25 -13016147|-1|-16777216 #nodeStyle
Ce0 f14 expr in #txt
Ce0 f14 110 408 248 468 #arcP
Ce0 f14 1 248 408 #addKink
Ce0 f14 1 0.5001001819221665 0 0 #arcLabel
Ce0 f13 expr out #txt
Ce0 f13 248 492 110 552 #arcP
Ce0 f13 1 248 552 #addKink
Ce0 f13 1 0.28598995975869906 0 0 #arcLabel
Ce0 f30 type filehandling.Data #txt
Ce0 f30 processCall 'Functional Processes/scanning/OrderMatching:get(ch.soreco.orderbook.bo.OrderMatching)' #txt
Ce0 f30 doCall true #txt
Ce0 f30 requestActionDecl '<ch.soreco.orderbook.bo.OrderMatching filter> param;
' #txt
Ce0 f30 requestMappingAction 'param.filter.orderType=in.masterOrder.orderType;
' #txt
Ce0 f30 responseActionDecl 'filehandling.Data out;
' #txt
Ce0 f30 responseMappingAction 'out=in;
out.Scan.docType=in.Scan.docType.equalsIgnoreCase("FO") ? in.Scan.docType : (result.orderMatching.mapToDocType.trim().length() > 0 || result.orderMatching.docType.trim().length() > 0) && (in.Scan.isMain || (in.Scan.isMain && in.Scan.isDossier))?  
result.orderMatching.mapToDocType.trim().length() > 0  ? result.orderMatching.mapToDocType : result.orderMatching.docType 
: in.Scan.docType;
' #txt
Ce0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get(OrderMatching)</name>
        <nameStyle>18,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f30 78 340 36 24 20 -2 #rect
Ce0 f30 @|CallSubIcon #fIcon
Ce0 f10 expr out #txt
Ce0 f10 96 364 96 394 #arcP
Ce0 f16 type filehandling.Data #txt
Ce0 f16 processCall 'Functional Processes/orderbook/Order:get(ch.soreco.orderbook.bo.Order)' #txt
Ce0 f16 doCall true #txt
Ce0 f16 requestActionDecl '<ch.soreco.orderbook.bo.Order filter> param;
' #txt
Ce0 f16 requestMappingAction 'param.filter.orderId=in.Order.matchedToOrder;
' #txt
Ce0 f16 responseActionDecl 'filehandling.Data out;
' #txt
Ce0 f16 responseMappingAction 'out=in;
out.masterOrder=result.order;
' #txt
Ce0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get master order</name>
        <nameStyle>16,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f16 78 84 36 24 20 -2 #rect
Ce0 f16 @|CallSubIcon #fIcon
Ce0 f17 expr out #txt
Ce0 f17 96 61 96 84 #arcP
Ce0 f4 expr out #txt
Ce0 f4 96 108 96 132 #arcP
Ce0 f18 actionDecl 'filehandling.Data out;
' #txt
Ce0 f18 actionTable 'out=in;
out.Order.BPId=in.masterOrder.BPId;
out.Order.matchedToOrder=in.masterOrder.orderId;
' #txt
Ce0 f18 type filehandling.Data #txt
Ce0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>remap master order data</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ce0 f18 78 292 36 24 20 -2 #rect
Ce0 f18 @|StepIcon #fIcon
Ce0 f15 expr out #txt
Ce0 f15 96 316 96 340 #arcP
Ce0 f20 type filehandling.Data #txt
Ce0 f20 82 250 28 28 14 0 #rect
Ce0 f20 @|AlternativeIcon #fIcon
Ce0 f21 expr out #txt
Ce0 f21 96 220 96 250 #arcP
Ce0 f19 expr in #txt
Ce0 f19 outCond 'in.masterOrder.BPId.trim().length() > 0' #txt
Ce0 f19 96 278 96 292 #arcP
Ce0 f22 expr in #txt
Ce0 f22 82 264 82 408 #arcP
Ce0 f22 1 24 264 #addKink
Ce0 f22 2 24 408 #addKink
Ce0 f22 1 0.37208556228844225 0 0 #arcLabel
>Proto Ce0 .type filehandling.Data #txt
>Proto Ce0 .processKind CALLABLE_SUB #txt
>Proto Ce0 0 0 32 24 18 0 #rect
>Proto Ce0 @|BIcon #fIcon
Ce0 f3 mainOut f7 tail #connect
Ce0 f7 head f9 mainIn #connect
Ce0 f8 out f5 tail #connect
Ce0 f5 head f2 mainIn #connect
Ce0 f2 mainOut f12 tail #connect
Ce0 f12 head f11 in #connect
Ce0 f11 out f6 tail #connect
Ce0 f6 head f1 mainIn #connect
Ce0 f8 out f14 tail #connect
Ce0 f14 head f25 mainIn #connect
Ce0 f25 mainOut f13 tail #connect
Ce0 f13 head f11 in #connect
Ce0 f30 mainOut f10 tail #connect
Ce0 f10 head f8 in #connect
Ce0 f0 mainOut f17 tail #connect
Ce0 f17 head f16 mainIn #connect
Ce0 f16 mainOut f4 tail #connect
Ce0 f4 head f3 mainIn #connect
Ce0 f18 mainOut f15 tail #connect
Ce0 f15 head f30 mainIn #connect
Ce0 f9 mainOut f21 tail #connect
Ce0 f21 head f20 in #connect
Ce0 f20 out f19 tail #connect
Ce0 f19 head f18 mainIn #connect
Ce0 f20 out f22 tail #connect
Ce0 f22 head f8 in #connect
