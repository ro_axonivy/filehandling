[Ivy]
[>Created: Tue Jun 21 11:46:16 CEST 2016]
12D98A23438753EE 3.17 #module
>Proto >Proto Collection #zClass
On0 OrderPreparation Big #zClass
On0 B #cInfo
On0 #process
On0 @TextInP .resExport .resExport #zField
On0 @TextInP .type .type #zField
On0 @TextInP .processKind .processKind #zField
On0 @AnnotationInP-0n ai ai #zField
On0 @TextInP .xml .xml #zField
On0 @TextInP .responsibility .responsibility #zField
On0 @StartSub f0 '' #zField
On0 @EndSub f1 '' #zField
On0 @Alternative f3 '' #zField
On0 @GridStep f16 '' #zField
On0 @Alternative f18 '' #zField
On0 @PushWFArc f7 '' #zField
On0 @PushWFArc f20 '' #zField
On0 @CallSub f8 '' #zField
On0 @Alternative f23 '' #zField
On0 @PushWFArc f24 '' #zField
On0 @Alternative f12 '' #zField
On0 @PushWFArc f15 '' #zField
On0 @Alternative f17 '' #zField
On0 @PushWFArc f32 '' #zField
On0 @PushWFArc f19 '' #zField
On0 @Alternative f21 '' #zField
On0 @PushWFArc f22 '' #zField
On0 @Alternative f33 '' #zField
On0 @CallSub f35 '' #zField
On0 @PushWFArc f36 '' #zField
On0 @PushWFArc f34 '' #zField
On0 @CallSub f37 '' #zField
On0 @PushWFArc f38 '' #zField
On0 @PushWFArc f39 '' #zField
On0 @GridStep f5 '' #zField
On0 @PushWFArc f6 '' #zField
On0 @PushWFArc f4 '' #zField
On0 @PushWFArc f10 '' #zField
On0 @PushWFArc f14 '' #zField
On0 @PushWFArc f9 '' #zField
On0 @GridStep f11 '' #zField
On0 @PushWFArc f13 '' #zField
On0 @PushWFArc f25 '' #zField
On0 @InfoButton f2 '' #zField
On0 @AnnotationArc f26 '' #zField
>Proto On0 On0 OrderPreparation #zField
On0 f0 inParamDecl '<java.lang.Number masterOrderId,ch.soreco.orderbook.bo.Scanning scanning> param;' #txt
On0 f0 inParamTable 'out.order.matchedToOrder=param.masterOrderId;
out.scanning=param.scanning;
' #txt
On0 f0 outParamDecl '<java.lang.Boolean success> result;
' #txt
On0 f0 outParamTable 'result.success=in.success;
' #txt
On0 f0 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
On0 f0 callSignature call(Number,ch.soreco.orderbook.bo.Scanning) #txt
On0 f0 type ch.soreco.filehandling.functional.Archiving #txt
On0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call(Scanning)</name>
        <nameStyle>14,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f0 155 27 26 26 14 0 #rect
On0 f0 @|StartSubIcon #fIcon
On0 f1 type ch.soreco.filehandling.functional.Archiving #txt
On0 f1 155 691 26 26 14 0 #rect
On0 f1 @|EndSubIcon #fIcon
On0 f3 type ch.soreco.filehandling.functional.Archiving #txt
On0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is flagged as archived?</name>
        <nameStyle>23
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f3 290 114 28 28 15 0 #rect
On0 f3 @|AlternativeIcon #fIcon
On0 f3 -13016147|-1|-16777216 #nodeStyle
On0 f16 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
On0 f16 actionTable 'out=in;
out.scanning.archivingPreparedDate=new DateTime();
out.scanning.archivingState=ch.soreco.orderbook.enums.ArchivingState.IMTF_ARCHIVED.toString();
' #txt
On0 f16 actionCode ivy.persistence.Adressmutation.merge(in.scanning); #txt
On0 f16 type ch.soreco.filehandling.functional.Archiving #txt
On0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Set substate to IMTF_ARCHIVED &amp; preparedDate to now</name>
        <nameStyle>51,7
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f16 150 572 36 24 20 -2 #rect
On0 f16 @|StepIcon #fIcon
On0 f18 type ch.soreco.filehandling.functional.Archiving #txt
On0 f18 154 634 28 28 14 0 #rect
On0 f18 @|AlternativeIcon #fIcon
On0 f7 expr in #txt
On0 f7 168 662 168 691 #arcP
On0 f20 expr in #txt
On0 f20 outCond !in.scanning.archivingPreparedDate.toString().isEmpty() #txt
On0 f20 6 #arcStyle
On0 f20 290 128 154 648 #arcP
On0 f20 1 16 128 #addKink
On0 f20 2 16 648 #addKink
On0 f20 1 0.34694500044597587 0 0 #arcLabel
On0 f8 type ch.soreco.filehandling.functional.Archiving #txt
On0 f8 processCall 'Functional Processes/Archiving/CreateIndexFile:call(Number,ch.soreco.orderbook.bo.Scanning)' #txt
On0 f8 doCall true #txt
On0 f8 requestActionDecl '<java.lang.Number masterOrderId,ch.soreco.orderbook.bo.Scanning scanning> param;
' #txt
On0 f8 requestMappingAction 'param.masterOrderId=in.order.matchedToOrder;
param.scanning=in.scanning;
' #txt
On0 f8 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
On0 f8 responseMappingAction 'out=in;
out.success=result.success;
' #txt
On0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>create Indexfile</name>
        <nameStyle>16,7
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f8 150 348 36 24 20 -2 #rect
On0 f8 @|CallSubIcon #fIcon
On0 f23 type ch.soreco.filehandling.functional.Archiving #txt
On0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f23 154 410 28 28 14 0 #rect
On0 f23 @|AlternativeIcon #fIcon
On0 f24 expr out #txt
On0 f24 168 372 168 410 #arcP
On0 f12 type ch.soreco.filehandling.functional.Archiving #txt
On0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Success?</name>
        <nameStyle>8
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f12 154 514 28 28 14 0 #rect
On0 f12 @|AlternativeIcon #fIcon
On0 f15 expr in #txt
On0 f15 outCond in.success #txt
On0 f15 6 #arcStyle
On0 f15 168 542 168 572 #arcP
On0 f17 type ch.soreco.filehandling.functional.Archiving #txt
On0 f17 506 514 28 28 14 0 #rect
On0 f17 @|AlternativeIcon #fIcon
On0 f32 expr in #txt
On0 f32 182 528 506 528 #arcP
On0 f19 expr out #txt
On0 f19 168 596 168 634 #arcP
On0 f21 type ch.soreco.filehandling.functional.Archiving #txt
On0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>is documenttype = FO?</name>
        <nameStyle>21,7
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f21 154 178 28 28 14 0 #rect
On0 f21 @|AlternativeIcon #fIcon
On0 f22 expr in #txt
On0 f22 304 142 182 192 #arcP
On0 f22 1 304 192 #addKink
On0 f22 0 1.0 0 0 #arcLabel
On0 f33 type ch.soreco.filehandling.functional.Archiving #txt
On0 f33 154 282 28 28 14 0 #rect
On0 f33 @|AlternativeIcon #fIcon
On0 f35 type ch.soreco.filehandling.functional.Archiving #txt
On0 f35 processCall 'Functional Processes/common/File:get(ch.soreco.common.bo.File)' #txt
On0 f35 doCall true #txt
On0 f35 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
On0 f35 requestMappingAction 'param.filter.fileId=in.scanning.scanFileId;
' #txt
On0 f35 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
On0 f35 responseMappingAction 'out=in;
out.archivingFile=result.file;
out.scanning.scanFile=result.file.getFileName();
' #txt
On0 f35 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get file in high resolution</name>
        <nameStyle>27
</nameStyle>
        <desc>AI 07.02.2011
scanning.scanfile ist neu ein String und keine java.io.File mehr!!!!</desc>
    </language>
</elementInfo>
' #txt
On0 f35 38 228 36 24 20 -2 #rect
On0 f35 @|CallSubIcon #fIcon
On0 f36 expr in #txt
On0 f36 outCond in.scanning.getDocType().equalsIgnoreCase("FO")||in.scanning.scanMonoFileId==null||in.scanning.scanMonoFileId==0 #txt
On0 f36 158 196 74 232 #arcP
On0 f34 expr out #txt
On0 f34 74 249 159 291 #arcP
On0 f37 type ch.soreco.filehandling.functional.Archiving #txt
On0 f37 processCall 'Functional Processes/common/File:get(ch.soreco.common.bo.File)' #txt
On0 f37 doCall true #txt
On0 f37 requestActionDecl '<ch.soreco.common.bo.File filter> param;
' #txt
On0 f37 requestMappingAction 'param.filter.fileId=in.scanning.scanMonoFileId;
' #txt
On0 f37 responseActionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
On0 f37 responseMappingAction 'out=in;
out.archivingFile=result.file;
out.scanning.scanFile=result.file.getFileName();
' #txt
On0 f37 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get file in low resolution</name>
        <nameStyle>26
</nameStyle>
        <desc>AI 07.02.2011
scanning.scanfile ist neu ein String und keine java.io.File mehr!!!!</desc>
    </language>
</elementInfo>
' #txt
On0 f37 270 228 36 24 20 -2 #rect
On0 f37 @|CallSubIcon #fIcon
On0 f38 expr in #txt
On0 f38 178 196 270 233 #arcP
On0 f39 expr out #txt
On0 f39 270 248 178 292 #arcP
On0 f5 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
On0 f5 actionTable 'out=in;
' #txt
On0 f5 actionCode 'import ch.soreco.filehandling.Filehelper;

try
{
	String fileName = in.archivingFile.fileName.replace("PDF","pdf");
	String target = ivy.var.IMTFTarget + fileName.substring(0,fileName.lastIndexOf("."))+"_"+in.scanning.archivingDate.format("yyMMddhhmmss") + "\\" + fileName.replace("PDF","pdf").replace(".pdf","_"+in.scanning.archivingDate.format("yyMMddhhmmss")+".pdf");
	java.io.File outputFile = new java.io.File(target);
	Filehelper.copyFile(in.archivingFile.file, outputFile);
} catch (Exception ex)
{
	in.success = false;
	ivy.log.error("Error on copy index file to IMTF directory",ex);
}' #txt
On0 f5 type ch.soreco.filehandling.functional.Archiving #txt
On0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>copy File to Archiving Directory</name>
        <nameStyle>32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f5 150 460 36 24 20 -2 #rect
On0 f5 @|StepIcon #fIcon
On0 f6 expr in #txt
On0 f6 outCond in.success #txt
On0 f6 6 #arcStyle
On0 f6 168 438 168 460 #arcP
On0 f4 expr out #txt
On0 f4 168 484 168 514 #arcP
On0 f10 expr in #txt
On0 f10 182 424 520 514 #arcP
On0 f10 1 520 424 #addKink
On0 f10 0 0.6702080028471112 0 0 #arcLabel
On0 f14 expr in #txt
On0 f14 168 310 168 348 #arcP
On0 f9 expr in #txt
On0 f9 520 542 182 648 #arcP
On0 f9 1 520 648 #addKink
On0 f9 1 0.3297919971528887 0 0 #arcLabel
On0 f11 actionDecl 'ch.soreco.filehandling.functional.Archiving out;
' #txt
On0 f11 actionTable 'out=in;
out.success=true;
' #txt
On0 f11 actionCode 'out.scanning.archivingDate = new DateTime();' #txt
On0 f11 type ch.soreco.filehandling.functional.Archiving #txt
On0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init success</name>
        <nameStyle>12,7
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f11 150 68 36 24 19 -1 #rect
On0 f11 @|StepIcon #fIcon
On0 f13 expr out #txt
On0 f13 168 53 168 68 #arcP
On0 f25 expr out #txt
On0 f25 168 92 168 178 #arcP
On0 f2 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>17.11.2011 / PB
removed because of ITS-998</name>
        <nameStyle>16,7
26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
On0 f2 482 98 60 60 -14 -19 #rect
On0 f2 @|IBIcon #fIcon
On0 f2 -1|-1|-16777216 #nodeStyle
On0 f26 482 128 318 128 #arcP
>Proto On0 .type ch.soreco.filehandling.functional.Archiving #txt
>Proto On0 .processKind CALLABLE_SUB #txt
>Proto On0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto On0 0 0 32 24 18 0 #rect
>Proto On0 @|BIcon #fIcon
On0 f18 out f7 tail #connect
On0 f7 head f1 mainIn #connect
On0 f3 out f20 tail #connect
On0 f20 head f18 in #connect
On0 f8 mainOut f24 tail #connect
On0 f24 head f23 in #connect
On0 f12 out f15 tail #connect
On0 f15 head f16 mainIn #connect
On0 f12 out f32 tail #connect
On0 f32 head f17 in #connect
On0 f16 mainOut f19 tail #connect
On0 f19 head f18 in #connect
On0 f3 out f22 tail #connect
On0 f22 head f21 in #connect
On0 f21 out f36 tail #connect
On0 f36 head f35 mainIn #connect
On0 f35 mainOut f34 tail #connect
On0 f34 head f33 in #connect
On0 f21 out f38 tail #connect
On0 f38 head f37 mainIn #connect
On0 f37 mainOut f39 tail #connect
On0 f39 head f33 in #connect
On0 f23 out f6 tail #connect
On0 f6 head f5 mainIn #connect
On0 f5 mainOut f4 tail #connect
On0 f4 head f12 in #connect
On0 f23 out f10 tail #connect
On0 f10 head f17 in #connect
On0 f33 out f14 tail #connect
On0 f14 head f8 mainIn #connect
On0 f17 out f9 tail #connect
On0 f9 head f18 in #connect
On0 f0 mainOut f13 tail #connect
On0 f13 head f11 mainIn #connect
On0 f11 mainOut f25 tail #connect
On0 f25 head f21 in #connect
On0 f2 ao f26 tail #connect
On0 f26 head f3 @CG|ai #connect
