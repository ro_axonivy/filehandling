[Ivy]
[>Created: Thu Nov 10 09:53:01 CET 2011]
12D03D31A7BC47E5 3.15 #module
>Proto >Proto Collection #zClass
Hg0 Housekeeping Big #zClass
Hg0 B #cInfo
Hg0 #process
1b0 '1 Sub' Big #zClass
1b0 B #cInfo
Hg0 1b0 1b0 '1 Sub' #zField
Hg0 @TextInP .resExport .resExport #zField
Hg0 @TextInP .type .type #zField
Hg0 @TextInP .processKind .processKind #zField
Hg0 @AnnotationInP-0n ai ai #zField
Hg0 @TextInP .xml .xml #zField
Hg0 @TextInP .responsibility .responsibility #zField
Hg0 @StartSub f0 '' #zField
Hg0 @EndSub f1 '' #zField
Hg0 @PushWFArc f2 '' #zField
Hg0 @StartRequest f3 '' #zField
Hg0 @EndTask f6 '' #zField
Hg0 @GridStep f21 '' #zField
Hg0 @GridStep f30 '' #zField
Hg0 @Alternative f7 '' #zField
Hg0 @PushWFArc f32 '' #zField
Hg0 @GridStep f12 '' #zField
Hg0 @PushWFArc f34 '' #zField
Hg0 @Alternative f11 '' #zField
Hg0 @PushWFArc f39 '' #zField
Hg0 @GridStep f4 '' #zField
Hg0 @PushWFArc f41 '' #zField
Hg0 @PushWFArc f22 '' #zField
Hg0 @PushWFArc f8 '' #zField
Hg0 @Alternative f9 '' #zField
Hg0 @GridStep f13 '' #zField
Hg0 @GridStep f29 '' #zField
Hg0 @Alternative f31 '' #zField
Hg0 @GridStep f16 '' #zField
Hg0 @PushWFArc f18 '' #zField
Hg0 @PushWFArc f24 '' #zField
Hg0 @PushWFArc f35 '' #zField
Hg0 @PushWFArc f27 '' #zField
Hg0 @PushWFArc f10 '' #zField
Hg0 @PushWFArc f25 '' #zField
Hg0 @PushWFArc f23 '' #zField
Hg0 @PushWFArc f26 '' #zField
Hg0 @PushWFArc f14 '' #zField
Hg0 @PushWFArc f17 '' #zField
Hg0 @PushWFArc f15 '' #zField
>Proto Hg0 Hg0 Housekeeping #zField
1b0 @TextInP .resExport .resExport #zField
1b0 @TextInP .type .type #zField
1b0 @TextInP .processKind .processKind #zField
1b0 @AnnotationInP-0n ai ai #zField
1b0 @TextInP .xml .xml #zField
1b0 @TextInP .responsibility .responsibility #zField
1b0 @PushWFArc f31 '' #zField
1b0 @GridStep f33 '' #zField
1b0 @Alternative f18 '' #zField
1b0 @GridStep f13 '' #zField
1b0 @PushWFArc f42 '' #zField
1b0 @GridStep f23 '' #zField
1b0 @GridStep f28 '' #zField
1b0 @PushWFArc f24 '' #zField
1b0 @PushWFArc f19 '' #zField
1b0 @PushWFArc f36 '' #zField
1b0 @PushWFArc f29 '' #zField
1b0 @PushWFArc f16 '' #zField
1b0 @Alternative f17 '' #zField
1b0 @PushWFArc f26 '' #zField
1b0 @PushWFArc f35 '' #zField
1b0 @Alternative f9 '' #zField
1b0 @GridStep f14 '' #zField
1b0 @PushWFArc f27 '' #zField
1b0 @GridStep f10 '' #zField
1b0 @PushWFArc f25 '' #zField
1b0 @CallSub f20 '' #zField
1b0 @PushWFArc f8 '' #zField
1b0 @PushTrueWFInG-01 g0 '' #zField
1b0 @PushWFArc f0 '' #zField
1b0 @PushTrueWFOutG-01 g1 '' #zField
1b0 @PushWFArc f1 '' #zField
>Proto 1b0 1b0 '1 Sub' #zField
Hg0 1b0 .resExport export #txt
Hg0 1b0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language lang="en">
        <name>move files</name>
        <nameStyle>10,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 1b0 234 392 92 32 -28 -8 #rect
Hg0 1b0 @|BIcon #fIcon
Hg0 1b0 g1 0 16 #fFoot
Hg0 f0 outParamDecl '<> result;
' #txt
Hg0 f0 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f0 callSignature call() #txt
Hg0 f0 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>call()</name>
    </language>
</elementInfo>
' #txt
Hg0 f0 87 37 26 26 14 0 #rect
Hg0 f0 @|StartSubIcon #fIcon
Hg0 f1 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f1 87 287 26 26 14 0 #rect
Hg0 f1 @|EndSubIcon #fIcon
Hg0 f2 expr out #txt
Hg0 f2 100 63 100 287 #arcP
Hg0 f3 outLink houskeeping.ivp #txt
Hg0 f3 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f3 inParamDecl '<ch.soreco.orderbook.bo.ScanShare Share> param;' #txt
Hg0 f3 inParamTable 'out.Share=param.Share;
' #txt
Hg0 f3 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f3 guid 12D03EDF1CCBB3FC #txt
Hg0 f3 requestEnabled true #txt
Hg0 f3 triggerEnabled true #txt
Hg0 f3 callSignature houskeeping(ch.soreco.orderbook.bo.ScanShare) #txt
Hg0 f3 persist false #txt
Hg0 f3 startName Housekeeping #txt
Hg0 f3 taskData '#
#Fri Jul 22 09:45:16 CEST 2011
TaskTriggered.ROL=SYSTEM
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.NAM=Housekeeping
TaskTriggered.EXROL=SYSTEM
' #txt
Hg0 f3 caseData '#
#Fri Jul 22 09:45:16 CEST 2011
businessCreator.user=
businessMilestone.timestamp=
businessObject.code=
businessObject.docDb.code=
businessObject.folder.id=
businessObject.name=
businessPriority=
businessStart.timestamp=
case.description=
case.name=
correspondent.id=
mainContact.docDb.code=
mainContact.folder.id=
mainContact.id=
mainContact.name=
mainContact.type=
process.code=
process.name=
processCategory.code=
processCategory.name=
subType.code=
subType.name=
type.code=
type.name=
' #txt
Hg0 f3 showInStartList 0 #txt
Hg0 f3 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();taskUpdDef.setName(engine.expandMacros("Housekeeping"));
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("SYSTEM");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Hg0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>houskeeping(ScanShare)</name>
        <nameStyle>22,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f3 @C|.responsibility Everybody #txt
Hg0 f3 267 51 26 26 14 0 #rect
Hg0 f3 @|StartRequestIcon #fIcon
Hg0 f6 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f6 267 451 26 26 14 0 #rect
Hg0 f6 @|EndIcon #fIcon
Hg0 f21 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f21 actionTable 'out=in;
out.doneSuffix=".DONE";
out.error=null;
out.ScanLog.Errors=0;
out.ScanLog.ScanStart=new DateTime();
out.ScanLog.ScanType="Housekeeping";
out.ScanLog.ShareId=in.Share.id;
out.ScanLog.Successful=0;
out.ScanLog.Total=0;
out.ScanLog.Warnings=0;
out.warnSuffix=".WARN";
' #txt
Hg0 f21 actionCode 'import java.util.Calendar;

//get moving date
Calendar movingDate = Calendar.getInstance(); 
movingDate.add(Calendar.DATE,-1);

out.movingDate = movingDate.getTime().getDate();

//get delete date
Calendar deleteDate = Calendar.getInstance(); 
deleteDate.add(Calendar.DATE, - in.Share.ArchiveDeadlineDays);

out.deleteDate = deleteDate.getTime().getDate(); ' #txt
Hg0 f21 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f21 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>init</name>
        <nameStyle>4,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f21 262 100 36 24 20 -2 #rect
Hg0 f21 @|StepIcon #fIcon
Hg0 f30 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f30 actionTable 'out=in;
' #txt
Hg0 f30 actionCode 'import jcifs.smb.SmbFile;
import ch.soreco.filehandling.SmbHelper;

try {

//Declaration
	SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);
	String archiveName = in.Share.NetPath+in.Share.ArchiveFolder+"/";
	
	smb.mkdir(archiveName);

	out.achiveFolder = archiveName;
	out.success = true;

}
catch( Exception e ) {
	out.error ="Cannot found or create the archive folder on the Share.";
	ivy.log.error(in.error, e);
	out.success = false;
}' #txt
Hg0 f30 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f30 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get archive</name>
        <nameStyle>11
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f30 262 148 36 24 20 -2 #rect
Hg0 f30 @|StepIcon #fIcon
Hg0 f30 -13016147|-1|-16777216 #nodeStyle
Hg0 f7 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>found archive</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f7 266 178 28 28 14 0 #rect
Hg0 f7 @|AlternativeIcon #fIcon
Hg0 f7 -13016147|-1|-16777216 #nodeStyle
Hg0 f32 expr out #txt
Hg0 f32 280 124 280 148 #arcP
Hg0 f12 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f12 actionTable 'out=in;
' #txt
Hg0 f12 actionCode 'import ch.soreco.filehandling.SmbHelper;

String folderName;

try {

	SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);
	folderName = in.achiveFolder+in.movingDate.format("yyyyMMdd")+"/";
	
	smb.mkdir(folderName);

	out.achiveFolder = folderName;
	out.success = true;

}
catch( Exception e ) {
	out.error ="Cannot create the folder "+folderName+" in the archive.";
	ivy.log.error(in.error, e);
	out.success = false;
}' #txt
Hg0 f12 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f12 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get archiv folder</name>
        <nameStyle>17
</nameStyle>
        <desc>Create a directory for every day</desc>
    </language>
</elementInfo>
' #txt
Hg0 f12 262 300 36 24 20 -2 #rect
Hg0 f12 @|StepIcon #fIcon
Hg0 f34 expr out #txt
Hg0 f34 280 172 280 178 #arcP
Hg0 f11 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f11 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>found archive folder</name>
        <nameStyle>20
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f11 266 338 28 28 14 0 #rect
Hg0 f11 @|AlternativeIcon #fIcon
Hg0 f11 -13016147|-1|-16777216 #nodeStyle
Hg0 f39 expr out #txt
Hg0 f39 280 324 280 338 #arcP
Hg0 f4 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f4 actionTable 'out=in;
' #txt
Hg0 f4 actionCode 'import jcifs.smb.SmbFile;
import ch.soreco.filehandling.Filehelper;
import ch.soreco.filehandling.SmbHelper;

try {

//Declaration
	SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);

	for (String delFileName:smb.listFiles(in.achiveFolder)) {
		SmbFile delFile = new SmbFile(delFileName);
		if ( delFile.getName() <= in.deleteDate.format("yyyyMMdd")+"/"){
			smb.deleteDir(delFileName);
		}
	}
}
catch (Exception e) {
	out.error = "Cannot remove old files from archive during Housekeeping.";
	ivy.log.error(in.error,e);
	out.success = false;
}

' #txt
Hg0 f4 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f4 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>remove files</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f4 234 256 92 32 -33 -8 #rect
Hg0 f4 @|StepIcon #fIcon
Hg0 f41 expr out #txt
Hg0 f41 280 288 280 300 #arcP
Hg0 f41 0 0.16989688876400644 0 0 #arcLabel
Hg0 f22 expr in #txt
Hg0 f22 outCond in.success #txt
Hg0 f22 280 366 280 392 #arcP
Hg0 f8 280 424 280 451 #arcP
Hg0 f9 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>found directory?</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f9 578 338 28 28 13 1 #rect
Hg0 f9 @|AlternativeIcon #fIcon
Hg0 f9 -13016147|-1|-16777216 #nodeStyle
Hg0 f13 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f13 actionTable 'out=in;
' #txt
Hg0 f13 actionCode 'import jcifs.smb.SmbFile;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import ch.soreco.filehandling.SmbHelper;

SmbFile xDirectory;

try {

//Declaration
	SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);
	String folderDate = new Date().format("yyMMdd");
	out.success = true;

	//get directory one by one
	xDirectory = new SmbFile(in.rootFiles.removeGet(0));

	//verify if it has to be renamed
	if(	xDirectory.getName().endsWith("050/")
			&& !xDirectory.getName().startsWith("D0")
			&& xDirectory.getName().substring(7,13) < folderDate
			) {

				//store data
				out.Directory = xDirectory.getPath();
				out.allFiles = smb.listFiles(xDirectory.getPath());
				
				out.success = true;
			}

}
catch( Exception e ) {
	out.error = "Cannot read files from Register "+xDirectory.getName()+": \n"+
							e.getMessage();
	ivy.log.error(in.error,e);
	out.success = false;
}



' #txt
Hg0 f13 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get directory</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f13 574 276 36 24 20 -2 #rect
Hg0 f13 @|StepIcon #fIcon
Hg0 f29 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f29 actionTable 'out=in;
' #txt
Hg0 f29 actionCode 'import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import ch.soreco.filehandling.SmbHelper;

try {
	//Declaration
	SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);

	//get directories from root
	out.rootFiles = smb.listFiles(in.Share.NetPath);
	
}
catch( Exception e ) {
	out.error = "Cannot read the root files from the Share "+in.Share.NetPath+
							" (UID: "+in.Share.NetPathUID+") :\n"+e.getMessage();
	ivy.log.error(out.error,e);
	out.success = false;
}
' #txt
Hg0 f29 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f29 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get all directories</name>
        <nameStyle>19
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f29 574 148 36 24 20 -2 #rect
Hg0 f29 @|StepIcon #fIcon
Hg0 f31 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f31 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>next directory?</name>
        <nameStyle>15
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f31 578 210 28 28 14 0 #rect
Hg0 f31 @|AlternativeIcon #fIcon
Hg0 f16 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
Hg0 f16 actionTable 'out=in;
' #txt
Hg0 f16 actionCode 'import jcifs.smb.SmbFile;
import ch.soreco.filehandling.SmbHelper;

String source;
String suffix;

try {
	SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);
	
		int errors = 0;
		int warns = 0;
		
		//count errors and warns 
		for (String file:in.allFiles) {
			if (file.contains("ERROR")) {
				errors++;
			}	else if (file.contains("WARN")) {
				warns++;
			}
		}

		//set suffix
		suffix = "DONE";
		if (errors > 0) {
			suffix = "ERROR";
		}	else if (warns > 0) {
			suffix = "WARN";
		}

		//rename register
			source = in.Directory;
			String dest = in.Directory.replace("050/",suffix);
			smb.renameTo(source,dest);
		
			out.success = true;
		
//Errorhandling
}
catch (Exception e) {
	out.error = "Cannot rename Register "+source+" with suffix "+suffix+".";
	ivy.log.error(out.error,e);
	out.success = false;
}

//reset data for next run
out.Directory = null;
in.allFiles.clear();' #txt
Hg0 f16 type ch.soreco.filehandling.functional.Housekeeping #txt
Hg0 f16 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>rename Directory</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
Hg0 f16 806 340 36 24 -43 -33 #rect
Hg0 f16 @|StepIcon #fIcon
Hg0 f16 -13016147|-1|-16777216 #nodeStyle
Hg0 f18 expr out #txt
Hg0 f18 592 172 592 210 #arcP
Hg0 f24 expr out #txt
Hg0 f24 592 300 592 338 #arcP
Hg0 f35 expr in #txt
Hg0 f35 outCond 'in.success && in.allFiles.size()<1' #txt
Hg0 f35 578 352 584 230 #arcP
Hg0 f35 1 528 352 #addKink
Hg0 f35 2 528 272 #addKink
Hg0 f35 1 0.8015042758663199 0 0 #arcLabel
Hg0 f27 expr out #txt
Hg0 f27 293 64 592 148 #arcP
Hg0 f27 1 592 64 #addKink
Hg0 f27 0 0.652962937452105 0 0 #arcLabel
Hg0 f10 expr in #txt
Hg0 f10 606 352 806 352 #arcP
Hg0 f25 expr in #txt
Hg0 f25 outCond in.rootFiles.size()>0 #txt
Hg0 f25 592 238 592 276 #arcP
Hg0 f23 expr in #txt
Hg0 f23 578 224 298 112 #arcP
Hg0 f23 1 528 224 #addKink
Hg0 f23 2 528 112 #addKink
Hg0 f23 2 0.19941437272759718 0 0 #arcLabel
Hg0 f26 expr out #txt
Hg0 f26 824 340 606 224 #arcP
Hg0 f26 1 824 224 #addKink
Hg0 f26 1 0.2753163395552935 0 0 #arcLabel
Hg0 f14 expr in #txt
Hg0 f14 266 192 267 464 #arcP
Hg0 f14 1 152 192 #addKink
Hg0 f14 2 152 464 #addKink
Hg0 f14 1 0.4390643645818956 0 0 #arcLabel
Hg0 f17 expr in #txt
Hg0 f17 266 352 267 460 #arcP
Hg0 f17 1 192 352 #addKink
Hg0 f17 2 192 440 #addKink
Hg0 f17 2 0.3006378069653967 0 0 #arcLabel
Hg0 f15 expr in #txt
Hg0 f15 outCond in.success #txt
Hg0 f15 280 206 280 256 #arcP
>Proto Hg0 .type ch.soreco.filehandling.functional.Housekeeping #txt
>Proto Hg0 .processKind CALLABLE_SUB #txt
>Proto Hg0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <swimlaneLabel>Housekeeping</swimlaneLabel>
        <swimlaneLabel>Rename Register</swimlaneLabel>
        <swimlaneLabel></swimlaneLabel>
    </language>
    <swimlaneSize>468</swimlaneSize>
    <swimlaneSize>471</swimlaneSize>
    <swimlaneColor>-3355393</swimlaneColor>
    <swimlaneColor>-3342388</swimlaneColor>
</elementInfo>
' #txt
>Proto Hg0 0 0 32 24 18 0 #rect
>Proto Hg0 @|BIcon #fIcon
1b0 f31 expr in #txt
1b0 f31 176 262 176 340 #arcP
1b0 f33 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
1b0 f33 actionTable 'out=in;
' #txt
1b0 f33 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f33 158 444 36 24 20 -2 #rect
1b0 f33 @|StepIcon #fIcon
1b0 f18 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f18 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Selection successful?</name>
        <nameStyle>21
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f18 162 178 28 28 14 0 #rect
1b0 f18 @|AlternativeIcon #fIcon
1b0 f13 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
1b0 f13 actionTable 'out=in;
' #txt
1b0 f13 actionCode 'import java.io.IOException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import ch.soreco.filehandling.SmbHelper;

String rootDirectory;
try {

//Declaration
SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);
rootDirectory = in.Share.NetPath;
List<String> rootFiles =  smb.listFiles(rootDirectory);

//get root directories
	for (String xDirectory:rootFiles) {

		if (xDirectory.endsWith(in.doneSuffix+"/") ||
				xDirectory.endsWith(in.warnSuffix+"/")) {
			in.allFiles.add(xDirectory);
			out.success = true;
		}
	}
	out.success = true;

}
catch( Exception e ) {
	out.error ="Cannot read files from share "+rootDirectory+".";
	ivy.log.error(in.error, e);
	out.success = false;
}

' #txt
1b0 f13 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get all files</name>
        <nameStyle>13
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f13 158 44 36 24 20 -2 #rect
1b0 f13 @|StepIcon #fIcon
1b0 f42 expr out #txt
1b0 f42 176 420 176 444 #arcP
1b0 f23 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
1b0 f23 actionTable 'out=in;
out.ScanLog.Errors=IF(in.success,in.ScanLog.Errors,in.ScanLog.Errors + 1);
out.ScanLog.Successful=IF(in.success,in.ScanLog.Successful + 1,in.ScanLog.Successful);
out.ScanLog.Total=in.ScanLog.Total + 1;
' #txt
1b0 f23 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f23 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Log</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f23 278 284 36 24 20 -2 #rect
1b0 f23 @|StepIcon #fIcon
1b0 f23 -13016147|-1|-16777216 #nodeStyle
1b0 f28 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
1b0 f28 actionTable 'out=in;
out.ScanLog.ScanEnd=new DateTime();
' #txt
1b0 f28 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f28 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Log</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f28 158 340 36 24 20 -2 #rect
1b0 f28 @|StepIcon #fIcon
1b0 f24 expr in #txt
1b0 f24 outCond in.success #txt
1b0 f24 176 110 176 148 #arcP
1b0 f19 expr out #txt
1b0 f19 296 284 190 248 #arcP
1b0 f19 1 296 248 #addKink
1b0 f19 1 0.29660158679765447 0 0 #arcLabel
1b0 f36 expr out #txt
1b0 f36 176 364 176 396 #arcP
1b0 f29 expr out #txt
1b0 f29 194 296 278 296 #arcP
1b0 f16 expr in #txt
1b0 f16 outCond in.allFiles.size()>0 #txt
1b0 f16 176 262 176 284 #arcP
1b0 f17 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f17 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>has next?</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f17 162 234 28 28 14 0 #rect
1b0 f17 @|AlternativeIcon #fIcon
1b0 f26 expr out #txt
1b0 f26 176 68 176 82 #arcP
1b0 f35 expr in #txt
1b0 f35 162 96 158 456 #arcP
1b0 f35 1 56 96 #addKink
1b0 f35 2 56 456 #addKink
1b0 f35 1 0.8831571397628624 0 0 #arcLabel
1b0 f9 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Files found?</name>
        <nameStyle>12
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f9 162 82 28 28 14 0 #rect
1b0 f9 @|AlternativeIcon #fIcon
1b0 f14 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
1b0 f14 actionTable 'out=in;
' #txt
1b0 f14 actionCode 'import jcifs.smb.SmbFile;
import ch.soreco.filehandling.SmbHelper;

String movingFile;
try {
	movingFile = in.allFiles.removeGet(0);
	SmbHelper smb = new SmbHelper(in.Share.NetPathUID, in.Share.NetPathPWD);

	SmbFile source = new SmbFile(movingFile);
	String dest = in.achiveFolder+"/"+source.getName();
	smb.renameTo(movingFile,dest);
	
	out.success = true;
	
}
catch( Exception e ) {
	out.error ="Cannot move file "+movingFile+" into archive.";
	ivy.log.error(in.error, e);
	out.success = false;
}

' #txt
1b0 f14 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f14 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>move File</name>
        <nameStyle>9
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f14 158 284 36 24 20 -2 #rect
1b0 f14 @|StepIcon #fIcon
1b0 f14 -13016147|-1|-16777216 #nodeStyle
1b0 f27 expr out #txt
1b0 f27 176 172 176 178 #arcP
1b0 f10 actionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
1b0 f10 actionTable 'out=in;
' #txt
1b0 f10 actionCode 'import jcifs.smb.SmbFile;
try {

	//Declaration
	List<String> movingFiles =  new List<String>();
	String compareString;
	
	for (String movingFileName:in.allFiles) {
		SmbFile movingFile = new SmbFile(movingFileName);
			compareString = movingFile.getName().split("-").get(2);
			if (	compareString <= in.movingDate.format("yyMMdd") &&
						(movingFile.getName().contains("DONE") ||
						 movingFile.getName().contains("WARN"))
					) {		
				movingFiles.add(movingFileName);
			}
	}
	
	out.allFiles = movingFiles;
	out.success = true;

}
catch( Exception e ) {
	out.error ="Error while reading files for moveing into the archive.";
	ivy.log.error(in.error, e);
	out.success = false;
}' #txt
1b0 f10 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>get moving files</name>
        <nameStyle>16
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f10 159 148 34 24 20 -2 #rect
1b0 f10 @|StepIcon #fIcon
1b0 f25 expr in #txt
1b0 f25 outCond in.success #txt
1b0 f25 176 206 176 234 #arcP
1b0 f20 type ch.soreco.filehandling.functional.Housekeeping #txt
1b0 f20 processCall 'Functional Processes/scanning/ScanLog:set(ch.soreco.orderbook.bo.ScanLog)' #txt
1b0 f20 doCall true #txt
1b0 f20 requestActionDecl '<ch.soreco.orderbook.bo.ScanLog ScanLog> param;
' #txt
1b0 f20 requestMappingAction 'param.ScanLog=in.ScanLog;
' #txt
1b0 f20 responseActionDecl 'ch.soreco.filehandling.functional.Housekeeping out;
' #txt
1b0 f20 responseMappingAction 'out=in;
' #txt
1b0 f20 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>set Log</name>
        <nameStyle>7
</nameStyle>
    </language>
</elementInfo>
' #txt
1b0 f20 158 396 36 24 20 -2 #rect
1b0 f20 @|CallSubIcon #fIcon
1b0 f8 expr in #txt
1b0 f8 162 192 164 444 #arcP
1b0 f8 1 96 192 #addKink
1b0 f8 2 96 432 #addKink
1b0 f8 3 152 432 #addKink
1b0 f8 1 0.2992937058538003 0 0 #arcLabel
1b0 g0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language lang="en">
        <name>in 1</name>
    </language>
</elementInfo>
' #txt
1b0 g0 173 -6 6 6 10 3 #rect
1b0 g0 @|MIGIcon #fIcon
1b0 f0 176 0 176 44 #arcP
1b0 g1 173 501 6 6 10 16 #rect
1b0 g1 @|MOGIcon #fIcon
1b0 f1 expr out #txt
1b0 f1 176 468 176 501 #arcP
>Proto 1b0 0 0 32 24 18 0 #rect
>Proto 1b0 @|BIcon #fIcon
Hg0 f0 mainOut f2 tail #connect
Hg0 f2 head f1 mainIn #connect
Hg0 f21 mainOut f32 tail #connect
Hg0 f32 head f30 mainIn #connect
Hg0 f30 mainOut f34 tail #connect
Hg0 f34 head f7 in #connect
Hg0 f12 mainOut f39 tail #connect
Hg0 f39 head f11 in #connect
Hg0 f4 mainOut f41 tail #connect
Hg0 f41 head f12 mainIn #connect
Hg0 f22 head 1b0 g0 #connect
Hg0 f11 out f22 tail #connect
Hg0 1b0 g1 f8 tail #connect
Hg0 f8 head f6 mainIn #connect
Hg0 f29 mainOut f18 tail #connect
Hg0 f18 head f31 in #connect
Hg0 f13 mainOut f24 tail #connect
Hg0 f24 head f9 in #connect
Hg0 f9 out f35 tail #connect
Hg0 f35 head f31 in #connect
Hg0 f3 mainOut f27 tail #connect
Hg0 f27 head f29 mainIn #connect
Hg0 f10 head f16 mainIn #connect
Hg0 f31 out f25 tail #connect
Hg0 f25 head f13 mainIn #connect
Hg0 f9 out f10 tail #connect
Hg0 f31 out f23 tail #connect
Hg0 f23 head f21 mainIn #connect
Hg0 f16 mainOut f26 tail #connect
Hg0 f26 head f31 in #connect
Hg0 f14 head f6 mainIn #connect
Hg0 f11 out f17 tail #connect
Hg0 f17 head f6 mainIn #connect
Hg0 f7 out f15 tail #connect
Hg0 f15 head f4 mainIn #connect
Hg0 f7 out f14 tail #connect
1b0 f17 out f16 tail #connect
1b0 f16 head f14 mainIn #connect
1b0 f23 mainOut f19 tail #connect
1b0 f19 head f17 in #connect
1b0 f13 mainOut f26 tail #connect
1b0 f26 head f9 in #connect
1b0 f9 out f24 tail #connect
1b0 f24 head f10 mainIn #connect
1b0 f10 mainOut f27 tail #connect
1b0 f27 head f18 in #connect
1b0 f18 out f25 tail #connect
1b0 f25 head f17 in #connect
1b0 f17 out f31 tail #connect
1b0 f31 head f28 mainIn #connect
1b0 f14 mainOut f29 tail #connect
1b0 f29 head f23 mainIn #connect
1b0 f28 mainOut f36 tail #connect
1b0 f36 head f20 mainIn #connect
1b0 f20 mainOut f42 tail #connect
1b0 f42 head f33 mainIn #connect
1b0 f18 out f8 tail #connect
1b0 f8 head f33 mainIn #connect
1b0 f9 out f35 tail #connect
1b0 f35 head f33 mainIn #connect
1b0 g0 m f0 tail #connect
1b0 f0 head f13 mainIn #connect
1b0 f33 mainOut f1 tail #connect
1b0 f1 head g1 m #connect
1b0 0 0 600 500 0 #ivRect
